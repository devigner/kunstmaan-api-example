#!/usr/bin/env bash
set -e

bin/docker-exec-xdebug vendor/bin/phpunit --coverage-html coverage/
