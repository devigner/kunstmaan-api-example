#!/usr/bin/env bash

source bin/utils.sh

swagger () {
  log_start "swagger"
  containsElement "swagger" "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "swagger skipped"
    return
  fi

  if [[ ! -f backend/vendor/bin/openapi ]];
  then
    log_error "swagger is missing"
    return
  fi

  bin/openapi.sh && log_success "swagger" || log_error "swagger"
}

postman () {
  log_start "postman"

  containsElement "postman" "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "postman skipped"
    return
  fi

  if [[ ! `which newman` ]];
  then
    log_error "newman is missing"
    return
  fi

  if [[ ! -f src/Resources/postman/postman_collection.json ]];
  then
    log_error "postman collection is missing"
    return
  fi

  if [[ ! -f src/Resources/postman/Environment.postman_environment.json ]];
  then
    log_error "postman environment is missing"
    return
  fi

  bin/newman.sh && log_success "postman" || log_error "postman"
}

php_cs_fixer () {
  log_start "php-cs-fixer"
  containsElement "php-cs-fixer" "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "swagger skipped"
    return
  fi

  if [[ ! `which php-cs-fixer` ]];
  then
    log_error "php-cs-fixer is missing"
    log_hint "brew install php-cs-fixer"
    return
  fi

  bin/php-cs-fixer.sh --dry-run && log_success "php-cs-fixer" || log_error "php-cs-fixer"
}

php_unit () {
  log_start "php-unit"
  containsElement php_cs_fixer "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "swagger skipped"
    return
  fi

  if [[ ! -f backend/vendor/bin/phpunit ]];
  then
    log_error "phpunit is missing"
    log_hint "composer require --dev phpunit/phpunit"
    return
  fi

  bin/php-unit.sh && log_success "php-unit" || log_error "php-unit"
}

php_stan () {
  log_start "php-stan"
  containsElement "php-stan" "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "swagger skipped"
    return
  fi

  if [[ ! -f backend/vendor/bin/phpstan ]];
  then
    log_error "php-stan is missing"
    log_hint "composer require --dev phpstan/phpstan"
    return
  fi

  bin/php-stan.sh && log_success "php-stan" || log_error "php-stan"
}

psalm () {
  log_start "psalm"
  containsElement "psalm" "${SKIP_CASE[@]}"
  if [[ $? == 0 ]]; then
    log_error "psalm skipped"
    return
  fi

  if [[ ! -f backend/vendor/bin/psalm ]];
  then
    log_error "psalm is missing"
    log_hint "composer require --dev vimeo/psalm"
    return
  fi

  bin/psalm.sh && log_success "psalm" || log_error "psalm"
}

SKIP_CASE=()
for i in "$@"
do
  case $i in
    -h|--help)
    echo "Tests to skip are: swagger, postman, php-stan, php-cs-fixer, php-unit, php-stan, psalm"
    exit
    ;;
    -s=*|--skip=*)
    SKIP_CASE+=("${i#*=}")
    shift # past argument=value
    ;;
  esac
done

swagger
postman
php_cs_fixer
php_unit
php_stan
psalm
