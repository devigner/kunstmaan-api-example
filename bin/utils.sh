#!/usr/bin/env bash
containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

log_error  () {
  log_red    "⨯   $1"
}

log_success() {
  log_green  "✓   $1"
}

log_start  () {
  log_yellow "→️   $1"
}

log_hint   () {
  log_blue   "⚙︎   $1"
}

log_blue   () {
  echo -e "\033[1;34m$1\033[0m"
}

log_yellow () {
  echo -e "\033[1;33m$1\033[0m"
}

log_red    () {
  echo -e "\033[1;31m$1\033[0m"
}

log_green  () {
  echo -e "\033[1;32m$1\033[0m"
}
