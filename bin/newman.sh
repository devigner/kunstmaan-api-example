#!/usr/bin/env bash
set -e

newman run src/Resources/postman/postman_collection.json \
  --environment src/Resources/postman/Environment.postman_environment.json \
  --insecure \
  --reporter-cli-no-failures \
  --reporter-cli-no-summary \
  --reporter-cli-no-success-assertions \
  --reporter-cli-no-banner \
  --ignore-redirects
