#!/usr/bin/env bash
set -e

bin/docker-exec-xdebug vendor/bin/phpstan analyse src --level 1 -c phpstan.neon tests
