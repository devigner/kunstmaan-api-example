#!/usr/bin/env bash
set -e

backend/vendor/bin/openapi ./backend/src > backend/src/Resources/doc/swagger.yaml
git add backend/src/Resources/doc/swagger.yaml
