#!/usr/bin/env bash
set -e

php-cs-fixer fix --config=backend/.php_cs "$@" --allow-risky=yes -n backend/src
