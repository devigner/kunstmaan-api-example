<?php declare(strict_types=1);

namespace App\Tests\Model;

use App\Entity;
use App\Model;
use App\Tests\PropertyAccessTrait;
use Kunstmaan\MediaBundle\Entity\Media;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Kunstmaan\NodeBundle\Helper\NodeMenu;
use Kunstmaan\NodeBundle\Helper\NodeMenuItem;
use Kunstmaan\SeoBundle\Entity\Seo;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Devigner\KunstmaanApiBundle\Entity\PageModelInterface;
use Devigner\KunstmaanApiBundle\Model\ModelSchemaInterface;
use Doctrine\Common\Collections\ArrayCollection;

class PageTest extends TestCase
{
    use PropertyAccessTrait;

    public function getPageDataProvider(): array
    {
        $homePage = $this->createMock(Entity\Pages\HomePage::class);
        $contentPage = $this->createMock(Entity\Pages\ContentPage::class);
        $formPage = $this->createMock(Entity\Pages\FormPage::class);
        $comment = $this->createMock(Entity\NewsComment::class);
        $newsPage = $this->createMock(Entity\Pages\NewsPage::class);
        $newsPage->method('getComments')->willReturn(new ArrayCollection([$comment]));
        $newsOverviewPage = $this->createMock(Entity\Pages\NewsOverviewPage::class);

        return [
            [$homePage,         Model\HomeSchema::class],
            [$contentPage,      Model\ContentSchema::class],
            [$formPage,         Model\FormSchema::class],
            [$newsPage,         Model\NewsSchema::class],
            [$newsOverviewPage, Model\NewsOverviewSchema::class],
        ];
    }

    /**
     * @dataProvider getPageDataProvider
     */
    public function testHomePageSchema($entity, string $schemaClass): void
    {
        $nodeTranslation = $this->createMock(NodeTranslation::class);
        $nodeTranslation->method('getUrl')->willReturn('http');

        /** @var MockObject && Node $node */
        $node = $this->createMock(Node::class);
        $node->method('getNodeTranslation')->willReturn($nodeTranslation);

        $seo = $this->createMock(Seo::class);
        $seo->method('getRefId')->willReturn('x');

        $nodeMenuItemSubSub = $this->createMock(NodeMenuItem::class);
        $nodeMenuItemSubSub->method('getSlug')->willReturn('sub sub');
        $nodeMenuItemSubSub->method('getTitle')->willReturn('Sub Sub menu');
        $nodeMenuItemSubSub->method('getChildren')->willReturn([]);

        $nodeMenuItemSub = $this->createMock(NodeMenuItem::class);
        $nodeMenuItemSub->method('getSlug')->willReturn('sub');
        $nodeMenuItemSub->method('getTitle')->willReturn('Sub menu');
        $nodeMenuItemSub->method('getChildren')->willReturn([$nodeMenuItemSubSub]);

        $nodeMenuItem = $this->createMock(NodeMenuItem::class);
        $nodeMenuItem->method('getSlug')->willReturn('slugger');
        $nodeMenuItem->method('getTitle')->willReturn('HomepageTest');
        $nodeMenuItem->method('getChildren')->willReturn([$nodeMenuItemSub]);

        $nodeMenu = $this->createMock(NodeMenu::class);
        $nodeMenu->method('getActiveForDepth')->willReturn($nodeMenuItem);

        /** @var PageModelInterface && MockObject $entity */
        $entity->setPageTitle('Welcome');
        $entity->setTitle('Goodbye');

        /** @var ModelSchemaInterface $schema */
        $schema = new $schemaClass($entity, $node, 'nl_NL');
        $schema->setSeo($seo);
        $schema->addMenu('x', [], 'nl_NL');
        //$schema->setLinks('', '', 'http://website.ext/', []);

        $media = $this->createMock(Media::class);

        /** @var Entity\PageParts\DownloadPagePart && MockObject $downloadPart */
        $downloadPart = $this->createMock(Entity\PageParts\DownloadPagePart::class);
        $downloadPart->method('getMedia')->willReturn($media);

        /** @var Entity\PageParts\AudioPagePart && MockObject $audioPart */
        $audioPart = $this->createMock(Entity\PageParts\AudioPagePart::class);
        $audioPart->method('getMedia')->willReturn($media);

        /** @var Entity\PageParts\VideoPagePart && MockObject $videoPart */
        $videoPart = $this->createMock(Entity\PageParts\VideoPagePart::class);
        $videoPart->method('getVideo')->willReturn($media);
        $videoPart->method('getThumbnail')->willReturn($media);

        /** @var Entity\PageParts\MultiLineTextPagePart && MockObject $multilineTextPart */
        $multilineTextPart = $this->createMock(Entity\PageParts\MultiLineTextPagePart::class);
        $themePagePart = $this->createMock(Entity\PageParts\CategoryPagePart::class);
        $themePagePart->method('getThemes')->willReturn(new ArrayCollection());

        $parts = [
            new Model\PageParts\TextPagePart($this->createMock(Entity\PageParts\TextPagePart::class)),
            new Model\PageParts\ToTopPagePart($this->createMock(Entity\PageParts\ToTopPagePart::class)),
            new Model\PageParts\TocPagePart($this->createMock(Entity\PageParts\TocPagePart::class)),
            new Model\PageParts\ButtonPagePart($this->createMock(Entity\PageParts\ButtonPagePart::class)),
            new Model\PageParts\CheckboxPagePart($this->createMock(Entity\PageParts\CheckboxPagePart::class)),
            new Model\PageParts\ChoicePagePart($this->createMock(Entity\PageParts\ChoicePagePart::class)),
            new Model\PageParts\EmailPagePart($this->createMock(Entity\PageParts\EmailPagePart::class)),
            new Model\PageParts\FileUploadPagePart($this->createMock(Entity\PageParts\FileUploadPagePart::class)),
            new Model\PageParts\HeaderPagePart($this->createMock(Entity\PageParts\HeaderPagePart::class)),
            new Model\PageParts\ImagePagePart($this->createMock(Entity\PageParts\ImagePagePart::class)),
            new Model\PageParts\IntroTextPagePart($this->createMock(Entity\PageParts\IntroTextPagePart::class)),
            new Model\PageParts\LinePagePart($this->createMock(Entity\PageParts\LinePagePart::class)),
            new Model\PageParts\LinkPagePart($this->createMock(Entity\PageParts\LinkPagePart::class)),
            new Model\PageParts\MultiLineTextPagePart($multilineTextPart),
            new Model\PageParts\RawHtmlPagePart($this->createMock(Entity\PageParts\RawHtmlPagePart::class)),
            new Model\PageParts\SingleLineTextPagePart($this->createMock(Entity\PageParts\SingleLineTextPagePart::class)),
            new Model\PageParts\CategoryPagePart($themePagePart),
            //new Model\PageParts\NewsPagePart($this->createMock(Entity\PageParts\NewsPagePart::class)),
            new Model\PageParts\DownloadPagePart($downloadPart),
            new Model\PageParts\AudioPagePart($audioPart),
            new Model\PageParts\VideoPagePart($videoPart),
        ];
        $schema->addParts($parts);

        $homePageModel = $this->getValueOfProperty($schema, 'data');

        $this->assertObjectHasAttribute('data', $schema);
        $this->assertObjectHasAttribute('seo', $schema);
        $this->assertObjectHasAttribute('links', $schema);
        $this->assertObjectHasAttribute('menu', $schema);

        //$this->assertEquals('Goodbye', $this->getValueOfProperty($homePageModel, 'title'));
        //$this->assertEquals('Welcome', $this->getValueOfProperty($homePageModel, 'pageTitle'));
    }
}
