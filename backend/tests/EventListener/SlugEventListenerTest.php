<?php declare(strict_types=1);

namespace App\Tests\EventListener;

use App\Entity;
use App\Model;
use Devigner\KunstmaanApiBundle\EventListener\SlugEventListener;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializerInterface;
use Kunstmaan\MenuBundle\Entity\MenuItem;
use Kunstmaan\MenuBundle\Repository\MenuItemRepository;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Kunstmaan\NodeBundle\Entity\NodeVersion;
use Kunstmaan\NodeBundle\Event\SlugEvent;
use Kunstmaan\NodeBundle\Event\SlugSecurityEvent;
use Kunstmaan\NodeBundle\Helper\RenderContext;
use Kunstmaan\SeoBundle\Entity\Seo;
use Kunstmaan\SeoBundle\Repository\SeoRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SlugEventListenerTest extends TestCase
{
    public function getTransitionalData(): array
    {
        $tests = [];
        $response = new Response();
        $tests[] = [new SlugEvent($response, new RenderContext()), false, Response::class];

        $response = new Response();

        $node = $this->createMock(Node::class);

        $entity = $this->createMock(Entity\Pages\HomePage::class);
        $entity->method('getSerializeGroups')->willReturn(['always']);
        $model = new Model\HomeSchema($entity, $node, 'nl_NL');
        $entity->method('getSchemaModel')->willReturn($model);

        /** @var NodeVersion && MockObject $nodeVersion */
        $nodeVersion = $this->createMock(NodeVersion::class);
        $nodeVersion->method('getRef')->willReturn($entity);

        $nodeTranslation = $this->createMock(NodeTranslation::class);
        $nodeTranslation->method('getPublicNodeVersion')->willReturn($nodeVersion);
        $nodeTranslation->method('getNode')->willReturn($node);
        $nodeTranslation->method('getLang')->willReturn('nl_NL');

        $tests[] = [new SlugEvent($response, new RenderContext([
            'nodetranslation' => $nodeTranslation,
        ])), true, JsonResponse::class];

        return $tests;
    }

    /**
     * @dataProvider getTransitionalData
     *
     * @param SlugEvent $event
     * @param bool $useRepos
     * @param string $instance
     */
    public function testOnSlugAction(SlugEvent $event, bool $useRepos, string $instance): void
    {
        $entities = [];
        /** @var EntityManager|MockObject $entityManager */
        $entityManager = $this->createMock(EntityManager::class);
        $entityManager->method('persist')->willReturnCallback(function ($entity) use (&$entities) {
            $entities[] = $entity;
        });

        if ($useRepos) {
            $seoEntity = $this->createMock(Seo::class);
            $seoRepository = $this->createMock(SeoRepository::class);
            $seoRepository
                ->method('findFor')
                ->willReturn($seoEntity);
            $entityManager
                ->expects($this->at(0))
                ->method('getRepository')
                ->with(Seo::class)
                ->willReturn($seoRepository);

            $menuItemRepository = $this->createMock(MenuItemRepository::class);
            $menuItemRepository->method('getMenuItemsForLanguage')->willReturn([
                ['id' => 1, 'nodeTranslation' => ['title' => 'a', 'slug' => 'b']],
                ['id' => 2, 'nodeTranslation' => ['title' => 'a', 'slug' => 'b'], 'parent' => ['id' => 1]],
                ['id' => 3, 'nodeTranslation' => ['title' => 'a', 'slug' => 'b'], 'parent' => ['id' => 2]],
            ]);

            $entityManager
                ->expects($this->at(1))
                ->method('getRepository')
                ->with(MenuItem::class)
                ->willReturn($menuItemRepository);
        }

        /** @var SerializerInterface|MockObject $serializer */
        $serializer = $this->createMock(SerializerInterface::class);
        $request = $this->createMock(Request::class);
        $request->request = $this->createMock(ParameterBag::class);
        $request->attributes = $this->createMock(ParameterBag::class);
        $request->method('getLocale')->willReturn('nl_NL');

        $securityEvent = new SlugSecurityEvent();
        $securityEvent->setRequest($request);

        $eventListener = new SlugEventListener(['header']);
        $eventListener->setEntityManager($entityManager);
        $eventListener->setSerializer($serializer);
        $eventListener->onSlugSecurity($securityEvent);
        $eventListener->onSlugAction($event);

        $this->assertInstanceOf($instance, $event->getResponse());
    }
}
