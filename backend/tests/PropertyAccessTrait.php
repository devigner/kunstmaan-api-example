<?php declare(strict_types=1);

namespace App\Tests;

use ReflectionProperty, ReflectionClass;

trait PropertyAccessTrait
{
    /**
     * @param $obj
     * @param $property
     */
    protected function getValueOfProperty($obj, $property)
    {
        $myClassReflection = new ReflectionClass(get_class($obj));
        $secret = $myClassReflection->getProperty($property);
        $secret->setAccessible(true);
        return $secret->getValue($obj);
    }

    protected function setValueOfProperty($obj, $property, $value)
    {
        $myClassReflection = new ReflectionClass(get_class($obj));
        $secret = $myClassReflection->getProperty($property);
        $secret->setAccessible(true);
        $secret->setValue($obj, $value);
    }
}
