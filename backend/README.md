# Kunstmaan Example
## AUTH
### JWT
#### Keys

For JWT to work you need to create a private and public key:
`make run-generate-keys-jwt`, the passphrase needs to be stored in your `.env`

#### Requests

##### Get JWT
> Request

      curl -X POST \
        http://localhost/api/login_check \
        -H 'Accept: */*' \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'Content-Type: application/json' \
        -H 'accept-encoding: gzip, deflate' \
        -H 'cache-control: no-cache' \
        -H 'content-length: 54' \
        -d '{
      	"username": "developer",
      	"password": "developer"
      }'
      
> Response

      {
          "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NTc3NDUyMzgsImV4cCI6MTU1Nzc0ODgzOCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiZGV2ZWxvcGVyIn0.Bsg00uJDaeYrJqVNDYrpRYxmUkEdskVtiIyfd7f7PilazUtpOm5tL1aiBpwANALunuVgv1McwcOGswvmK5saV0UCF8LJlAoa1x-6sEHm2PLYKtQwBOuX3qzakvc8ipFT750moPQ2fa5uSLnAd0MlR6B44mfGct0m4IUjgNBCC-sBZK-Amd2zkYKLxpJEUUSm9aCXnnW_iBQPoHmTIcxukYNlahy1iTTWwitKmco-oONCpJKEiTnCK6DQLulIAngH02cOnQeGtWeH6z5Z1pSv2F1Mc9iSR_1PzMcCBDf5RBHpoYIR1-TwEyCIUG1_WtPo9Mc-UjA6PrDfcwBFGTfFAo4NIMXNlvLmMRFwMZyOYzJiZFTIVYziCeOTH_HJ3mx_SNiCqL6de7qKiZeRV1mffsXKWTvUSz4ltkhOogeSEhUVaRZb5Wc8aJiCBwn_N8BTnbYOhqrHTYPFn_2dYegavMBQ_63MNJA_e5z5uKhVGxZEXzZnY6JbIVifYtSXbRUuKqZsJ8gmLhLjHSRS4dUOx_FK59yM41GNtRodfSWf16zYKFlET0UWqAEcKeYbjMLqgqMkDVdtyH6ca6s27H2fQ_gEu3-i4ZYY37Rt56PXJtuTjsLfjQ0WKMXjslRLj3VEcJ9iqcoD7ak6GIOYjeRYmGjdyPbD0FQdU0IfQojmiPk",
          "refresh_token": "96b2b5326bb8c27ef3cea1e1c9cf1c6e3168feaad5e07db8aff857435db3c1b908dfac07bf5f293eb9f2ad4c94b120d3b66e269b90edd944b8c94eecf658bf08"
      }
      
##### Use JWT
> Request
    
      curl -X GET \
        http://localhost/api \
        -H 'Accept: */*' \
        -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NTc3NDUyMzgsImV4cCI6MTU1Nzc0ODgzOCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiZGV2ZWxvcGVyIn0.Bsg00uJDaeYrJqVNDYrpRYxmUkEdskVtiIyfd7f7PilazUtpOm5tL1aiBpwANALunuVgv1McwcOGswvmK5saV0UCF8LJlAoa1x-6sEHm2PLYKtQwBOuX3qzakvc8ipFT750moPQ2fa5uSLnAd0MlR6B44mfGct0m4IUjgNBCC-sBZK-Amd2zkYKLxpJEUUSm9aCXnnW_iBQPoHmTIcxukYNlahy1iTTWwitKmco-oONCpJKEiTnCK6DQLulIAngH02cOnQeGtWeH6z5Z1pSv2F1Mc9iSR_1PzMcCBDf5RBHpoYIR1-TwEyCIUG1_WtPo9Mc-UjA6PrDfcwBFGTfFAo4NIMXNlvLmMRFwMZyOYzJiZFTIVYziCeOTH_HJ3mx_SNiCqL6de7qKiZeRV1mffsXKWTvUSz4ltkhOogeSEhUVaRZb5Wc8aJiCBwn_N8BTnbYOhqrHTYPFn_2dYegavMBQ_63MNJA_e5z5uKhVGxZEXzZnY6JbIVifYtSXbRUuKqZsJ8gmLhLjHSRS4dUOx_FK59yM41GNtRodfSWf16zYKFlET0UWqAEcKeYbjMLqgqMkDVdtyH6ca6s27H2fQ_gEu3-i4ZYY37Rt56PXJtuTjsLfjQ0WKMXjslRLj3VEcJ9iqcoD7ak6GIOYjeRYmGjdyPbD0FQdU0IfQojmiPk' \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'accept-encoding: gzip, deflate' \
        -H 'cache-control: no-cache' \
        -H 'cookie: sf_redirect=%7B%22token%22%3A%220de5a9%22%2C%22route%22%3A%22api-home%22%2C%22method%22%3A%22GET%22%2C%22controller%22%3A%7B%22class%22%3A%22Symfony%5C%5CBundle%5C%5CFrameworkBundle%5C%5CController%5C%5CRedirectController%22%2C%22method%22%3A%22urlRedirectAction%22%2C%22file%22%3A%22%5C%2Fapp%5C%2Fvendor%5C%2Fsymfony%5C%2Fframework-bundle%5C%2FController%5C%2FRedirectController.php%22%2C%22line%22%3A101%7D%2C%22status_code%22%3A301%2C%22status_text%22%3A%22Moved%20Permanently%22%7D' \
        -b sf_redirect=%7B%22token%22%3A%220de5a9%22%2C%22route%22%3A%22api-home%22%2C%22method%22%3A%22GET%22%2C%22controller%22%3A%7B%22class%22%3A%22Symfony%5C%5CBundle%5C%5CFrameworkBundle%5C%5CController%5C%5CRedirectController%22%2C%22method%22%3A%22urlRedirectAction%22%2C%22file%22%3A%22%5C%2Fapp%5C%2Fvendor%5C%2Fsymfony%5C%2Fframework-bundle%5C%2FController%5C%2FRedirectController.php%22%2C%22line%22%3A101%7D%2C%22status_code%22%3A301%2C%22status_text%22%3A%22Moved%20Permanently%22%7D
        
> Returns: 
        
      {
          "x": "y"
      }

##### Use refresh token

>  Request

      curl -X POST \
        http://localhost/api/token/refresh \
        -H 'Accept: */*' \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -H 'accept-encoding: gzip, deflate' \
        -H 'cache-control: no-cache' \
        -H 'content-length: 142' \
        -d refresh_token=96b2b5326bb8c27ef3cea1e1c9cf1c6e3168feaad5e07db8aff857435db3c1b908dfac07bf5f293eb9f2ad4c94b120d3b66e269b90edd944b8c94eecf658bf08
        
> Response
      
      {
          "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NTc3NDcwNDcsImV4cCI6MTU1Nzc1MDY0Nywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiZGV2ZWxvcGVyIn0.RDTayXsnwyA26hloAISxYCNPG7fiCutwtUXnsEUcsHqeRZUybW0j5tStNG3z6VUluYELZRYdqlpDEtkYuwCEnh2obr5W09EyBsdMbrEie6lzhw7HyIXPoTqfpQaWXiDpO4Dl6nNOP74d0id14Ny33stgSh5H8iwq8b1sz9_iuKa1Lr4NM2OeaCaUW7UP0u2FFXpWrCURviAj_zwyqPmhgUZHotJ-PmK7iWQzbGiWDo406-QS079tlX929lmMGx6nwKzyZ9cdgMHEhAa2OaviWVlBJo-cCWcOaIOwW9I0eH5SY0V553uh0zl3HsHbF0PEYK1Atx7aDPyE3-Lv21-08NKXB4D3Ab80jzKCS5TrTNCGDTDvCoKyxWqrxkqg4XiWq3Ln8txMn26g1-KoRbIdP_M4dDS6HknSU3wfyGqzL2iIc33rEiIG3Viz-MHxSoSStF9w__8gyLv_bfhD3YERTsoaJSd3klsWxFYMtFkaOlMJudkA6taff-3WHwC5kd7DwA-qPSyCo63OnrOngfAMxwKq9XjePE2nVk14PzYDjoxnT3Aw4zP658tMPqHifDWmDvc_SeaIyLN5ZqPCcsvDi57T60uFX9TfVrbDo3LFLH_jZnHUcWjmxPEGzKY_lz6OGxoOs-KrlLUUVe_sKC_d_oPXD7wtWOK0AwPhGmslWt0",
          "refresh_token": "96b2b5326bb8c27ef3cea1e1c9cf1c6e3168feaad5e07db8aff857435db3c1b908dfac07bf5f293eb9f2ad4c94b120d3b66e269b90edd944b8c94eecf658bf08"
      }
