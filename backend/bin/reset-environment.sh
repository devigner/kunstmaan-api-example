#!/bin/bash

bin/console doctrine:schema:drop -f
bin/console doctrine:schema:update -f
bin/console doctrine:fixtures:load -n
bin/console kuma:user:create superadmin superadmin superadmin nl_NL --super-admin --group=Administrators
