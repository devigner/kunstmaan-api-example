<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Helper\Menu;

use Kunstmaan\AdminBundle\Helper\Menu\MenuAdaptorInterface;
use Kunstmaan\AdminBundle\Helper\Menu\MenuBuilder;
use Kunstmaan\AdminBundle\Helper\Menu\MenuItem;
use Symfony\Component\HttpFoundation\Request;

class SettingsMenuAdaptor implements MenuAdaptorInterface
{
    /**
     * @param MenuBuilder $menu
     * @param array $children
     * @param MenuItem|null $parent
     * @param Request|null $request
     */
    public function adaptChildren(MenuBuilder $menu, array &$children, MenuItem $parent = null, Request $request = null): void
    {
        if (null === $parent) {
            return;
        }

        if ('KunstmaanAdminBundle_settings' !== $parent->getRoute()) {
            return;
        }

        $menuItems = [];
        $menuItems[] = $this->createMenuItem($menu, $parent, $request, 'app_admin_companies', 'Companies');
        $menuItems[] = $this->createMenuItem($menu, $parent, $request, 'app_admin_categories', 'Categorieen');
        $menuItems[] = $this->createMenuItem($menu, $parent, $request, 'app_admin_pages_newspage', 'Nieuws');
        $menuItems[] = $this->createMenuItem($menu, $parent, $request, 'app_admin_newsauthor', 'Auteurs');
        $menuItems[] = $this->createMenuItem($menu, $parent, $request, 'app_admin_tag', 'Tags');
        //$menuItems[] = $this->createMenuItem($menu, $parent, $request, 'kunstmaanmenubundle_admin_menu', 'Menu');

        array_splice($children, 1, 0, $menuItems);
    }

    /**
     * @param MenuBuilder $menu
     * @param MenuItem $parent
     * @param Request $request
     * @param string $route
     * @param string $label
     *
     * @return MenuItem
     */
    private function createMenuItem(MenuBuilder $menu, MenuItem $parent, Request $request, string $route, string $label): MenuItem
    {
        $companyMenuItem = new MenuItem($menu);
        $companyMenuItem
            ->setRoute($route)
            ->setUniqueId($route)
            ->setLabel($label)
            ->setParent($parent)
            ->setWeight(-60);

        if (null !== $request && stripos($request->attributes->get('_route'), $companyMenuItem->getRoute()) === 0) {
            $companyMenuItem->setActive(true);
            $parent->setActive(true);
        }
        return $companyMenuItem;
    }
}
