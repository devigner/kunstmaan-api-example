<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller;

use App\AdminList\NewsAuthorAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\Permission\PermissionMap;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AdminListConfiguratorInterface;
use Kunstmaan\ArticleBundle\Controller\AbstractArticleAuthorAdminListController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/%kunstmaan_admin.admin_prefix%/author", requirements={"_locale"="%requiredlocales%"})
 */
class NewsAuthorAdminListController extends AbstractArticleAuthorAdminListController
{
    /**
     * @return AdminListConfiguratorInterface
     */
    public function createAdminListConfigurator(): AdminListConfiguratorInterface
    {
        return new NewsAuthorAdminListConfigurator($this->getEntityManager(), $this->aclHelper, $this->locale, PermissionMap::PERMISSION_EDIT);
    }

    /**
     * @Route("/", name="app_admin_newsauthor")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return parent::doIndexAction($this->getAdminListConfigurator($request), $request);
    }

    /**
     * @Route("/add", name="app_admin_newsauthor_add", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        return parent::doAddAction($this->getAdminListConfigurator($request), null, $request);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, name="app_admin_newsauthor_edit", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function editAction(Request $request, $id): Response
    {
        return parent::doEditAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/delete", requirements={"id" = "\d+"}, name="app_admin_newsauthor_delete", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function deleteAction(Request $request, $id): Response
    {
        return parent::doDeleteAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * @param Request $request
     * @param $_format
     *
     * @Route("/export.{_format}", requirements={"_format" = "csv"}, name="app_admin_newsauthor_export", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function exportAction(Request $request, $_format): Response
    {
        return parent::doExportAction($this->getAdminListConfigurator($request), $_format, $request);
    }
}
