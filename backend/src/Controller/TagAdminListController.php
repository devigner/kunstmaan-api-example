<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller;

use App\AdminList\TagAdminListConfigurator;
use Kunstmaan\ArticleBundle\Controller\AbstractArticleTagAdminListController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/%kunstmaan_admin.admin_prefix%/tag", requirements={"_locale"="%requiredlocales%"})
 */
class TagAdminListController extends AbstractArticleTagAdminListController
{
    /**
     * The index action
     *
     * @Route("/", name="app_admin_tag")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return parent::doIndexAction($this->getAdminListConfigurator($request), $request);
    }

    /**
     * The add action
     *
     * @Route("/add", name="app_admin_tag_add", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        return parent::doAddAction($this->getAdminListConfigurator($request), null, $request);
    }

    /**
     * The edit action
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, name="app_admin_tag_edit", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function editAction(Request $request, $id): Response
    {
        return parent::doEditAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * The edit action
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, name="app_admin_tag_view", methods={"GET"})
     *
     * @return Response
     */
    public function viewAction(Request $request, $id): Response
    {
        return parent::doViewAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * The delete action
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/delete", requirements={"id" = "\d+"}, name="app_admin_tag_delete", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function deleteAction(Request $request, $id): Response
    {
        return parent::doDeleteAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * The export action
     *
     * @param Request $request
     * @param string $_format
     *
     * @Route("/export.{_format}", requirements={"_format" = "csv|xlsx|ods"}, name="app_admin_tag_export", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function exportAction(Request $request, $_format): Response
    {
        return parent::doExportAction($this->getAdminListConfigurator($request), $_format, $request);
    }

    /**
     * The move up action
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/move-up", requirements={"id" = "\d+"}, name="app_admin_tag_move_up", methods={"GET"})
     *
     * @return Response
     */
    public function moveUpAction(Request $request, $id): Response
    {
        return parent::doMoveUpAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * The move down action
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/move-down", requirements={"id" = "\d+"}, name="app_admin_tag_move_down", methods={"GET"})
     *
     * @return Response
     */
    public function moveDownAction(Request $request, $id): Response
    {
        return parent::doMoveDownAction($this->getAdminListConfigurator($request), $id, $request);
    }

    /**
     * @return TagAdminListConfigurator
     */
    public function createAdminListConfigurator(): TagAdminListConfigurator
    {
        return new TagAdminListConfigurator($this->em, $this->aclHelper);
    }
}
