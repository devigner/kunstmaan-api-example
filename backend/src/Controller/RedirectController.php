<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User\User;
use Devigner\KunstmaanApiBundle\Traits\RouterTrait;
use Devigner\KunstmaanApiBundle\Traits\TokenStorageTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class RedirectController
{
    use TokenStorageTrait;
    use RouterTrait;

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @param string $defaultLocale
     */
    public function __construct(string $defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @Route("/", name="home")
     *
     * @return Response
     */
    public function getHomeRedirect(): Response
    {
        $entity = $this->getUser();
        if ($entity instanceof User && $entity->hasRole('ROLE_SUPER_ADMIN')) {
            return new RedirectResponse($this->router->generate('KunstmaanAdminBundle_homepage'));
        }

        return new RedirectResponse($this->router->generate('_slug', ['_locale' => $this->defaultLocale, 'url' => '']));
    }
}
