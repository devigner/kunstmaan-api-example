<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginCheckOptionsController
{
    /**
     * @Route("/api/login_check", name="login_check", methods={"OPTIONS"})
     *
     * @return Response
     */
    public function getHomeRedirect(): Response
    {
        return new Response();
    }
}
