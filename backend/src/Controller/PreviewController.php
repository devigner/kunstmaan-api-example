<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PreviewController
{
    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @param AdapterInterface $cache
     */
    public function __construct(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @Route("/preview/{token}", name="preview_route")
     *
     * @param Request $request
     * @param string $token
     *
     * @return Response
     */
    public function getPreview(Request $request, string $token): Response
    {
        try {
            $cacheEntry = $this->cache->getItem($token);

            if (!$cacheEntry->isHit()) {
                return new Response(null, Response::HTTP_NOT_FOUND);
            }

            return new JsonResponse(json_decode((string)$cacheEntry->get(), true), Response::HTTP_OK);
        } catch (InvalidArgumentException $exception) {
            return new Response('Failed to load cache', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
