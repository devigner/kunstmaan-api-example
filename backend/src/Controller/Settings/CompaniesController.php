<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Settings;

use App\AdminList\CompanyAdminListConfigurator;
use App\Entity\User\Company;
use App\Form\User\CompanyType;
use Kunstmaan\AdminBundle\Controller\BaseSettingsController;
use Kunstmaan\AdminBundle\FlashMessages\FlashTypes;
use Kunstmaan\AdminListBundle\AdminList\AdminList;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Settings controller handling everything related to creating, editing, deleting and listing users in an admin list
 */
class CompaniesController extends BaseSettingsController
{
    use EntityManagerTrait;

    /**
     * List users
     *
     * @Route("/", name="app_admin_companies")
     * @Template("KunstmaanAdminListBundle:Default:list.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function listAction(Request $request): array
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $configurator = new CompanyAdminListConfigurator($this->entityManager);

        /* @var AdminList $adminList */
        $adminList = $this->container->get('kunstmaan_adminlist.factory')->createList($configurator);
        $adminList->bindRequest($request);

        return [
            'adminlist' => $adminList,
        ];
    }

    /**
     * Add a user
     *
     * @Route("/add", name="app_admin_companies_add", methods={"GET", "POST"})
     * @Template("Companies/add.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($company);
                $this->entityManager->flush($company);

                $this->addFlash(
                    FlashTypes::SUCCESS,
                    $this->container->get('translator')->trans('app.company.add.flash.success', [
                        '%groupname%' => $company->getName(),
                    ])
                );

                return new RedirectResponse($this->generateUrl('app_admin_companies'));
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Edit a user
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/edit", requirements={"id" = "\d+"}, name="app_admin_companies_edit", methods={"GET", "POST"})
     * @Template("Companies/edit.html.twig")
     *
     * @return array
     * @throws AccessDeniedException
     */
    public function editAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        /* @var Company $company */
        $company = $this->entityManager->getRepository(Company::class)->find($id);
        $form = $this->createForm(CompanyType::class, $company);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($company);
                $this->entityManager->flush();
                $this->addFlash(
                    FlashTypes::SUCCESS,
                    $this->container->get('translator')->trans('app.company.edit.flash.success', [
                        '%groupname%' => $company->getName(),
                    ])
                );

                return new RedirectResponse($this->generateUrl('app_admin_companies'));
            }
        }

        return [
            'form' => $form->createView(),
            'company' => $company,
        ];
    }

    /**
     * Delete a company
     *
     * @param Request $request
     * @param int $id
     *
     * @Route("/{id}/delete", requirements={"id" = "\d+"}, name="app_admin_companies_delete", methods={"GET", "POST"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $company = $this->entityManager->getRepository(Company::class)->find($id);
        if (null === $company) {
            return new RedirectResponse($this->generateUrl('app_admin_companies'));
        }

        $this->entityManager->remove($company);
        $this->entityManager->flush();

        $this->addFlash(
            FlashTypes::SUCCESS,
            $this->container->get('translator')->trans('app.company.delete.flash.success', [
                '%groupname%' => $company->getName(),
            ])
        );

        return new RedirectResponse($this->generateUrl('app_admin_companies'));
    }
}
