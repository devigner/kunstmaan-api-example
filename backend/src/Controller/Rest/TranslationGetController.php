<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Model;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Devigner\KunstmaanApiBundle\Traits\SerializerTrait;
use Kunstmaan\TranslatorBundle\Entity\Translation;
use Kunstmaan\TranslatorBundle\Repository\TranslationRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class TranslationGetController
{
    use EntityManagerTrait;
    use SerializerTrait;

    private function getTranslationRepository(): TranslationRepository
    {
        return $this->entityManager->getRepository(Translation::class);
    }

    /**
     * @Route("/api/{_locale}/translation")
     *
     * @param string $_locale
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(string $_locale, Request $request): JsonResponse
    {
        $translations = $this->getTranslationRepository()->getTranslationsByLocalesAndDomains([$_locale], ['messages']);

        $models = [];
        foreach ($translations as $translation) {
            $models[] = new Model\Translation($translation);
        }

        return $this->serializedResponse($models, Response::HTTP_OK);
    }
}
