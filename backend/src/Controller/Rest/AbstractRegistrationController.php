<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Devigner\KunstmaanApiBundle\Traits\SerializerTrait;
use Devigner\KunstmaanApiBundle\Traits\TranslatorTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;

abstract class AbstractRegistrationController extends AbstractFOSRestController
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var JWTTokenManagerInterface
     */
    protected $jwtTokenManager;

    use SerializerTrait;
    use TranslatorTrait;

    /**
     * @required
     * @param FormFactoryInterface $formFactory
     */
    public function setFormFactory(FormFactoryInterface $formFactory): void
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @required
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager): void
    {
        $this->userManager = $userManager;
    }

    /**
     * @required
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @required
     * @param JWTTokenManagerInterface $jwtTokenManager
     */
    public function setJwtTokenManager(JWTTokenManagerInterface $jwtTokenManager): void
    {
        $this->jwtTokenManager = $jwtTokenManager;
    }
}
