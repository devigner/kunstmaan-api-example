<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity\User\User;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Devigner\KunstmaanApiBundle\Traits\TokenGeneratorTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Annotations\RouteResource("registration", pluralize=false)
 */
class RegistrationConfirmGetController extends AbstractRegistrationController implements ClassResourceInterface
{
    use TokenGeneratorTrait;

    /**
     * @Annotations\Get("/api/{_locale}/register/confirm/{token}")
     * @Route("/api/{_locale}/register/confirm/{token}", name="fos_user_registration_confirm")
     *
     * @param Request $request
     * @param string $_locale
     * @param string $token
     * @return JsonResponse
     */
    public function confirmAction(Request $request, string $_locale, string $token): JsonResponse
    {
        $userManager = $this->userManager;
        /** @var User $user */
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->addRole('ROLE_DYNAMICS_USER');
        $user->setSyncToken($this->tokenGenerator->generateToken());

        $userManager->updateUser($user);

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
