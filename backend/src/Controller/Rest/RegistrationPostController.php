<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Form\Error\FormErrorResponse;
use App\Form\RegistrationFormType;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Annotations\RouteResource("registration", pluralize=false)
 */
class RegistrationPostController extends AbstractRegistrationController implements ClassResourceInterface
{
    /**
     * @Annotations\Post("/api/{_locale}/register")
     * @Route("/api/{_locale}/register", name="fos_user_registration_register")
     *
     * @param Request $request
     * @param string $_locale
     * @return Response
     */
    public function registerAction(Request $request, string $_locale): Response
    {
        $input = json_decode($request->getContent(), true);
        $input['locale'] = $_locale;
        $input['username'] = $input['email'];
        $user = $this->userManager->createUser();

        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->formFactory->create(RegistrationFormType::class, $user, ['csrf_protection' => false]);
        $form->submit($input);
        if (!$form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);
            if (null !== $response = $event->getResponse()) {
                return $response;
            }

            return $this->serializedResponse(FormErrorResponse::getResponse($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $event = new FormEvent($form, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
        if ($event->getResponse()) {
            return $event->getResponse();
        }

        $this->userManager->updateUser($user);

        $json = [
            'token' => $this->jwtTokenManager->create($user),
            'confirmation_token' => $user->getConfirmationToken(),
        ];
        $response = new JsonResponse($json, Response::HTTP_CREATED);

        $this->eventDispatcher->dispatch(
            FOSUserEvents::REGISTRATION_COMPLETED,
            new FilterUserResponseEvent($user, $request, $response)
        );

        return $response;
    }
}
