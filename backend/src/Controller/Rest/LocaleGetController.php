<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

final class LocaleGetController
{
    /**
     * @var string
     */
    private $locale;

    /**
     * @var array
     */
    private $requiredLocales;

    /**
     * @param string $locale
     * @param string $requiredLocales
     */
    public function __construct(string $locale, string $requiredLocales)
    {
        $this->locale = $locale;
        $this->requiredLocales = explode('|', $requiredLocales);
    }

    /**
     * @Route("/api/locale")
     *
     * @OA\Get(
     *   path="/api/locale",
     *   tags={"bootstrap"},
     *   operationId="get-locales",
     *   @OA\Response(
     *     response=200,
     *     description="Get all locales",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(type="string")
     *     )
     *   )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request): JsonResponse
    {
        $locales = [];
        foreach ($this->requiredLocales as $lang) {
            $locales[$lang] = $this->locale === $lang;
        }
        return new JsonResponse(
            $locales
        );
    }
}
