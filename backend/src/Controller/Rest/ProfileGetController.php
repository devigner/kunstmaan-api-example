<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity;
use App\Model;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Traits\RouterTrait;
use Devigner\KunstmaanApiBundle\Traits\SerializerTrait;
use Devigner\KunstmaanApiBundle\Traits\TokenStorageTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ProfileGetController
{
    use TokenStorageTrait;
    use RouterTrait;
    use SerializerTrait;

    /**
     * @Route("/api/{locale}/profile")
     *
     * @OA\Get(
     *   path="/api/{locale}profile",
     *   tags={"bootstrap"},
     *   operationId="get-profile",
     *   @OA\Response(
     *     response=403,
     *     description="Forbidden"
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Display user info",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/User"
     *     )
     *   )
     * )
     *
     * @param string $locale
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getUserAction(string $locale, Request $request): Response
    {
        /** @var Entity\User\User $entity */
        $entity = $this->getUser();
        if (!$entity instanceof Entity\User\User) {
            return new JsonResponse([], Response::HTTP_FORBIDDEN);
        }

        return $this->serializedResponse(
            new Model\User\User($entity, $this->isGranted('ROLE_PREVIOUS_ADMIN')),
            Response::HTTP_OK,
            ['always', 'user']
        );
    }
}
