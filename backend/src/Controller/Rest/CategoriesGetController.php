<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity\Category;
use App\Model;
use App\Repository\CategoryRepository;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Devigner\KunstmaanApiBundle\Traits\SerializerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CategoriesGetController
{
    use EntityManagerTrait;
    use SerializerTrait;

    private function getCategoryRepository(): CategoryRepository
    {
        return $this->entityManager->getRepository(Category::class);
    }

    /**
     * @Route("/api/{_locale}/themes")
     *
     * @param string $_locale
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(string $_locale, Request $request): JsonResponse
    {
        /** @var Category[] $themes */
        $themes = $this->getCategoryRepository()->findAll();

        $models = [];
        foreach ($themes as $theme) {
            $theme->setLocale($_locale);
            $models[] = new Model\Category($theme);
        }

        return $this->serializedResponse($models, Response::HTTP_OK);
    }
}
