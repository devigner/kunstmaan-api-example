<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\User;

use App\Entity;
use App\Model\ModelInterface;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class User implements ModelInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always", "user"})
     * @JMS\Type("string")
     * @OA\Property(type="string", format="email")
     */
    protected $email;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $username;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $phoneNumber;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $name;

    /**
     * @var bool
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("bool")
     * @OA\Property(type="boolean")
     */
    protected $impersonated;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $role;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $logoutRoute;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $loginRoute;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("App\Model\User\Company")
     * @OA\Property(ref="#/components/schemas/Company")
     */
    protected $company;

    /**
     * @param Entity\User\User $user
     * @param bool $impersonated
     */
    public function __construct(Entity\User\User $user, bool $impersonated = false)
    {
        $this->email = $user->getEmail();
        $this->username = $user->getUsername();

        if (null !== $user->getCompany()) {
            $this->company = $user->getCompany()->getModel();
        }
        $this->impersonated = $impersonated;
    }

    /**
     * @param string $logoutRoute
     */
    public function setLogoutRoute(string $logoutRoute): void
    {
        $this->logoutRoute = $logoutRoute;
    }

    /**
     * @param string $loginRoute
     */
    public function setLoginRoute(string $loginRoute): void
    {
        $this->loginRoute = $loginRoute;
    }
}
