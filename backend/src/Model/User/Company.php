<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\User;

use App\Entity;
use App\Model\ModelInterface;
use App\Model\Category;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class Company implements ModelInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $street;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $city;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $country;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $organisationType;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    protected $chamberOfCommerceNumber;

    /**
     * @var array|Category[]
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/Theme"
     *   )
     * )
     */
    protected $themes;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("array")
     * @OA\Property(type="array")
     */
    protected $tags;

    /**
     * @param Entity\User\Company $company
     */
    public function __construct(Entity\User\Company $company)
    {
        $this->name = $company->getName();
        $this->street = $company->getStreet();
        $this->postalCode = $company->getPostalCode();
        $this->city = $company->getCity();
        $this->country = $company->getCountry();
        $this->chamberOfCommerceNumber = $company->getChamberOfCommerceNumber();
        $this->organisationType = $company->getOrganisationType();

        $this->themes = [];
        foreach ($company->getThemes() as $theme) {
            $this->themes[] = $theme->getModel();
        }

        $this->tags = [];
        foreach ($company->getTags() as $tag) {
            $this->tags[] = $tag->getName();
        }
    }
}
