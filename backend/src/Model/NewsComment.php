<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model;

use App\Entity;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class NewsComment implements ModelInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $comment;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"user"})
     * @JMS\Type("App\Model\User\User")
     * @OA\Property(ref="#/components/schemas/User")
     */
    private $user;

    /**
     * @var int
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("integer")
     * @OA\Property(type="integer")
     */
    private $userRef;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $updatedAt;

    public function __construct(Entity\NewsComment $comment)
    {
        $this->comment = $comment->getComment();
        $this->user = $comment->getUser()->getModel();
        $this->userRef = $comment->getUser()->getId();
        $this->createdAt = $comment->getCreatedAt()->format(DATE_RFC1036);
        $this->updatedAt = $comment->getUpdatedAt()->format(DATE_RFC1036);
    }
}
