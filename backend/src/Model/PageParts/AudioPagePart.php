<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use App\Resolver\MediaResolver;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class AudioPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $media;

    /**
     * @param PageParts\AudioPagePart $pagePart
     */
    public function __construct(PageParts\AudioPagePart $pagePart)
    {
        $this->media = MediaResolver::createPublicUrl($pagePart->getMedia());

        parent::__construct($pagePart);
    }
}
