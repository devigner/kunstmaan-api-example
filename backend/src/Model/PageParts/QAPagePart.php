<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Model\User\Company;
use Devigner\KunstmaanApiBundle\Model\Helper\Links;
use Devigner\KunstmaanApiBundle\Model\ModelInjectionInterface;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;
use App\Entity\PageParts;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
final class QAPagePart extends AbstractPagePart implements PagePartsEntityInterface, ModelInjectionInterface
{
    /**
     * @var int
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("integer")
     * @OA\Property(type="integer")
     */
    private $maxResults;

    /**
     * @var array|Company[]
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/Company"
     *   )
     * )
     */
    private $children;

    /**
     * @var Links
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     */
    protected $links;

    /**
     * @param PageParts\QAPagePart $pagePart
     */
    public function __construct(PageParts\QAPagePart $pagePart)
    {
        $this->maxResults = $pagePart->getMaxResults();

        parent::__construct($pagePart);
    }

    /**
     * @param string $namespace,
     * @param array|Company[] $children
     */
    public function setChildren(string $namespace, array $children): void
    {
        $this->children[$namespace] = $children;
    }

    /**
     * @param string $namespace,
     * @param string $self
     * @param string $last
     * @param string|null $next
     */
    public function setLinks(string $namespace, string $self, string $last, ?string $next = null): void
    {
        $this->links[$namespace] = new Links($self, $last, $next);
    }
}
