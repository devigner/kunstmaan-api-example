<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
class HomeStatisticsPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(type="array")
     */
    private $items;

    /**
     * @param PageParts\HomeStatisticsPagePart $pagePart
     */
    public function __construct(PageParts\HomeStatisticsPagePart $pagePart)
    {
        $this->items[] = [
            'value' => $pagePart->getStatistic1Value(),
            'title' => $pagePart->getStatistic1Title(),
            'text' => $pagePart->getStatistic1Text(),
        ];

        $this->items[] = [
            'value' => $pagePart->getStatistic2Value(),
            'title' => $pagePart->getStatistic2Title(),
            'text' => $pagePart->getStatistic2Text(),
        ];

        $this->items[] = [
            'value' => $pagePart->getStatistic3Value(),
            'title' => $pagePart->getStatistic3Title(),
            'text' => $pagePart->getStatistic3Text(),
        ];

        parent::__construct($pagePart);
    }
}
