<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class ToTopPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @param PageParts\ToTopPagePart $pagePart
     */
    public function __construct(PageParts\ToTopPagePart $pagePart)
    {
        parent::__construct($pagePart);
    }
}
