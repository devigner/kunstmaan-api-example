<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class ButtonPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $linkText;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $linkUrl;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $position;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $size;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $buttonType;

    /**
     * @param PageParts\ButtonPagePart $pagePart
     */
    public function __construct(PageParts\ButtonPagePart $pagePart)
    {
        $this->linkText = $pagePart->getLinkText();
        $this->linkUrl = $pagePart->getLinkUrl();
        $this->position = $pagePart->getPosition();
        $this->size = $pagePart->getSize();
        $this->buttonType = $pagePart->getType();

        parent::__construct($pagePart);
    }
}
