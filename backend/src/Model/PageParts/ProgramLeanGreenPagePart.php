<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;

/**
 * @OA\Schema()
 */
class ProgramLeanGreenPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $leanTitle;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $leanContent;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $greenTitle;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $greenContent;

    /**
     * @param PageParts\ProgramLeanGreenPagePart $pagePart
     */
    public function __construct(PageParts\ProgramLeanGreenPagePart $pagePart)
    {
        $this->title = $pagePart->getTitle();
        $this->leanTitle = $pagePart->getLeanTitle();
        $this->leanContent = $pagePart->getLeanContent();
        $this->greenTitle = $pagePart->getGreenTitle();
        $this->greenContent = $pagePart->getGreenContent();

        parent::__construct($pagePart);
    }
}
