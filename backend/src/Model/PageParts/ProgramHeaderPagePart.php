<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use App\Resolver\MediaResolver;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;

/**
 * @OA\Schema()
 */
class ProgramHeaderPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $subtitle;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $content;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $image;

    /**
     * @param PageParts\ProgramHeaderPagePart $pagePart
     */
    public function __construct(PageParts\ProgramHeaderPagePart $pagePart)
    {
        $this->title = $pagePart->getTitle();
        $this->image = MediaResolver::createPublicUrl($pagePart->getImage());
        $this->subtitle = $pagePart->getSubtitle();
        $this->content = $pagePart->getContent();

        parent::__construct($pagePart);
    }
}
