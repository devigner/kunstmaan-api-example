<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use App\Provider\MediaResolver;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;

/**
 * @OA\Schema()
 */
class ProgramStarwayPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $subtitle;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $content;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $registerButton;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(type="array")
     */
    private $stars;

    /**
     * @param PageParts\ProgramStarwayPagePart $pagePart
     */
    public function __construct(PageParts\ProgramStarwayPagePart $pagePart)
    {
        $this->title = $pagePart->getTitle();
        $this->subtitle = $pagePart->getSubtitle();
        $this->content = $pagePart->getContent();
        $this->registerButton = $pagePart->getRegisterButton();

        $this->stars[] = [
            'title' => $pagePart->getStar1Title(),
            'content' => $pagePart->getStar1Content(),
            'image' => MediaResolver::createPublicUrl($pagePart->getStar1Image()),
            'imageAlt' => $pagePart->getStar1ImageAltText(),
        ];

        $this->stars[] = [
            'title' => $pagePart->getStar2Title(),
            'content' => $pagePart->getStar2Content(),
            'image' => MediaResolver::createPublicUrl($pagePart->getStar2Image()),
            'imageAlt' => $pagePart->getStar2ImageAltText(),
        ];

        $this->stars[] = [
            'title' => $pagePart->getStar3Title(),
            'content' => $pagePart->getStar3Content(),
            'image' => MediaResolver::createPublicUrl($pagePart->getStar3Image()),
            'imageAlt' => $pagePart->getStar3ImageAltText(),
        ];

        $this->stars[] = [
            'title' => $pagePart->getStar4Title(),
            'content' => $pagePart->getStar4Content(),
            'image' => MediaResolver::createPublicUrl($pagePart->getStar4Image()),
            'imageAlt' => $pagePart->getStar4ImageAltText(),
        ];

        parent::__construct($pagePart);
    }
}
