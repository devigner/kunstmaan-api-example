<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class LinkPagePart extends AbstractPagePart implements PagePartsEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $url;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $text;

    /**
     * @var bool
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("bool")
     * @OA\Property(type="bool")
     */
    private $openInNewWindow;

    /**
     * @param PageParts\LinkPagePart $pagePart
     */
    public function __construct(PageParts\LinkPagePart $pagePart)
    {
        $this->url = $pagePart->getUrl();
        $this->text = $pagePart->getText();
        $this->openInNewWindow = $pagePart->getOpenInNewWindow();

        parent::__construct($pagePart);
    }
}
