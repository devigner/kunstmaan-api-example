<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractFormPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class MultiLineTextPagePart extends AbstractFormPagePart implements PagePartsEntityInterface
{
    /**
     * @param PageParts\MultiLineTextPagePart $pagePart
     */
    public function __construct(PageParts\MultiLineTextPagePart $pagePart)
    {
        parent::__construct($pagePart);
    }
}
