<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\PageParts;

use App\Entity\PageParts;
use App\Model\User\Company;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\Helper\Links;
use Devigner\KunstmaanApiBundle\Model\ModelInjectionInterface;
use Devigner\KunstmaanApiBundle\Model\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @OA\Schema()
 */
final class BenefitPagePart extends AbstractPagePart implements PagePartsEntityInterface, ModelInjectionInterface
{
    /**
     * @var int
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("integer")
     * @OA\Property(type="integer")
     */
    private $maxResults;

    /**
     * @var array|Company[]
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/Company"
     *   )
     * )
     */
    private $children;

    /**
     * @var Links
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("Devigner\KunstmaanApiBundle\Model\Helper\Links")
     * @OA\Property(ref="#/components/schemas/Links")
     */
    protected $links;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $showAllButton;

    /**
     * @param PageParts\BenefitPagePart $pagePart
     */
    public function __construct(PageParts\BenefitPagePart $pagePart)
    {
        $this->showAllButton = $pagePart->getShowAllButton();
        $this->maxResults = $pagePart->getMaxResults();
        parent::__construct($pagePart);
    }

    /**
     * @param string $namespace
     * @param array|Company[] $children
     */
    public function setChildren(string $namespace, array $children): void
    {
        $this->children = $children;
    }

    /**
     * @param string $namespace
     * @param string $self
     * @param string $last
     * @param string|null $next
     */
    public function setLinks(string $namespace, string $self, string $last, ?string $next = null): void
    {
        $this->links = new Links($self, $last, $next);
    }
}
