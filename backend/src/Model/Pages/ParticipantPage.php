<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\Pages;

use App\Model\User\Company;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use App\Entity\Pages;
use Devigner\KunstmaanApiBundle\Model\PageEntityInterface;
use Devigner\KunstmaanApiBundle\Model\Pages\AbstractPage;

/**
 * @OA\Schema()
 */
final class ParticipantPage extends AbstractPage implements PageEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $pageTitle;

    /**
     * @var array|Company[]
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/Company"
     *   )
     * )
     */
    private $children;

    /**
     * @param Pages\ParticipantPage $page
     */
    public function __construct(Pages\ParticipantPage $page)
    {
        $this->title = $page->getTitle();
        $this->pageTitle = $page->getPageTitle();

        parent::__construct($page);
    }

    /**
     * @param string $namespace
     * @param array|Company[] $children
     */
    public function setChildren(string $namespace, array $children): void
    {
        $this->children = $children;
    }
}
