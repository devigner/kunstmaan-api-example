<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Model\Pages;

use App\Entity\Pages;
use DateTime;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;
use Devigner\KunstmaanApiBundle\Model\PageEntityInterface;
use Devigner\KunstmaanApiBundle\Model\Pages\AbstractPage;

/**
 * @OA\Schema()
 */
final class NewsPage extends AbstractPage implements PageEntityInterface
{
    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var DateTime
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("DateTime")
     * @OA\Property(type="DateTime")
     */
    private $date;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $author;

    /**
     * @var string
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("string")
     * @OA\Property(type="string")
     */
    private $pageTitle;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(type="array")
     */
    private $tags;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/Theme"
     *   )
     * )
     */
    protected $themes;

    /**
     * @var array
     *
     * @JMS\Expose()
     * @JMS\Groups({"always"})
     * @JMS\Type("array")
     * @OA\Property(
     *   type="array",
     *   @OA\Items(
     *     ref="#/components/schemas/NewsComment"
     *   )
     * )
     */
    protected $comments;

    /**
     * @param Pages\NewsPage $page
     */
    public function __construct(Pages\NewsPage $page)
    {
        $this->title = $page->getTitle();
        $this->pageTitle = $page->getPageTitle();
        $this->author = $page->getAuthor();
        $this->date = $page->getDate();

        foreach ($page->getCategories() as $theme) {
            $this->themes[] = $theme->getModel();
        }

        foreach ($page->getTags() as $tag) {
            $this->tags[] = $tag->getName();
        }

        if ($page->getComments() instanceof Collection) {
            foreach ($page->getComments() as $comment) {
                $this->comments[] = $comment->getModel();
            }
        }

        parent::__construct($page);
    }
}
