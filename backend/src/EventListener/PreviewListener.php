<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\EventListener;

use Psr\Cache\InvalidArgumentException;
use Devigner\KunstmaanApiBundle\Event\AdminRequestEvent;
use Devigner\KunstmaanApiBundle\Traits\RouterTrait;
use Devigner\KunstmaanApiBundle\Traits\TokenGeneratorTrait;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class PreviewListener implements EventSubscriberInterface
{
    use TokenGeneratorTrait;
    use RouterTrait;

    /**
     * @var array
     */
    private $replace;

    /**
     * @var string
     */
    private $hostAndSchemeFrontend;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @param array $replace
     * @param string $hostAndSchemeFrontend
     * @param string $enabled
     */
    public function __construct(array $replace, string $hostAndSchemeFrontend, string $enabled)
    {
        $this->replace = $replace;
        $this->hostAndSchemeFrontend = $hostAndSchemeFrontend;
        $this->enabled = $enabled !== 'false';
    }

    /**
     * @required
     * @param AdapterInterface $cache
     */
    public function setCache(AdapterInterface $cache): void
    {
        $this->cache = $cache;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            AdminRequestEvent::ADMIN_REQUEST => 'onAdminRequest',
        ];
    }

    /**
     * @param AdminRequestEvent $event
     */
    public function onAdminRequest(AdminRequestEvent $event): void
    {
        if (!$this->enabled) {
            return;
        }

        try {
            $token = substr($this->tokenGenerator->generateToken(), 0, 8);
            $cacheEntry = $this->cache->getItem($token);
            $cacheEntry->expiresAfter(3600);
            $cacheEntry->set($event->getOriginalResponse()->getContent());
            $this->cache->save($cacheEntry);

            $route = sprintf('%s%s', $this->hostAndSchemeFrontend, $event->getRequest()->getPathInfo());
            foreach ($this->replace as $key) {
                $route = str_replace($key, '', $route);
            }

            $response = new RedirectResponse(sprintf('%s?preview-token=%s', $route, $token));

            $event->setResponse($response);
        } catch (InvalidArgumentException $exception) {
            $event->setResponse(new Response('Creating cache failed', Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }
}
