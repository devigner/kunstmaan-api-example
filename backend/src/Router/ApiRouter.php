<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Router;

use Kunstmaan\NodeBundle\Router\SlugRouter;

class ApiRouter extends SlugRouter
{
    /**
     * @return array
     */
    protected function getSlugRouteParameters(): array
    {
        $config = parent::getSlugRouteParameters();
        $config['path'] = sprintf('/api%s', $config['path']);
        return $config;
    }
}
