<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM\StringFilterType;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;

/**
 * User admin list configurator used to manage {@link User} in the admin
 */
class CompanyAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator
{
    /**
     * Build filters for admin list
     */
    public function buildFilters(): void
    {
        $this->addFilter('name', new StringFilterType('name'), 'app.company.adminlist.filter.name');
    }

    /**
     * Configure the visible columns
     */
    public function buildFields(): void
    {
        $this->addField('name', 'Naam', true);
        $this->addField('dynamicsId', 'Dynamics ID', false);
        $this->addField('lastDynamicsSync', 'Laatste Dynamics Sync', false);
    }

    /**
     * Override path convention (because settings is a virtual admin subtree)
     *
     * @param string $suffix
     *
     * @return string
     */
    public function getPathByConvention($suffix = null): string
    {
        return 'app_admin_companies' . (empty($suffix) ? '' : '_' . $suffix);
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName(): string
    {
        return 'User\Company';
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * Get total per page
     *
     * @return int
     */
    public function getCount(): int
    {
        return 50;
    }
}
