<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use Kunstmaan\ArticleBundle\AdminList\AbstractArticleAuthorAdminListConfigurator;

/**
 * The AdminList configurator for the NewsAuthor
 */
class NewsAuthorAdminListConfigurator extends AbstractArticleAuthorAdminListConfigurator
{
    /**
     * Return current bundle name.
     *
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * Return current entity name.
     *
     * @return string
     */
    public function getEntityName(): string
    {
        return 'NewsAuthor';
    }
}
