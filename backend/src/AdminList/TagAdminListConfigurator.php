<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use App\Form\TagAdminType;
use Doctrine\ORM\EntityManagerInterface;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\ArticleBundle\AdminList\AbstractArticleTagAdminListConfigurator;

class TagAdminListConfigurator extends AbstractArticleTagAdminListConfigurator
{
    /**
     * @param EntityManagerInterface $em The entity manager
     * @param AclHelper $aclHelper The ACL helper
     */
    public function __construct(EntityManagerInterface $em, AclHelper $aclHelper)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(TagAdminType::class);
    }

    /**
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return 'Tag';
    }
}
