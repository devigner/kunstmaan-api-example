<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use App\Entity\Pages\NewsOverviewPage;
use App\Entity\Pages\NewsPage;
use App\Repository\NewsOverviewPageRepository;
use Doctrine\ORM\QueryBuilder;
use Kunstmaan\ArticleBundle\AdminList\AbstractArticlePageAdminListConfigurator;

class NewsPageAdminListConfigurator extends AbstractArticlePageAdminListConfigurator
{
    /**
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return 'Pages\NewsPage';
    }

    /**
     * @param QueryBuilder $queryBuilder The query builder
     */
    public function adaptQueryBuilder(QueryBuilder $queryBuilder): void
    {
        parent::adaptQueryBuilder($queryBuilder);

        $queryBuilder->setParameter('class', NewsPage::class);
    }

    /**
     * @return NewsOverviewPageRepository
     */
    public function getOverviewPageRepository(): NewsOverviewPageRepository
    {
        return $this->em->getRepository(NewsOverviewPage::class);
    }

    /**
     * @return string
     */
    public function getListTemplate(): string
    {
        return '@KunstmaanApi/AdminList/NewsPageAdminList/list.html.twig';
    }
}
