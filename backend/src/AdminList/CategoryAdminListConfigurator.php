<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use App\Form\CategoryAdminType;
use Doctrine\ORM\EntityManagerInterface;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\ArticleBundle\AdminList\AbstractArticleCategoryAdminListConfigurator;

/**
 * The AdminList configurator for the NewsCategory
 */
class CategoryAdminListConfigurator extends AbstractArticleCategoryAdminListConfigurator
{
    /**
     * @param EntityManagerInterface $em The entity manager
     * @param AclHelper $aclHelper The ACL helper
     */
    public function __construct(EntityManagerInterface $em, AclHelper $aclHelper)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(CategoryAdminType::class);
    }

    public function buildFields(): void
    {
        $this->addField('name', 'Naam', true);
        $this->addField('slug', 'Slug', true);
        $this->addField('colour', 'Kleur', true);
    }

    /**
     * Return current bundle name.
     *
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * Return current entity name.
     *
     * @return string
     */
    public function getEntityName(): string
    {
        return 'Category';
    }
}
