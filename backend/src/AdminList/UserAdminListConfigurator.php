<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\AdminList;

use Kunstmaan\UserManagementBundle\AdminList\UserAdminListConfigurator as ParentAdminListConfigurator;

class UserAdminListConfigurator extends ParentAdminListConfigurator
{
    public function buildFields(): void
    {
        $this->addField('enabled', 'Actief', true);
        $this->addField('firstName', 'Voornaam', true);
        $this->addField('lastName', 'Achternaam', true);
        $this->addField('email', 'E-mail', true);
        $this->addField('roles', 'Rollen', false);
        $this->addField('lastLogin', 'Laatst gezien', false);
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName(): string
    {
        return 'User\User';
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName(): string
    {
        return 'App';
    }

    /**
     * Get total per page
     *
     * @return int
     */
    public function getCount(): int
    {
        return 50;
    }
}
