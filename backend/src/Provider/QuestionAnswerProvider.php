<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Provider;

use App\Entity\User\Company;
use App\Repository\CompanyRepository;
use Devigner\KunstmaanApiBundle\Provider\AbstractProvider;
use Symfony\Component\HttpFoundation\Request;

class QuestionAnswerProvider extends AbstractProvider
{
    public function getNamespace(): string
    {
        return 'question-answer';
    }

    /**
     * @param Request $request
     * @param int $maxResult
     */
    public function executeService(Request $request, int $maxResult): void
    {
        $name = $request->get('name');
        $themes = $request->get('theme') ? explode(',', $request->get('theme')) : null;
        $tags = $request->get('tag') ? explode(',', $request->get('tag')) : null;
        $industries = $request->get('industry') ? explode(',', $request->get('industry')) : null;

        /** @var CompanyRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(Company::class);
        $result = $companyRepository->findPublicCompanies($name, $themes, $tags, $industries);

        if (null === $result) {
            return;
        }

        $this->createPagerFanta($result->toArray(), $maxResult, $request);

        $this->results = [];
        foreach ($this->pagerfanta->getCurrentPageResults() as $entity) {
            /** @var Company $entity */
            $this->results[] = $entity->getModel();
        }
    }
}
