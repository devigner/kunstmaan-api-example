<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use Kunstmaan\AdminBundle\Form\WysiwygType;
use Kunstmaan\ArticleBundle\Form\AbstractCategoryAdminType;
use Kunstmaan\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The type for Category
 */
class CategoryAdminType extends AbstractCategoryAdminType
{
    /**
     * Builds the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('content', WysiwygType::class)
            ->add('colour', ChoiceType::class, [
                'required' => true,
                'expanded' => false,
                'placeholder' => false,
                'choices' => [
                    'blue' => 'blue',
                    'dark-blue' => 'dark-blue',
                    'orange' => 'orange',
                    'yellow' => 'yellow',
                    'light-green' => 'light-green',
                    'green' => 'green',
                    'dark-green' => 'dark-green',
                ],
                'attr' => [
                    'class' => 'js-advanced-select form-control advanced-select',
                ],
            ])
            ->add('icon', MediaType::class, [
                'mediatype' => 'image',
            ])
            ->add('image', MediaType::class, [
                'mediatype' => 'image',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix(): string
    {
        return 'theme_form';
    }
}
