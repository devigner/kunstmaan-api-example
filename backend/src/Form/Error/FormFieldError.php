<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\Error;

use OpenApi\Annotations as OA;
use JMS\Serializer\Annotation as JMS;

/**
 * @OA\Schema()
 */
class FormFieldError
{
    /**
     * @var string
     * @OA\Property(type="string")
     */
    public $field;

    /**
     * @var string
     * @OA\Property(type="string")
     */
    public $error;

    public function __construct(string $field, $error)
    {
        $this->field = $field;
        $this->error = $error;
    }
}
