<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\Error;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;

class FormErrorResponse
{
    /**
     * @param FormInterface $form
     * @return array|FormFieldError[]
     */
    public static function getResponse(FormInterface $form): array
    {
        $errors = $form->getErrors(true, true);
        $response = [
            ['form' => ($form->getName())],
        ];
        foreach ($errors as $name => $errorIterator) {
            /** @var FormError $errorIterator */
            /** @var ConstraintViolation $cause */
            $cause = $errorIterator->getCause();
            $response[] = new FormFieldError($cause->getPropertyPath(), $errorIterator->getMessage());
        }

        return $response;
    }
}
