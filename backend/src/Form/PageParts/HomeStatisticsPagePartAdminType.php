<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\PageParts;

use App\Entity\PageParts\HomeStatisticsPagePart;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * HomeStatisticsPagePartAdminType
 */
class HomeStatisticsPagePartAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     *
     * @see FormTypeExtensionInterface::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('statistic1Value', TextType::class, [
            'label' => 'Value',
            'required' => true,
        ]);
        $builder->add('statistic1Title', TextType::class, [
            'label' => 'Title',
            'required' => true,
        ]);
        $builder->add('statistic1Text', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'label' => 'Text',
            'required' => true,
        ]);
        $builder->add('statistic2Value', TextType::class, [
            'label' => 'Value',
            'required' => true,
        ]);
        $builder->add('statistic2Title', TextType::class, [
            'label' => 'Title',
            'required' => true,
        ]);
        $builder->add('statistic2Text', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'label' => 'Text',
            'required' => true,
        ]);
        $builder->add('statistic3Value', TextType::class, [
            'label' => 'Value',
            'required' => true,
        ]);
        $builder->add('statistic3Title', TextType::class, [
            'label' => 'Title',
            'required' => true,
        ]);
        $builder->add('statistic3Text', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'label' => 'Text',
            'required' => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix()
    {
        return 'app_homestatisticspageparttype';
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HomeStatisticsPagePart::class,
        ]);
    }
}
