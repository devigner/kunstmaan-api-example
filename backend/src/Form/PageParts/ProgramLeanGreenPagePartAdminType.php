<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\PageParts;

use App\Entity\PageParts\ProgramLeanGreenPagePart;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ProgramLeanGreenPagePartAdminType
 */
class ProgramLeanGreenPagePartAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     *
     * @see FormTypeExtensionInterface::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('leanTitle', TextType::class, [
            'required' => false,
        ]);
        $builder->add('leanContent', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
        $builder->add('greenTitle', TextType::class, [
            'required' => false,
        ]);
        $builder->add('greenContent', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix(): string
    {
        return 'app_programleangreenpageparttype';
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProgramLeanGreenPagePart::class,
        ]);
    }
}
