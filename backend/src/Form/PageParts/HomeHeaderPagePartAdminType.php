<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\PageParts;

use App\Entity\PageParts\HomeHeaderPagePart;
use Kunstmaan\MediaBundle\Form\Type\MediaType;
use Kunstmaan\AdminBundle\Form\WysiwygType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * HomeHeaderPagePartAdminType
 */
class HomeHeaderPagePartAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     *
     * @see FormTypeExtensionInterface::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('imgSrc', MediaType::class, [
            'mediatype' => 'image',
            'label' => 'Background image',
            'required' => true,
        ]);
        $builder->add('title', WysiwygType::class, [
            'required' => true,
        ]);
        $builder->add('subtitle', TextType::class, [
            'required' => true,
        ]);
        $builder->add('anchorText', TextType::class, [
            'required' => false,
        ]);
        $builder->add('anchorTargetId', TextType::class, [
            'required' => false,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix()
    {
        return 'app_homeheaderpageparttype';
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HomeHeaderPagePart::class,
        ]);
    }
}
