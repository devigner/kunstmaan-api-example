<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\PageParts;

use App\Entity\PageParts\ProgramStarwayPagePart;
use Kunstmaan\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ProgramStarwayPagePartAdminType
 */
class ProgramStarwayPagePartAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array $options The options
     *
     * @see FormTypeExtensionInterface::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('subtitle', TextType::class, [
            'required' => false,
        ]);
        $builder->add('title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('content', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
        $builder->add('registerButton', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star1Image', MediaType::class, [
            'mediatype' => 'image',
            'required' => false,
        ]);
        $builder->add('star1ImageAltText', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star1Title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star1Content', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
        $builder->add('star2Image', MediaType::class, [
            'mediatype' => 'image',
            'required' => false,
        ]);
        $builder->add('star2ImageAltText', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star2Title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star2Content', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
        $builder->add('star3Image', MediaType::class, [
            'mediatype' => 'image',
            'required' => false,
        ]);
        $builder->add('star3ImageAltText', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star3Title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star3Content', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
        $builder->add('star4Image', MediaType::class, [
            'mediatype' => 'image',
            'required' => false,
        ]);
        $builder->add('star4ImageAltText', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star4Title', TextType::class, [
            'required' => false,
        ]);
        $builder->add('star4Content', TextareaType::class, [
            'attr' => ['rows' => 10, 'cols' => 600],
            'required' => false,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix(): string
    {
        return 'app_programstarwaypageparttype';
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProgramStarwayPagePart::class,
        ]);
    }
}
