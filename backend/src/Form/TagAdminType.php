<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form;

use Kunstmaan\ArticleBundle\Form\AbstractTagAdminType;

/**
 * The type for Tag
 */
class TagAdminType extends AbstractTagAdminType
{
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix(): string
    {
        return 'news_tag_form';
    }
}
