<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\User;

use Kunstmaan\AdminBundle\Entity\Role;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Symfony\Component\Form\DataTransformerInterface;

class RolesTransformer implements DataTransformerInterface
{
    use EntityManagerTrait;

    /**
     * @param array $roles
     * @return array|Role[]
     */
    public function transform($roles)
    {
        $repo = $this->entityManager->getRepository(Role::class);

        $transformed = [];
        foreach ($roles as $role) {
            if ('ROLE_USER' === $role) { // ROLE_USER does not exists
                continue;
            }

            $transformed[] = $repo->findOneBy(['role' => $role]);
        }

        return $transformed;
    }

    /**
     * @param array|Role[] $roles
     * @return array
     */
    public function reverseTransform($roles)
    {
        $transformed = [];
        foreach ($roles as $role) {
            $transformed[] = $role->getRole();
        }

        return $transformed;
    }
}
