<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\User;

use App\Entity\User\Company;
use App\Entity\User\User;
use Kunstmaan\AdminBundle\Entity\Role;
use Kunstmaan\AdminBundle\Form\UserType as AdminUserType;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AdminUserType
{
    use EntityManagerTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $transformer = new RolesTransformer();
        $transformer->setEntityManager($this->entityManager);

        $builder
            ->remove('groups')
            ->remove('adminLocale')
            ->add('firstName', TextType::class, [
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('lastName', TextType::class, [
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('locale', ChoiceType::class, [
                'required' => true,
                'expanded' => false,
                'placeholder' => false,
                'choices' => [
                    'settings.user.locale.nl_NL' => 'nl_NL',
                    'settings.user.gender.en_NL' => 'en_NL',
                ],
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('gender', ChoiceType::class, [
                'required' => false,
                'expanded' => false,
                'placeholder' => false,
                'choices' => [
                    'settings.user.gender.male' => 'Male',
                    'settings.user.gender.female' => 'Female',
                ],
                'translation_domain' => 'messages',
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('newsletter', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('telephone', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'required' => false,
                'choice_label' => 'name',
                'attr' => [
                    'readonly' => true,
                    'placeholder' => 'Kies een bedrijf',
                ],
            ])
            ->add('roles', EntityType::class, [
                'class' => Role::class,
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'compound' => true,
                'attr' => [
                    'placeholder' => 'Rollen',
                    'class' => 'js-advanced-select form-control advanced-select',
                ],
            ])
            ->get('roles')->addModelTransformer($transformer)
            ->add('dynamicsId', TextType::class, [
                'label' => 'settings.user.dynamics_id',
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('lastDynamicsSync', TextType::class, [
                'label' => 'Laatste Dynamics Sync',
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('sync', ButtonType::class, [
                'label' => 'Sync User ← Dynamics',
                'attr' => [
                    'onClick' => sprintf('window.open("%s")', sprintf('/api/sync/user/%s', $builder->getData()->getSyncToken())),
                    'class' => 'btn btn-secondary',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
