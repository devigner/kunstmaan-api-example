<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form\User;

use App\Entity\Tag;
use App\Entity\Category;
use App\Entity\User\Company;
use App\Entity\User\Industry;
use App\Entity\User\User;
use Devigner\KunstmaanApiBundle\Traits\EntityManagerTrait;
use Devigner\KunstmaanApiBundle\Traits\RouterTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    use EntityManagerTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('street', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('postalCode', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('organisationType', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('industry', EntityType::class, [
                'class' => Industry::class,
                'expanded' => false,
                'required' => false,
                #'attr' => [
                #    'placeholder' => 'settings.company.themes_placeholder',
                #    'class' => 'js-advanced-select form-control advanced-select',
                #],
            ])
            ->add('themes', EntityType::class, [
                'class' => Category::class,
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'settings.company.themes_placeholder',
                    'class' => 'js-advanced-select form-control advanced-select',
                ],
            ])
            ->add('tags', EntityType::class, [
                'class' => Tag::class,
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'settings.company.themes_placeholder',
                    'class' => 'js-advanced-select form-control advanced-select',
                ],
            ])
            ->add('enabled', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                ],
            ])
            ->add('leanAndGreenEnabled', CheckboxType::class, [
                'required' => false,
                'disabled' => true,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('chamberOfCommerceNumber', TextType::class, [
                'label' => 'Kvk Nummer',
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('users', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'disabled' => true,
                'attr' => [
                    'readonly' => true,
                    'placeholder' => 'settings.company.users_placeholder',
                    'class' => 'js-advanced-select form-control advanced-select',
                ],
            ])
            ->add('dynamicsId', TextType::class, [
                'label' => 'Company ID@MS Dynamics',
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('lastDynamicsSync', TextType::class, [
                'label' => 'settings.user.last_dynamics_sync',
                'required' => false,
                'attr' => [
                    'class' => 'dynamics',
                    'readonly' => true,
                ],
            ])
            ->add('sync', ButtonType::class, [
                'label' => 'settings.user.sync',
                'attr' => [
                    'onClick' => sprintf('window.open("%s")', sprintf('/api/sync/company/%s', $builder->getData()->getSyncToken())),
                    'class' => 'btn btn-secondary',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return Company::class;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
