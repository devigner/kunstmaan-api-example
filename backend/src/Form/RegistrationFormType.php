<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Form;

use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'registration.form.email'])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'first_options' => ['label' => 'registration.form.password'],
                'second_options' => ['label' => 'registration.form.password_confirmation'],
                'invalid_message' => 'registration.password.mismatch',
            ])
            ->add('username', TextType::class, ['label' => 'registration.form.username'])
            ->add('firstName', TextType::class, ['label' => 'registration.form.firstName'])
            ->add('lastName', TextType::class, ['label' => 'registration.form.lastName'])
            ->add('locale', TextType::class, ['label' => 'registration.form.locale']);
    }

    public function getBlockPrefix(): string
    {
        return 'app_user_registration';
    }
}
