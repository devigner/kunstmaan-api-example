<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CompanyRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Company|null
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?Company
    {
        $criteria['deletedAt'] = null;
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * @param string $email
     * @return Company|null
     */
    public function findUserByEmail(string $email): ?Company
    {
        $result = $this->findBy(['email' => $email, 'deletedAt' => null]);
        if (0 === count($result)) {
            return null;
        }

        return $result[0];
    }
}
