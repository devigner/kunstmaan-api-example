<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Repository;

use Kunstmaan\ArticleBundle\Repository\AbstractArticleOverviewPageRepository;

/**
 * Repository class for the NewsOverviewPage
 */
class NewsOverviewPageRepository extends AbstractArticleOverviewPageRepository
{
}
