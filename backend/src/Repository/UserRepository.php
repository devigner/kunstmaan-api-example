<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User\User;
use Kunstmaan\AdminBundle\Repository\UserRepository as KunstmaanUserRepository;

class UserRepository extends KunstmaanUserRepository
{
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return object|null
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        $criteria['deletedAt'] = null;
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findUserByEmail(string $email): ?User
    {
        $result = $this->findBy(['email' => $email, 'deletedAt' => null]);
        if (0 === count($result)) {
            return null;
        }

        return $result[0];
    }
}
