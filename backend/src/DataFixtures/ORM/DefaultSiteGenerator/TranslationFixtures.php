<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\DataFixtures\ORM\DefaultSiteGenerator;

use Doctrine\Common\Persistence\ObjectManager;
use Kunstmaan\TranslatorBundle\DataFixtures\ORM\TranslationFixtures as KumaTranslationFixtures;

class TranslationFixtures extends KumaTranslationFixtures
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        return; // Disabled on purpose
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 3;
    }
}
