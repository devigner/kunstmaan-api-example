<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\DataFixtures\ORM\DefaultSiteGenerator;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Kunstmaan\AdminBundle\Entity\DashboardConfiguration;
use Kunstmaan\FixturesBundle\Builder\BuildingSupervisor;
use Kunstmaan\FixturesBundle\Loader\Fixture;
use Kunstmaan\FixturesBundle\Parser\Parser as FixtureParser;
use Kunstmaan\MediaBundle\Helper\Services\MediaCreatorService;
use Kunstmaan\NodeBundle\Helper\Services\PageCreatorService;
use Kunstmaan\PagePartBundle\Helper\Services\PagePartCreatorService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Parser;

/**
 * DefaultSiteFixtures
 */
class DefaultSiteFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container = null;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var PageCreatorService
     */
    private $pageCreator;

    /**
     * @var PagePartCreatorService
     */
    private $pagePartCreator;

    /**
     * @var MediaCreatorService
     */
    private $mediaCreator;

    /**
     * Defined locales during generation
     */
    private $requiredLocales;

    /**
     * @return array
     */
    protected function getFixtures(): array
    {
        return [
            __DIR__ . '/yaml/user.yaml',
            __DIR__ . '/yaml/1.homepage.yaml',
            __DIR__ . '/yaml/2.deelnemen.yaml',
            __DIR__ . '/yaml/3.nieuws-events.yaml',
            __DIR__ . '/yaml/4.maatregelen.yaml',
            __DIR__ . '/yaml/5.deelnemers.yaml',
            __DIR__ . '/yaml/themes.yaml',
            __DIR__ . '/yaml/translations.yaml',
            __DIR__ . '/yaml/menu.yaml',
        ];
    }

    /**
     * @var BuildingSupervisor
     */
    protected $buildingSupervisor;

    /**
     * @var FixtureParser
     */
    protected $parser;

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->pageCreator = $this->container->get('kunstmaan_node.page_creator_service');
        $this->pagePartCreator = $this->container->get('kunstmaan_pageparts.pagepart_creator_service');
        $this->mediaCreator = $this->container->get('kunstmaan_media.media_creator_service');
        $this->parser = $this->container->get('kunstmaan_fixtures.parser.parser');
        $this->buildingSupervisor = $this->container->get('kunstmaan_fixtures.builder.builder');
        $this->requiredLocales = explode('|', $this->container->getParameter('requiredlocales'));

        $this->createDashboard();
        $parser = new Parser();
        $data = [];
        foreach ($this->getFixtures() as $fixture) {
            $data[] = $parser->parse(file_get_contents($fixture));
        }

        $fixtures = $this->initFixtures($data);
        $this->buildingSupervisor->addProvider($this);

        /*
         * because of faker's magic calls we'll want to add this as last provider
         */
        $this->buildingSupervisor->addProvider(Factory::create('nl_NL'));
        $this->buildingSupervisor->setFixtures($fixtures);
        $this->buildingSupervisor->buildFixtures($manager);
    }

    public function adminUser(): string
    {
        return 'superadmin';
    }

    /**
     * Parse specs and initiate fixtures
     *
     * @param $data
     *
     * @return array|mixed
     */
    private function initFixtures($data)
    {
        $fixtures = [];
        foreach ($data as $file) {
            foreach ($file as $class => $specs) {
                foreach ($specs as $name => $options) {
                    $fixture = new Fixture($name, $class, $options);
                    $fixtures = $this->parser->parseSpec($name, $fixture, $fixtures);
                }
            }
        }

        return $fixtures;
    }

    /**
     * Create the dashboard
     */
    private function createDashboard()
    {
        /** @var $dashboard DashboardConfiguration */
        $dashboard = $this->manager->getRepository(DashboardConfiguration::class)->findOneBy([]);
        if (null === $dashboard) {
            $dashboard = new DashboardConfiguration();
        }
        $dashboard->setTitle('Dashboard');
        $dashboard->setContent('Welkom');
        $this->manager->persist($dashboard);
        $this->manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 52;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }
}
