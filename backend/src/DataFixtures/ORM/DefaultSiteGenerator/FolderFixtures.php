<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */


namespace App\DataFixtures\ORM\DefaultSiteGenerator;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Kunstmaan\MediaBundle\Entity\Folder;

/**
 * Fixtures that make a general media-folder for a project
 * and for every type of media a folder in that media-folder
 */
class FolderFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        return;
        $this->manager = $manager;

        $gal = $this->createFolder();

        $subgal = new Folder();
        $subgal->setParent($gal);
        $subgal->setRel(Folder::TYPE_IMAGE);
        $subgal->setName('Images');
        $subgal->setTranslatableLocale('en_NL');
        $manager->persist($subgal);
        $manager->flush();

        $subgal->setTranslatableLocale('nl_NL');
        $manager->refresh($subgal);
        $subgal->setName('Afbeeldingen');
        $manager->persist($subgal);
        $manager->flush();

        $subgal = new Folder();
        $subgal->setParent($gal);
        $subgal->setRel(Folder::TYPE_FILES);
        $subgal->setName('Files');
        $subgal->setTranslatableLocale('en_NL');
        $manager->persist($subgal);
        $manager->flush();
        //$this->addReference('files-folder-en', $subgal);

        $subgal->setTranslatableLocale('nl_NL');
        $manager->refresh($subgal);
        $subgal->setName('Bestanden');
        $manager->persist($subgal);
        $manager->flush();

        $subgal = new Folder();
        $subgal->setParent($gal);
        $subgal->setRel(Folder::TYPE_SLIDESHOW);
        $subgal->setName('Slides');
        $subgal->setTranslatableLocale('en_NL');
        $manager->persist($subgal);
        $manager->flush();
        //$this->addReference('slides-folder-en', $subgal);

        $subgal->setTranslatableLocale('nl_NL');
        $manager->refresh($subgal);
        $subgal->setName('Presentaties');
        $manager->persist($subgal);
        $manager->flush();

        $subgal = new Folder();
        $subgal->setParent($gal);
        $subgal->setRel(Folder::TYPE_VIDEO);
        $subgal->setName('Videos');
        $subgal->setTranslatableLocale('en_NL');
        $manager->persist($subgal);
        $manager->flush();
        //$this->addReference('videos-folder-en', $subgal);

        $subgal->setTranslatableLocale('nl_NL');
        $manager->refresh($subgal);
        $subgal->setName('Video\'s');
        $manager->persist($subgal);
        $manager->flush();
    }

    protected function createFolder(): Folder
    {
        $gal = new Folder();
        $gal->setRel(Folder::TYPE_MEDIA);
        $gal->setName('Media');
        $gal->setTranslatableLocale('en_NL');
        $this->manager->persist($gal);
        $this->manager->flush();

        $gal->setTranslatableLocale('nl_NL');
        $this->manager->refresh($gal);
        $gal->setName('Media');
        $this->manager->persist($gal);
        $this->manager->flush();

        return $gal;
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}
