<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\DataFixtures\ORM\DefaultSiteGenerator;

use Doctrine\Common\Persistence\ObjectManager;
use Kunstmaan\GeneratorBundle\DataFixtures\ORM\UserFixtures as KumaUserFixtures;

class UserFixtures extends KumaUserFixtures
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        return; // Disabled on purpose
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 3;
    }
}
