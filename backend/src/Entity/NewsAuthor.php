<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity;

use App\Form\NewsAuthorAdminType;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\ArticleBundle\Entity\AbstractAuthor;

/**
 * The author for a News
 *
 * @ORM\Entity()
 * @ORM\Table(name="app_news_authors")
 */
class NewsAuthor extends AbstractAuthor
{
    /**
     * Returns the default backend form type for this page
     *
     * @return string
     */
    public function getAdminType(): string
    {
        return NewsAuthorAdminType::class;
    }
}
