<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity;

use App\Form\TagAdminType;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Kunstmaan\ArticleBundle\Entity\AbstractTag;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_tags", uniqueConstraints={@ORM\UniqueConstraint(name="name_idx", columns={"name"})})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Tag extends AbstractTag
{
    /**
     * Returns the default backend form type for this page
     *
     * @return string
     */
    public function getAdminType(): string
    {
        return TagAdminType::class;
    }
}
