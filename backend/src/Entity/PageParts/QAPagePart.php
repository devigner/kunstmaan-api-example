<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Entity\MaxResultsTrait;
use App\Form\PageParts\QAPagePartAdminType;
use App\Model;
use App\Provider\QuestionAnswerProvider;
use Doctrine\ORM\Mapping as ORM;
use Devigner\KunstmaanApiBundle\Entity\EntityInjectionInterface;
use Devigner\KunstmaanApiBundle\Entity\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @ORM\Table(name="app_qa_page_parts")
 * @ORM\Entity
 */
class QAPagePart extends AbstractPagePart implements PagePartsModelInterface, EntityInjectionInterface
{
    use MaxResultsTrait;

    /**
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/QAPagePart/view.html.twig';
    }

    /**
     * @return string[]|array
     */
    public function getProvider(): array
    {
        return [QuestionAnswerProvider::class];
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return QAPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\QAPagePart($this);
    }
}
