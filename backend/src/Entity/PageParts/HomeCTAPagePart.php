<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */


namespace App\Entity\PageParts;

use App\Form\PageParts\HomeCTAPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\PagePartBundle\Entity\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * HomeCTAPagePart
 *
 * @ORM\Table(name="app_home_c_t_a_page_parts")
 * @ORM\Entity
 */
class HomeCTAPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="register_title", type="string", length=255, nullable=true)
     */
    private $registerTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="register_button", type="string", length=255, nullable=true)
     */
    private $registerButton;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_title", type="string", length=255, nullable=true)
     */
    private $contactTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_button", type="string", length=255, nullable=true)
     */
    private $contactButton;


    /**
     * Set registerTitle.
     *
     * @param string|null $registerTitle
     *
     * @return HomeCTAPagePart
     */
    public function setRegisterTitle($registerTitle = null)
    {
        $this->registerTitle = $registerTitle;

        return $this;
    }

    /**
     * Get registerTitle.
     *
     * @return string|null
     */
    public function getRegisterTitle()
    {
        return $this->registerTitle;
    }

    /**
     * Set registerButton.
     *
     * @param string|null $registerButton
     *
     * @return HomeCTAPagePart
     */
    public function setRegisterButton($registerButton = null)
    {
        $this->registerButton = $registerButton;

        return $this;
    }

    /**
     * Get registerButton.
     *
     * @return string|null
     */
    public function getRegisterButton()
    {
        return $this->registerButton;
    }

    /**
     * Set contactTitle.
     *
     * @param string|null $contactTitle
     *
     * @return HomeCTAPagePart
     */
    public function setContactTitle($contactTitle = null)
    {
        $this->contactTitle = $contactTitle;

        return $this;
    }

    /**
     * Get contactTitle.
     *
     * @return string|null
     */
    public function getContactTitle()
    {
        return $this->contactTitle;
    }

    /**
     * Set contactButton.
     *
     * @param string|null $contactButton
     *
     * @return HomeCTAPagePart
     */
    public function setContactButton($contactButton = null)
    {
        $this->contactButton = $contactButton;

        return $this;
    }

    /**
     * Get contactButton.
     *
     * @return string|null
     */
    public function getContactButton()
    {
        return $this->contactButton;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'PageParts/HomeCTAPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType()
    {
        return HomeCTAPagePartAdminType::class;
    }

    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\HomeCTAPagePart($this);
    }
}
