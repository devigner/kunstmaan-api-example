<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Form\PageParts\ProgramStarwayPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\MediaBundle\Entity\Media;
use Kunstmaan\PagePartBundle\Entity\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * ProgramStarwayPagePart
 *
 * @ORM\Table(name="app_program_starway_page_parts")
 * @ORM\Entity
 */
class ProgramStarwayPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="register_button", type="string", length=255, nullable=true)
     */
    private $registerButton;

    /**
     * @var string|null
     *
     * @ORM\Column(name="startitle", type="string", length=255, nullable=true)
     */
    private $starTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star1title", type="string", length=255, nullable=true)
     */
    private $star1Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star1content", type="text", nullable=true)
     */
    private $star1Content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star2title", type="string", length=255, nullable=true)
     */
    private $star2Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star2content", type="text", nullable=true)
     */
    private $star2Content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star3title", type="string", length=255, nullable=true)
     */
    private $star3Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star3content", type="text", nullable=true)
     */
    private $star3Content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star4title", type="string", length=255, nullable=true)
     */
    private $star4Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star4content", type="text", nullable=true)
     */
    private $star4Content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="starimage_alt_text", type="text", nullable=true)
     */
    private $starImageAltText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star1image_alt_text", type="text", nullable=true)
     */
    private $star1ImageAltText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star2image_alt_text", type="text", nullable=true)
     */
    private $star2ImageAltText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star3image_alt_text", type="text", nullable=true)
     */
    private $star3ImageAltText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="star4image_alt_text", type="text", nullable=true)
     */
    private $star4ImageAltText;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="starimage_id", referencedColumnName="id")
     * })
     */
    private $starImage;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="star1image_id", referencedColumnName="id")
     * })
     */
    private $star1Image;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="star2image_id", referencedColumnName="id")
     * })
     */
    private $star2Image;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="star3image_id", referencedColumnName="id")
     * })
     */
    private $star3Image;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="star4image_id", referencedColumnName="id")
     * })
     */
    private $star4Image;

    /**
     * Set subtitle.
     *
     * @param string|null $subtitle
     *
     * @return ProgramStarwayPagePart
     */
    public function setSubtitle($subtitle = null): ProgramStarwayPagePart
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle.
     *
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return ProgramStarwayPagePart
     */
    public function setTitle($title = null): ProgramStarwayPagePart
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return ProgramStarwayPagePart
     */
    public function setContent($content = null): ProgramStarwayPagePart
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set registerButton.
     *
     * @param string|null $registerButton
     *
     * @return ProgramStarwayPagePart
     */
    public function setRegisterButton($registerButton = null): ProgramStarwayPagePart
    {
        $this->registerButton = $registerButton;

        return $this;
    }

    /**
     * Get registerButton.
     *
     * @return string|null
     */
    public function getRegisterButton(): ?string
    {
        return $this->registerButton;
    }

    /**
     * Set star1Title.
     *
     * @param string|null $star1Title
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar1Title($star1Title = null): ProgramStarwayPagePart
    {
        $this->star1Title = $star1Title;

        return $this;
    }

    /**
     * Get star1Title.
     *
     * @return string|null
     */
    public function getStar1Title(): ?string
    {
        return $this->star1Title;
    }

    /**
     * Set star1Content.
     *
     * @param string|null $star1Content
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar1Content($star1Content = null): ProgramStarwayPagePart
    {
        $this->star1Content = $star1Content;

        return $this;
    }

    /**
     * Get star1Content.
     *
     * @return string|null
     */
    public function getStar1Content(): ?string
    {
        return $this->star1Content;
    }

    /**
     * Set star2Title.
     *
     * @param string|null $star2Title
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar2Title($star2Title = null): ProgramStarwayPagePart
    {
        $this->star2Title = $star2Title;

        return $this;
    }

    /**
     * Get star2Title.
     *
     * @return string|null
     */
    public function getStar2Title(): ?string
    {
        return $this->star2Title;
    }

    /**
     * Set star2Content.
     *
     * @param string|null $star2Content
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar2Content($star2Content = null): ProgramStarwayPagePart
    {
        $this->star2Content = $star2Content;

        return $this;
    }

    /**
     * Get star2Content.
     *
     * @return string|null
     */
    public function getStar2Content(): ?string
    {
        return $this->star2Content;
    }

    /**
     * Set star3Title.
     *
     * @param string|null $star3Title
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar3Title($star3Title = null): ProgramStarwayPagePart
    {
        $this->star3Title = $star3Title;

        return $this;
    }

    /**
     * Get star3Title.
     *
     * @return string|null
     */
    public function getStar3Title(): ?string
    {
        return $this->star3Title;
    }

    /**
     * Set star3Content.
     *
     * @param string|null $star3Content
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar3Content($star3Content = null): ProgramStarwayPagePart
    {
        $this->star3Content = $star3Content;

        return $this;
    }

    /**
     * Get star3Content.
     *
     * @return string|null
     */
    public function getStar3Content(): ?string
    {
        return $this->star3Content;
    }

    /**
     * Set star4Title.
     *
     * @param string|null $star4Title
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar4Title($star4Title = null): ProgramStarwayPagePart
    {
        $this->star4Title = $star4Title;

        return $this;
    }

    /**
     * Get star4Title.
     *
     * @return string|null
     */
    public function getStar4Title(): ?string
    {
        return $this->star4Title;
    }

    /**
     * Set star4Content.
     *
     * @param string|null $star4Content
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar4Content($star4Content = null): ProgramStarwayPagePart
    {
        $this->star4Content = $star4Content;

        return $this;
    }

    /**
     * Get star4Content.
     *
     * @return string|null
     */
    public function getStar4Content(): ?string
    {
        return $this->star4Content;
    }

    /**
     * Set star1ImageAltText.
     *
     * @param string|null $star1ImageAltText
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar1ImageAltText($star1ImageAltText = null): ProgramStarwayPagePart
    {
        $this->star1ImageAltText = $star1ImageAltText;

        return $this;
    }

    /**
     * Get star1ImageAltText.
     *
     * @return string|null
     */
    public function getStar1ImageAltText(): ?string
    {
        return $this->star1ImageAltText;
    }

    /**
     * Set star2ImageAltText.
     *
     * @param string|null $star2ImageAltText
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar2ImageAltText($star2ImageAltText = null): ProgramStarwayPagePart
    {
        $this->star2ImageAltText = $star2ImageAltText;

        return $this;
    }

    /**
     * Get star2ImageAltText.
     *
     * @return string|null
     */
    public function getStar2ImageAltText(): ?string
    {
        return $this->star2ImageAltText;
    }

    /**
     * Set star3ImageAltText.
     *
     * @param string|null $star3ImageAltText
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar3ImageAltText($star3ImageAltText = null): ProgramStarwayPagePart
    {
        $this->star3ImageAltText = $star3ImageAltText;

        return $this;
    }

    /**
     * Get star3ImageAltText.
     *
     * @return string|null
     */
    public function getStar3ImageAltText(): ?string
    {
        return $this->star3ImageAltText;
    }

    /**
     * Set star4ImageAltText.
     *
     * @param string|null $star4ImageAltText
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar4ImageAltText($star4ImageAltText = null): ProgramStarwayPagePart
    {
        $this->star4ImageAltText = $star4ImageAltText;

        return $this;
    }

    /**
     * Get star4ImageAltText.
     *
     * @return string|null
     */
    public function getStar4ImageAltText(): ?string
    {
        return $this->star4ImageAltText;
    }

    /**
     * Set star1Image.
     *
     * @param Media|null $star1Image
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar1Image(Media $star1Image = null): ProgramStarwayPagePart
    {
        $this->star1Image = $star1Image;

        return $this;
    }

    /**
     * Get star1Image.
     *
     * @return Media|null
     */
    public function getStar1Image(): ?Media
    {
        return $this->star1Image;
    }

    /**
     * Set star2Image.
     *
     * @param Media|null $star2Image
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar2Image(Media $star2Image = null): ProgramStarwayPagePart
    {
        $this->star2Image = $star2Image;

        return $this;
    }

    /**
     * Get star2Image.
     *
     * @return Media|null
     */
    public function getStar2Image(): ?Media
    {
        return $this->star2Image;
    }

    /**
     * Set star3Image.
     *
     * @param Media|null $star3Image
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar3Image(Media $star3Image = null): ProgramStarwayPagePart
    {
        $this->star3Image = $star3Image;

        return $this;
    }

    /**
     * Get star3Image.
     *
     * @return Media|null
     */
    public function getStar3Image(): ?Media
    {
        return $this->star3Image;
    }

    /**
     * Set star4Image.
     *
     * @param Media|null $star4Image
     *
     * @return ProgramStarwayPagePart
     */
    public function setStar4Image(Media $star4Image = null): ProgramStarwayPagePart
    {
        $this->star4Image = $star4Image;

        return $this;
    }

    /**
     * Get star4Image.
     *
     * @return Media|null
     */
    public function getStar4Image(): ?Media
    {
        return $this->star4Image;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/ProgramStarwayPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return ProgramStarwayPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\ProgramStarwayPagePart($this);
    }
}
