<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Entity\Category;
use App\Form\PageParts\CategoryPagePartAdminType;
use App\Model;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Devigner\KunstmaanApiBundle\Entity\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @ORM\Table(name="app_category_page_parts")
 * @ORM\Entity
 */
class CategoryPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="listTitle", type="text", nullable=true)
     */
    private $listTitle;

    /**
     * @var Collection|Category[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Category")
     * @ORM\JoinTable(name="app_category_page_parts_app_theme",
     *   joinColumns={@ORM\JoinColumn(name="category_part_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return CategoryPagePart
     */
    public function setTitle($title = null): CategoryPagePart
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return CategoryPagePart
     */
    public function setContent($content = null): CategoryPagePart
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set title.
     *
     * @param string|null $listTitle
     *
     * @return CategoryPagePart
     */
    public function setListTitle($listTitle = null): CategoryPagePart
    {
        $this->listTitle = $listTitle;

        return $this;
    }

    /**
     * Get listTitle.
     *
     * @return string|null
     */
    public function getListTitle(): ?string
    {
        return $this->listTitle;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/CategoryPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return CategoryPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\CategoryPagePart($this);
    }
}
