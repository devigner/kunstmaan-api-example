<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Form\PageParts\ToTopPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Devigner\KunstmaanApiBundle\Entity\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * @ORM\Table(name="app_to_top_page_parts")
 * @ORM\Entity
 */
class ToTopPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/ToTopPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return ToTopPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\ToTopPagePart($this);
    }
}
