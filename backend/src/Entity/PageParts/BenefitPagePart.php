<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */


namespace App\Entity\PageParts;

use App\Model;
use App\Entity\MaxResultsTrait;
use App\Form\PageParts\BenefitPagePartAdminType;
use App\Provider\BenefitProvider;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\PagePartBundle\Entity\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\EntityInjectionInterface;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * BenefitPagePart
 *
 * @ORM\Table(name="app_benefit_page_parts")
 * @ORM\Entity
 */
class BenefitPagePart extends AbstractPagePart implements PagePartsModelInterface, EntityInjectionInterface
{
    use MaxResultsTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="show_all_button", type="string", length=255, nullable=true)
     */
    private $showAllButton;

    /**
     * Set showAllButton.
     *
     * @param string|null $showAllButton
     *
     * @return BenefitPagePart
     */
    public function setShowAllButton($showAllButton = null): BenefitPagePart
    {
        $this->showAllButton = $showAllButton;

        return $this;
    }

    /**
     * Get showAllButton.
     *
     * @return string|null
     */
    public function getShowAllButton()
    {
        return $this->showAllButton;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'PageParts/BenefitPagePart/view.html.twig';
    }

    /**
     * @return string[]|array
     */
    public function getProvider(): array
    {
        return [BenefitProvider::class];
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType()
    {
        return BenefitPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\BenefitPagePart($this);
    }
}
