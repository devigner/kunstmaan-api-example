<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Form\PageParts\ProgramLeanGreenPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\PagePartBundle\Entity\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * ProgramLeanGreenPagePart
 *
 * @ORM\Table(name="app_program_lean_green_page_parts")
 * @ORM\Entity
 */
class ProgramLeanGreenPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="lean_title", type="string", length=255, nullable=true)
     */
    private $leanTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lean_content", type="text", nullable=true)
     */
    private $leanContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="green_title", type="string", length=255, nullable=true)
     */
    private $greenTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="green_content", type="text", nullable=true)
     */
    private $greenContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;


    /**
     * Set leanTitle.
     *
     * @param string|null $leanTitle
     *
     * @return ProgramLeanGreenPagePart
     */
    public function setLeanTitle($leanTitle = null): ProgramLeanGreenPagePart
    {
        $this->leanTitle = $leanTitle;

        return $this;
    }

    /**
     * Get leanTitle.
     *
     * @return string|null
     */
    public function getLeanTitle(): ?string
    {
        return $this->leanTitle;
    }

    /**
     * Set leanContent.
     *
     * @param string|null $leanContent
     *
     * @return ProgramLeanGreenPagePart
     */
    public function setLeanContent($leanContent = null): ProgramLeanGreenPagePart
    {
        $this->leanContent = $leanContent;

        return $this;
    }

    /**
     * Get leanContent.
     *
     * @return string|null
     */
    public function getLeanContent(): ?string
    {
        return $this->leanContent;
    }

    /**
     * Set greenTitle.
     *
     * @param string|null $greenTitle
     *
     * @return ProgramLeanGreenPagePart
     */
    public function setGreenTitle($greenTitle = null): ProgramLeanGreenPagePart
    {
        $this->greenTitle = $greenTitle;

        return $this;
    }

    /**
     * Get greenTitle.
     *
     * @return string|null
     */
    public function getGreenTitle(): ?string
    {
        return $this->greenTitle;
    }

    /**
     * Set greenContent.
     *
     * @param string|null $greenContent
     *
     * @return ProgramLeanGreenPagePart
     */
    public function setGreenContent($greenContent = null): ProgramLeanGreenPagePart
    {
        $this->greenContent = $greenContent;

        return $this;
    }

    /**
     * Get greenContent.
     *
     * @return string|null
     */
    public function getGreenContent(): ?string
    {
        return $this->greenContent;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return ProgramLeanGreenPagePart
     */
    public function setTitle($title = null): ProgramLeanGreenPagePart
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/ProgramLeanGreenPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return ProgramLeanGreenPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\ProgramLeanGreenPagePart($this);
    }
}
