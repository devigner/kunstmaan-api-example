<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Form\PageParts\HomeStatisticsPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Devigner\KunstmaanApiBundle\Entity\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * HomeStatisticsPagePart
 *
 * @ORM\Table(name="app_home_statistics_page_parts")
 * @ORM\Entity
 */
class HomeStatisticsPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic1title", type="string", length=255, nullable=true)
     */
    private $statistic1Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic1value", type="string", length=255, nullable=true)
     */
    private $statistic1Value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic1text", type="text", nullable=true)
     */
    private $statistic1Text;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic2title", type="string", length=255, nullable=true)
     */
    private $statistic2Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic2value", type="string", length=255, nullable=true)
     */
    private $statistic2Value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic2text", type="text", nullable=true)
     */
    private $statistic2Text;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic3title", type="string", length=255, nullable=true)
     */
    private $statistic3Title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic3value", type="string", length=255, nullable=true)
     */
    private $statistic3Value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statistic3text", type="text", nullable=true)
     */
    private $statistic3Text;


    /**
     * Set statistic1Title.
     *
     * @param string|null $statistic1Title
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic1Title($statistic1Title = null)
    {
        $this->statistic1Title = $statistic1Title;

        return $this;
    }

    /**
     * Get statistic1Title.
     *
     * @return string|null
     */
    public function getStatistic1Title()
    {
        return $this->statistic1Title;
    }

    /**
     * Set statistic1Value.
     *
     * @param string|null $statistic1Value
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic1Value($statistic1Value = null)
    {
        $this->statistic1Value = $statistic1Value;

        return $this;
    }

    /**
     * Get statistic1Value.
     *
     * @return string|null
     */
    public function getStatistic1Value()
    {
        return $this->statistic1Value;
    }

    /**
     * Set statistic1Text.
     *
     * @param string|null $statistic1Text
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic1Text($statistic1Text = null)
    {
        $this->statistic1Text = $statistic1Text;

        return $this;
    }

    /**
     * Get statistic1Text.
     *
     * @return string|null
     */
    public function getStatistic1Text()
    {
        return $this->statistic1Text;
    }

    /**
     * Set statistic2Title.
     *
     * @param string|null $statistic2Title
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic2Title($statistic2Title = null)
    {
        $this->statistic2Title = $statistic2Title;

        return $this;
    }

    /**
     * Get statistic2Title.
     *
     * @return string|null
     */
    public function getStatistic2Title()
    {
        return $this->statistic2Title;
    }

    /**
     * Set statistic2Value.
     *
     * @param string|null $statistic2Value
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic2Value($statistic2Value = null)
    {
        $this->statistic2Value = $statistic2Value;

        return $this;
    }

    /**
     * Get statistic2Value.
     *
     * @return string|null
     */
    public function getStatistic2Value()
    {
        return $this->statistic2Value;
    }

    /**
     * Set statistic2Text.
     *
     * @param string|null $statistic2Text
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic2Text($statistic2Text = null)
    {
        $this->statistic2Text = $statistic2Text;

        return $this;
    }

    /**
     * Get statistic2Text.
     *
     * @return string|null
     */
    public function getStatistic2Text()
    {
        return $this->statistic2Text;
    }

    /**
     * Set statistic3Title.
     *
     * @param string|null $statistic3Title
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic3Title($statistic3Title = null)
    {
        $this->statistic3Title = $statistic3Title;

        return $this;
    }

    /**
     * Get statistic3Title.
     *
     * @return string|null
     */
    public function getStatistic3Title()
    {
        return $this->statistic3Title;
    }

    /**
     * Set statistic3Value.
     *
     * @param string|null $statistic3Value
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic3Value($statistic3Value = null)
    {
        $this->statistic3Value = $statistic3Value;

        return $this;
    }

    /**
     * Get statistic3Value.
     *
     * @return string|null
     */
    public function getStatistic3Value()
    {
        return $this->statistic3Value;
    }

    /**
     * Set statistic3Text.
     *
     * @param string|null $statistic3Text
     *
     * @return HomeStatisticsPagePart
     */
    public function setStatistic3Text($statistic3Text = null)
    {
        $this->statistic3Text = $statistic3Text;

        return $this;
    }

    /**
     * Get statistic3Text.
     *
     * @return string|null
     */
    public function getStatistic3Text()
    {
        return $this->statistic3Text;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView()
    {
        return 'PageParts/HomeStatisticsPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType()
    {
        return HomeStatisticsPagePartAdminType::class;
    }

    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\HomeStatisticsPagePart($this);
    }
}
