<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\PageParts;

use App\Form\PageParts\HomeHeaderPagePartAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\MediaBundle\Entity\Media;
use Devigner\KunstmaanApiBundle\Entity\PageParts\AbstractPagePart;
use Devigner\KunstmaanApiBundle\Entity\PagePartsModelInterface;
use Devigner\KunstmaanApiBundle\Model\PagePartsEntityInterface;

/**
 * HomeHeaderPagePart
 *
 * @ORM\Table(name="app_home_header_page_parts")
 * @ORM\Entity
 */
class HomeHeaderPagePart extends AbstractPagePart implements PagePartsModelInterface
{
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="img_src_id", referencedColumnName="id")
     * })
     */
    private $imgSrc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="anchor_text", type="string", length=255, nullable=true)
     */
    private $anchorText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="anchor_target_id", type="string", length=255, nullable=true)
     */
    private $anchorTargetId;

    /**
     * Set imgSrc.
     *
     * @param Media|null $imgSrc
     *
     * @return HomeHeaderPagePart
     */
    public function setImgSrc(Media $imgSrc = null)
    {
        $this->imgSrc = $imgSrc;

        return $this;
    }

    /**
     * Get imgSrc.
     *
     * @return Media|null
     */
    public function getImgSrc(): ?Media
    {
        return $this->imgSrc;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return HomeHeaderPagePart
     */
    public function setTitle($title = null): HomeHeaderPagePart
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set subtitle.
     *
     * @param string|null $subtitle
     *
     * @return HomeHeaderPagePart
     */
    public function setSubtitle($subtitle = null): HomeHeaderPagePart
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle.
     *
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * Set anchorText.
     *
     * @param string|null $anchorText
     *
     * @return HomeHeaderPagePart
     */
    public function setAnchorText($anchorText = null): HomeHeaderPagePart
    {
        $this->anchorText = $anchorText;

        return $this;
    }

    /**
     * Get anchorText.
     *
     * @return string|null
     */
    public function getAnchorText(): ?string
    {
        return $this->anchorText;
    }

    /**
     * Set AnchorTargetId.
     *
     * @param string|null $anchorTargetId
     *
     * @return HomeHeaderPagePart
     */
    public function setAnchorTargetId($anchorTargetId = null): HomeHeaderPagePart
    {
        $this->anchorTargetId = $anchorTargetId;

        return $this;
    }

    /**
     * Get AnchorTargetId.
     *
     * @return string|null
     */
    public function getAnchorTargetId(): ?string
    {
        return $this->anchorTargetId;
    }

    /**
     * Get the twig view.
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'PageParts/HomeHeaderPagePart/view.html.twig';
    }

    /**
     * Get the admin form type.
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return HomeHeaderPagePartAdminType::class;
    }

    /**
     * @return PagePartsEntityInterface
     */
    public function getModel(): PagePartsEntityInterface
    {
        return new Model\PageParts\HomeHeaderPagePart($this);
    }
}
