<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Pages\NewsPage;
use App\Model;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Kunstmaan\AdminBundle\Entity\AbstractEntity;
use Devigner\KunstmaanApiBundle\Entity\PageModelInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_news_comment")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class NewsComment extends AbstractEntity implements EntityModelInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var NewsPage
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pages\NewsPage", inversedBy="comments")
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     */
    private $newsPage;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @return NewsPage
     */
    public function getNewsPage(): NewsPage
    {
        return $this->newsPage;
    }

    /**
     * @param NewsPage $newsPage
     */
    public function setNewsPage(NewsPage $newsPage): void
    {
        $this->newsPage = $newsPage;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    public function getModel(): Model\ModelInterface
    {
        return new Model\NewsComment($this);
    }
}
