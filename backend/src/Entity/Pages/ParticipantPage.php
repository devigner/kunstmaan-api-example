<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\Pages;

use App\Form\Pages\ParticipantPageAdminType;
use App\Model;
use App\Provider\ParticipantPageProvider;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\NodeBundle\Entity\AbstractPage;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeSearchBundle\Helper\SearchTypeInterface;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use Kunstmaan\PagePartBundle\PagePartAdmin\AbstractPagePartAdminConfigurator;
use Devigner\KunstmaanApiBundle\Entity\PageModelInterface;
use Devigner\KunstmaanApiBundle\Entity\EntityInjectionInterface;
use Devigner\KunstmaanApiBundle\Model\ModelSchemaInterface;
use Devigner\KunstmaanApiBundle\Model\PageEntityInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_participant")
 */
class ParticipantPage extends AbstractPage implements HasPageTemplateInterface, SearchTypeInterface, PageModelInterface, EntityInjectionInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $maxResults = 10;

    /**
     * @return int
     */
    public function getMaxResults(): int
    {
        if (null === $this->maxResults) {
            $this->maxResults = 10;
        }

        return $this->maxResults;
    }

    /**
     * @param int $maxResults
     */
    public function setMaxResults(int $maxResults): void
    {
        $this->maxResults = $maxResults;
    }

    /**
     * @return array
     */
    public function getPossibleChildTypes(): array
    {
        return [];
    }

    /**
     * @return AbstractPagePartAdminConfigurator[]
     */
    public function getPagePartAdminConfigurations(): array
    {
        return ['participants'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates(): array
    {
        return ['participantsTemplate'];
    }

    /**
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'Pages/ParticipantPage/view.html.twig';
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchType(): string
    {
        return 'Participant';
    }

    /**
     * Returns the default backend form type for this page
     *
     * @return string
     */
    public function getDefaultAdminType(): string
    {
        return ParticipantPageAdminType::class;
    }

    /**
     * @return array
     */
    public function getSerializeGroups(): ?array
    {
        return ['always'];
    }

    /**
     * @return PageEntityInterface
     */
    public function getModel(): PageEntityInterface
    {
        return new Model\Pages\ParticipantPage($this);
    }

    /**
     * @param PageModelInterface $entityModel
     * @param Node $node
     * @param string $locale
     * @return ModelSchemaInterface
     */
    public function getSchemaModel(PageModelInterface $entityModel, Node $node, string $locale): ModelSchemaInterface
    {
        return new Model\ParticipantSchema($entityModel, $node, $locale);
    }

    /**
     * @return string[]|array
     */
    public function getProvider(): array
    {
        return [ParticipantPageProvider::class];
    }
}
