<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\Pages;

use App\Form\Pages\HomePageAdminType;
use App\Model;
use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\NodeBundle\Entity\AbstractPage;
use Kunstmaan\NodeBundle\Entity\HomePageInterface;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeSearchBundle\Helper\SearchTypeInterface;
use Kunstmaan\PagePartBundle\Helper\HasPageTemplateInterface;
use Devigner\KunstmaanApiBundle\Entity\PageModelInterface;
use Devigner\KunstmaanApiBundle\Model\ModelSchemaInterface;
use Devigner\KunstmaanApiBundle\Model\PageEntityInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_home_pages")
 */
class HomePage extends AbstractPage implements HasPageTemplateInterface, SearchTypeInterface, HomePageInterface, PageModelInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDefaultAdminType(): string
    {
        return HomePageAdminType::class;
    }

    /**
     * @return array
     */
    public function getPossibleChildTypes(): array
    {
        return [
            [
                'name' => 'Pagina',
                'class' => ContentPage::class
            ],
            [
                'name' => 'Formulier',
                'class' => FormPage::class
            ],
            [
                'name' => 'Nieuws overzicht',
                'class' => NewsOverviewPage::class
            ],
            [
                'name' => 'Deelnemers overzicht',
                'class' => ParticipantPage::class
            ],
            [
                'name' => 'Zoek pagina',
                'class' => SearchPage::class
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getPagePartAdminConfigurations(): array
    {
        return ['homePage', 'main'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPageTemplates(): array
    {
        return ['homePageTemplate'];
    }

    /**
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'Pages/HomePage/view.html.twig';
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchType(): string
    {
        return 'Home';
    }

    /**
     * @return array|null
     */
    public function getSerializeGroups(): ?array
    {
        return ['always'];
    }

    /**
     * @return PageEntityInterface
     */
    public function getModel(): PageEntityInterface
    {
        return new Model\Pages\HomePage($this);
    }

    /**
     * @param PageModelInterface $entityModel
     * @param Node $node
     * @param string $locale
     * @return ModelSchemaInterface
     */
    public function getSchemaModel(PageModelInterface $entityModel, Node $node, string $locale): ModelSchemaInterface
    {
        return new Model\HomeSchema($entityModel, $node, $locale);
    }
}
