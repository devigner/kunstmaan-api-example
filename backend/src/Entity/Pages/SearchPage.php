<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity\Pages;

use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\NodeSearchBundle\Entity\AbstractSearchPage;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_search_search_page")
 */
class SearchPage extends AbstractSearchPage
{
    /**
     * return string
     */
    public function getDefaultView(): string
    {
        return 'Pages/SearchPage/view.html.twig';
    }
}
