<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity;

use App\Model\ModelInterface;

interface EntityModelInterface
{
    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface;
}
