<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Entity;

use App\Model;
use App\Entity\User\Company;
use App\Form\CategoryAdminType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Kunstmaan\ArticleBundle\Entity\AbstractCategory;
use Kunstmaan\MediaBundle\Entity\Media;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="app_category", uniqueConstraints={@ORM\UniqueConstraint(name="name_idx", columns={"name"})})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
final class Category extends AbstractCategory implements EntityModelInterface
{
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media", fetch="LAZY")
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="colour", type="string", length=255, nullable=true)
     */
    private $colour;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media", fetch="LAZY")
     */
    private $image;

    /**
     * @var Company[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User\Company", mappedBy="categories")
     */
    private $companies;

    public function __construct()
    {
        $this->companies = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return Media
     */
    public function getIcon(): ?Media
    {
        return $this->icon;
    }

    /**
     * @param Media $icon
     */
    public function setIcon(Media $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getColour(): ?string
    {
        return $this->colour;
    }

    /**
     * @param string $colour
     */
    public function setColour(string $colour): void
    {
        $this->colour = $colour;
    }

    /**
     * @return Media
     */
    public function getImage(): ?Media
    {
        return $this->image;
    }

    /**
     * @param Media $image
     */
    public function setImage(Media $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @param Company $company
     */
    public function addCompany(Company $company): void
    {
        if ($this->companies->contains($company)) {
            return;
        }

        $company->addCategory($this);
        $this->companies->add($company);
    }

    /**
     * @param Company $company
     */
    public function removeCompany(Company $company): void
    {
        if (!$this->companies->contains($company)) {
            return;
        }

        $company->removeCategory($this);
        $this->companies->remove($company);
    }

    /**
     * @return Company[]|Collection
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    /**
     * @param Company[]|Collection $companies
     */
    public function setCompanies(Collection $companies): void
    {
        foreach ($companies as $company) {
            $this->addCompany($company);
        }
    }

    /**
     * @return Model\ModelInterface
     */
    public function getModel(): Model\ModelInterface
    {
        return new Model\Category($this);
    }

    /**
     * Returns the default backend form type for this page
     *
     * @return string
     */
    public function getAdminType(): string
    {
        return CategoryAdminType::class;
    }
}
