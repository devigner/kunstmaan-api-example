<?php

/**
 * Kunstmaan API Example
 *
 * @copyright Devigner / Martijn van Beek
 */

declare(strict_types=1);

namespace App\Resolver;

use Kunstmaan\MediaBundle\Entity\Media as KumaMedia;

class MediaResolver
{
    /**
     * @param KumaMedia|null $media
     * @return string|null
     */
    public static function createPublicUrl(?KumaMedia $media): ?string
    {
        if (null === $media) {
            return null;
        }

        return sprintf('%s%s', $_ENV['BACKEND_HOST'], $media->getUrl());
    }
}
