SHELL:=/bin/bash
OS := $(shell uname)

####### BACKEND PUBLIC GITLAB CI/CD ##########
## UNITTEST
run-backend-unittest:
	@make _write-env TEST_APP_ENV=test TEST_DB_HOST=mysql TEST_DB_USER=root TEST_DB_PASSWD=root TEST_DB_NAME=test_db
	@cp .env backend/.env
	@mkdir -p ~/.composer
	@cp .docker/gitlab-ci/php-fpm/auth.json ~/.composer
	@composer --working-dir=backend install
	@backend/vendor/bin/phpunit --configuration=backend/phpunit.xml.dist --coverage-text --colors=never backend/tests

## TEST
run-backend-build-test:
	@make _write-env
	@make _push-backend-docker DOCKER_FILE=test

run-frontend-build-test:
	@make _write-env
	@make _push-frontend-docker DOCKER_FILE=test

run-webserver-build-test:
	@make _write-env
	@make _push-webserver-docker DOCKER_FILE=test

run-deploy-test:
	@make _write-env
	@make _deploy COMPOSE=./.docker/gitlab-ci/docker-compose-test.yml FLUENTD=test

## ACCEPTANCE
run-backend-build-acceptance:
	@make _write-env
	@make _push-backend-docker DOCKER_FILE=prod

run-frontend-build-acceptance:
	@make _write-env
	@make _push-frontend-docker DOCKER_FILE=prod

run-webserver-build-acceptance:
	@make _write-env
	@make _push-webserver-docker DOCKER_FILE=prod

run-deploy-acceptance:
	@make _write-env
	@make _deploy COMPOSE=./.docker/gitlab-ci/docker-compose-prod.yml FLUENTD=acceptance

## PRODUCTION
run-deploy-production:
	@make _write-env
	@make _docker-login
	@docker pull ${CI_REGISTRY_IMAGE}/backend:acceptance
	@docker tag  ${CI_REGISTRY_IMAGE}/backend:acceptance ${CI_REGISTRY_IMAGE}/backend:${VERSION}
	@docker push ${CI_REGISTRY_IMAGE}/backend:${VERSION}
	@docker rmi  ${CI_REGISTRY_IMAGE}/backend:${VERSION}
	@docker pull ${CI_REGISTRY_IMAGE}/frontend:acceptance
	@docker tag  ${CI_REGISTRY_IMAGE}/frontend:acceptance ${CI_REGISTRY_IMAGE}/frontend:${VERSION}
	@docker push ${CI_REGISTRY_IMAGE}/frontend:${VERSION}
	@docker rmi  ${CI_REGISTRY_IMAGE}/frontend:${VERSION}
	@docker pull ${CI_REGISTRY_IMAGE}/webserver:acceptance
	@docker tag  ${CI_REGISTRY_IMAGE}/webserver:acceptance ${CI_REGISTRY_IMAGE}/webserver:${VERSION}
	@docker push ${CI_REGISTRY_IMAGE}/webserver:${VERSION}
	@docker rmi  ${CI_REGISTRY_IMAGE}/webserver:${VERSION}
	@make _deploy COMPOSE=./.docker/gitlab-ci/docker-compose-prod.yml FLUENTD=production

####### PRIVATE #########

_docker-login:
	@docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}

_replace_env:
	@sed -i -e "s@{{${key}}}@$(subst @,\@,${val})@g" ./.env

## Write environment variables
_write-env:
	@cp .env.dist .env
	@make _replace_env key=ENV                val=${ENV}
	@make _replace_env key=DEPLOY_VERSION     val=${DEPLOY_VERSION}
	@make _replace_env key=VERSION            val=${VERSION}
	@make _replace_env key=JWT_PASSPHRASE     val=${JWT_PASSPHRASE}
	@make _replace_env key=FRONTEND_HOST      val=${${ENV}_FRONTEND_HOST}
	@make _replace_env key=BACKEND_HOST       val=${${ENV}_BACKEND_HOST}
	@make _replace_env key=DB_HOST            val=${${ENV}_DB_HOST}
	@make _replace_env key=DB_NAME            val=${${ENV}_DB_NAME}
	@make _replace_env key=DB_USER            val=${${ENV}_DB_USER}
	@make _replace_env key=DB_PASSWD          val=${${ENV}_DB_PASSWD}
	@make _replace_env key=APP_ENV            val=${${ENV}_APP_ENV}
	@make _replace_env key=GA_ID              val=${${ENV}_GA_ID}

_push-backend-docker:
	@mkdir -p ./backend/config/jwt
	@echo ${JWT_PRIVATE_KEY} | base64 -d > ./backend/config/jwt/private.pem
	@echo ${JWT_PUBLIC_KEY}  | base64 -d > ./backend/config/jwt/public.pem
	@make __push-docker IMAGE=php-fpm SERVICE=backend

_push-frontend-docker:
	@make __push-docker IMAGE=node-js SERVICE=frontend

_push-webserver-docker:
	@echo ${SSL_KEY}         | base64 -d > .docker/gitlab-ci/wildcard.key
	@echo ${SSL_CERT}        | base64 -d > .docker/gitlab-ci/wildcard.cert
	@make __push-docker IMAGE=nginx SERVICE=webserver

__push-docker:
	@make _docker-login
	docker build -f .docker/gitlab-ci/${IMAGE}/${DOCKER_FILE}/Dockerfile . -t ${CI_REGISTRY_IMAGE}/${SERVICE}:${VERSION}
	docker push ${CI_REGISTRY_IMAGE}/${SERVICE}:${VERSION}
	docker rmi  ${CI_REGISTRY_IMAGE}/${SERVICE}:${VERSION}

_deploy:
	make __deploy SSH=${${ENV}_SSH_ADDRESS} DOC_ROOT=${${ENV}_DOCUMENT_ROOT}

__deploy:
	#ssh ${SSH} "cd ${DOC_ROOT}/ && mkdir -p .docker/monitoring"

	scp .docker/gitlab-ci/Makefile ${SSH}:${DOC_ROOT}/Makefile
	scp .env ${SSH}:${DOC_ROOT}/.env
	#scp -rp ./.docker/monitoring ${SSH}:${DOC_ROOT}/.docker

	ssh ${SSH} "docker login -u gitlab-ci-token -p '${CI_JOB_TOKEN}' ${CI_REGISTRY}"
	ssh ${SSH} "cd ${DOC_ROOT}/ && if [[ -f docker-compose.yml ]] ; then docker-compose pull -q; fi"

	scp ${COMPOSE} ${SSH}:${DOC_ROOT}/docker-compose.yml
	ssh ${SSH} "cd ${DOC_ROOT} && docker-compose up -d"

### LOCAL STUFF

run-dev:
	#docker-sync start
	docker-compose up -d
	bin/docker-exec chmod -R 777 /cache

run-stop-dev:
	#docker-sync stop
	docker-compose stop

run-reset-dev:
	#docker-sync stop
	#docker-sync clean
	docker-compose down -v
	@make run-dev
	@make run-install-symfony

run-install-symfony:
	bin/docker-exec composer run-script post-update-cmd
	bin/docker-exec bin/console assets:install public
	make run-fixtures

run-create-migration:
	bin/docker-exec bin/console doctrine:migrations:diff && bin/docker-exec bin/console doctrine:migrations:migrate

run-fixtures:
	bin/docker-exec bin/console doctrine:schema:drop -f
	bin/docker-exec bin/console doctrine:schema:update -f
	bin/docker-exec bin/console doctrine:fixtures:load -n --fixtures src/DataFixtures/ORM

run-generate-keys-jwt:
	mkdir -p backend/config/jwt
	openssl genrsa -out backend/config/jwt/private.pem -aes256 4096
	openssl rsa -pubout -in backend/config/jwt/private.pem -out backend/config/jwt/public.pem

### TESTING ###

run-local-tests:
	@bin/tests.sh -s=psalm | tee -a local-tests.log
