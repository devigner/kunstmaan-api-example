# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.0.36-MariaDB-1~xenial)
# Database: lean_and_green
# Generation Time: 2019-07-03 15:03:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acl_classes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_classes`;

CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `acl_classes` WRITE;
/*!40000 ALTER TABLE `acl_classes` DISABLE KEYS */;

INSERT INTO `acl_classes` (`id`, `class_type`)
VALUES
	(1,'Kunstmaan\\NodeBundle\\Entity\\Node');

/*!40000 ALTER TABLE `acl_classes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_entries`;

CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`),
  CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `acl_entries` WRITE;
/*!40000 ALTER TABLE `acl_entries` DISABLE KEYS */;

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`)
VALUES
	(1,1,1,1,NULL,0,1073741823,1,'all',0,0),
	(2,1,1,2,NULL,1,61,1,'all',0,0),
	(3,1,1,3,NULL,2,1,1,'all',0,0),
	(4,1,2,1,NULL,0,1073741823,1,'all',0,0),
	(5,1,2,2,NULL,1,61,1,'all',0,0),
	(6,1,2,3,NULL,2,1,1,'all',0,0),
	(7,1,3,1,NULL,0,1073741823,1,'all',0,0),
	(8,1,3,2,NULL,1,61,1,'all',0,0),
	(9,1,3,3,NULL,2,1,1,'all',0,0),
	(10,1,4,1,NULL,0,1073741823,1,'all',0,0),
	(11,1,4,2,NULL,1,61,1,'all',0,0),
	(12,1,4,3,NULL,2,1,1,'all',0,0),
	(13,1,5,1,NULL,0,1073741823,1,'all',0,0),
	(14,1,5,2,NULL,1,61,1,'all',0,0),
	(15,1,5,3,NULL,2,1,1,'all',0,0),
	(16,1,6,3,NULL,0,1,1,'all',0,0),
	(17,1,6,2,NULL,1,61,1,'all',0,0),
	(18,1,6,1,NULL,2,1073741823,1,'all',0,0);

/*!40000 ALTER TABLE `acl_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_object_identities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_object_identities`;

CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`),
  CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `acl_object_identities` WRITE;
/*!40000 ALTER TABLE `acl_object_identities` DISABLE KEYS */;

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`)
VALUES
	(1,NULL,1,'1',1),
	(2,NULL,1,'2',1),
	(3,NULL,1,'3',1),
	(4,NULL,1,'4',1),
	(5,NULL,1,'5',1),
	(6,NULL,1,'6',1);

/*!40000 ALTER TABLE `acl_object_identities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_object_identity_ancestors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`),
  CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `acl_object_identity_ancestors` WRITE;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` DISABLE KEYS */;

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`)
VALUES
	(1,1),
	(2,2),
	(3,3),
	(4,4),
	(5,5),
	(6,6);

/*!40000 ALTER TABLE `acl_object_identity_ancestors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_security_identities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_security_identities`;

CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `acl_security_identities` WRITE;
/*!40000 ALTER TABLE `acl_security_identities` DISABLE KEYS */;

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`)
VALUES
	(3,'IS_AUTHENTICATED_ANONYMOUSLY',0),
	(2,'ROLE_ADMIN',0),
	(1,'ROLE_SUPER_ADMIN',0);

/*!40000 ALTER TABLE `acl_security_identities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_audio_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_audio_page_parts`;

CREATE TABLE `app_audio_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9BABE884EA9FDD75` (`media_id`),
  CONSTRAINT `FK_9BABE884EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_benefit_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_benefit_page_parts`;

CREATE TABLE `app_benefit_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `show_all_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_results` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_benefit_page_parts` WRITE;
/*!40000 ALTER TABLE `app_benefit_page_parts` DISABLE KEYS */;

INSERT INTO `app_benefit_page_parts` (`id`, `show_all_button`, `max_results`)
VALUES
	(1,'Show all benefits',10),
	(2,'Toon alle voordelen',10),
	(3,'Mooie benefit',10),
	(4,'Show all sander',10),
	(5,'Show all benefits',10),
	(6,'Show all benefits',10),
	(7,NULL,10),
	(8,NULL,10),
	(9,NULL,10),
	(10,NULL,10);

/*!40000 ALTER TABLE `app_benefit_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_benefits
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_benefits`;

CREATE TABLE `app_benefits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_id` bigint(20) DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `image_alt_text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_9EDA29533DA5256D` (`image_id`),
  CONSTRAINT `FK_9EDA29533DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_benefits` WRITE;
/*!40000 ALTER TABLE `app_benefits` DISABLE KEYS */;

INSERT INTO `app_benefits` (`id`, `image_id`, `subtitle`, `title`, `content`, `image_alt_text`)
VALUES
	(1,10,'Waarom Lean & Green?','Toegang tot het Lean & Green netwerk','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.',NULL),
	(2,10,'Waarom Lean & Green?','Landelijke erkenning als duurzaam partner','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.',NULL),
	(3,10,'Waarom Lean & Green? EN','Inzicht in je C02 footprint en reducerende maatregelen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.',NULL),
	(4,10,'Waarom Lean & Green','Vrijstelling van de Europese energie audit','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.',NULL);

/*!40000 ALTER TABLE `app_benefits` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_button_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_button_page_parts`;

CREATE TABLE `app_button_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `link_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_new_window` tinyint(1) DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_button_page_parts` WRITE;
/*!40000 ALTER TABLE `app_button_page_parts` DISABLE KEYS */;

INSERT INTO `app_button_page_parts` (`id`, `link_text`, `link_url`, `link_new_window`, `type`, `size`, `position`)
VALUES
	(3,'Open iets','http://www.loremipsum.nl/',NULL,'primary','default','left'),
	(4,'Open Something','http://www.loremipsum.nl/',NULL,'primary','default','left'),
	(5,'Open iets','http://www.loremipsum.nl/',0,'primary','default','left');

/*!40000 ALTER TABLE `app_button_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_checkbox_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_checkbox_page_parts`;

CREATE TABLE `app_checkbox_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_choice_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_choice_page_parts`;

CREATE TABLE `app_choice_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expanded` tinyint(1) DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT NULL,
  `choices` longtext COLLATE utf8_unicode_ci,
  `empty_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_content_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_content_pages`;

CREATE TABLE `app_content_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_content_pages` WRITE;
/*!40000 ALTER TABLE `app_content_pages` DISABLE KEYS */;

INSERT INTO `app_content_pages` (`id`, `title`, `page_title`)
VALUES
	(1,'Mee doen','Mee doen'),
	(2,'Participate','Participate'),
	(3,'Maatregelen','Maatregelen'),
	(4,'Measures','Measures'),
	(5,'Deelnemers','Deelnemers'),
	(6,'Attendees','Attendees'),
	(7,'Deelnemen','Deelnemen'),
	(8,'Participate','Participate'),
	(9,'Deelnemen','Deelnemen'),
	(10,'Mee doen','Mee doen');

/*!40000 ALTER TABLE `app_content_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_download_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_download_page_parts`;

CREATE TABLE `app_download_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68BD8990EA9FDD75` (`media_id`),
  CONSTRAINT `FK_68BD8990EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_dynamics_sync_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_dynamics_sync_log`;

CREATE TABLE `app_dynamics_sync_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_dynamics_sync_log` WRITE;
/*!40000 ALTER TABLE `app_dynamics_sync_log` DISABLE KEYS */;

INSERT INTO `app_dynamics_sync_log` (`id`, `user_id`, `action`, `content`, `created_at`, `updated_at`)
VALUES
	(1,3,'CREATE_REMOTE_WITH_LOCAL','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"account_id > accountid: \",\n    \"gender > gendercode: man\"\n]','2019-06-14 11:46:02','2019-06-14 11:46:02'),
	(2,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 11:48:07','2019-06-14 11:48:07'),
	(3,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 11:52:28','2019-06-14 11:52:28'),
	(4,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 11:56:30','2019-06-14 11:56:30'),
	(5,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 11:58:54','2019-06-14 11:58:54'),
	(6,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 11:59:17','2019-06-14 11:59:17'),
	(7,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:00:01','2019-06-14 12:00:01'),
	(8,3,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn17\",\n    \"lastName > lastname: van Beek17\",\n    \"email > emailaddress1: martijn+api17@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:00:17','2019-06-14 12:00:17'),
	(9,NULL,'CREATE_REMOTE_WITH_LOCAL','[\n    \"firstName > firstname: Martijn\",\n    \"lastName > lastname: van Beek\",\n    \"email > emailaddress1: martijn+api@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\"\n]','2019-06-14 12:06:19','2019-06-14 12:06:19'),
	(10,NULL,'CREATE_REMOTE_WITH_LOCAL','[\n    \"firstName > firstname: Martijn\",\n    \"lastName > lastname: van Beek\",\n    \"email > emailaddress1: martijn+api-x@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\"\n]','2019-06-14 12:07:30','2019-06-14 12:07:30'),
	(11,5,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn\",\n    \"lastName > lastname: van Beek\",\n    \"email > emailaddress1: martijn+api-x@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:11:23','2019-06-14 12:11:23'),
	(12,5,'UPDATE_REMOTE','[\n    \"firstName > firstname: Martijn\",\n    \"lastName > lastname: van Beek\",\n    \"email > emailaddress1: martijn+api-x@raakrdam.nl\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:11:48','2019-06-14 12:11:48'),
	(13,NULL,'CREATE_REMOTE_WITH_LOCAL','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\"\n]','2019-06-14 12:17:44','2019-06-14 12:17:44'),
	(14,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:18:06','2019-06-14 12:18:06'),
	(15,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:18:33','2019-06-14 12:18:33'),
	(16,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 12:19:12','2019-06-14 12:19:12'),
	(17,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 13:03:37','2019-06-14 13:03:37'),
	(18,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 13:09:41','2019-06-14 13:09:41'),
	(19,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"telephone < telephone1: \",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\"\n]','2019-06-14 13:09:41','2019-06-14 13:09:41'),
	(20,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"telephone < telephone1: \",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\"\n]','2019-06-14 13:59:56','2019-06-14 13:59:56'),
	(21,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 13:59:57','2019-06-14 13:59:57'),
	(22,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"telephone < telephone1: \",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\"\n]','2019-06-14 14:00:57','2019-06-14 14:00:57'),
	(23,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 14:00:58','2019-06-14 14:00:58'),
	(24,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"telephone < telephone1: \",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\"\n]','2019-06-14 14:11:59','2019-06-14 14:11:59'),
	(25,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 14:12:00','2019-06-14 14:12:00'),
	(26,1,'CREATE_REMOTE_WITH_LOCAL','[\n    \"name > name: RaakRdam Dev\"\n]','2019-06-14 14:28:50','2019-06-14 14:28:50'),
	(27,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"telephone > telephone1: \",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-14 14:38:24','2019-06-14 14:38:24'),
	(28,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"telephone < telephone1: \",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\"\n]','2019-06-14 14:38:25','2019-06-14 14:38:25'),
	(29,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"telephone > telephone1: \",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"account_id > accountid: 0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 12:03:20','2019-06-17 12:03:20'),
	(30,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"telephone > telephone1: \",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"account_id > accountid: 0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 12:20:53','2019-06-17 12:20:53'),
	(31,1,'UPDATE_LOCAL','[\n    \"name < name: RaakRdam Dev\"\n]','2019-06-17 12:27:21','2019-06-17 12:27:21'),
	(32,1,'UPDATE_LOCAL','[\n    \"name < name: RaakRdam Dev\"\n]','2019-06-17 12:48:10','2019-06-17 12:48:10'),
	(33,6,'UPDATE_REMOTE','[\n    \"firstName > firstname: Test\",\n    \"lastName > lastname: User\",\n    \"telephone > telephone1: \",\n    \"email > emailaddress1: lean-green@test.dev\",\n    \"newsletter > connekt_nieuwsbrief: \",\n    \"gender > gendercode: Man\",\n    \"account_id > accountid: 0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\",\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 13:46:29','2019-06-17 13:46:29'),
	(34,6,'UPDATE_LOCAL','[\n    \"firstName < firstname: Test\",\n    \"lastName < lastname: User\",\n    \"telephone < telephone1: \",\n    \"email < emailaddress1: lean-green@test.dev\",\n    \"newsletter < connekt_nieuwsbrief: \",\n    \"gender < gendercode: Man\",\n    \"account_id < parentcustomerid: \"\n]','2019-06-17 14:24:43','2019-06-17 14:24:43'),
	(35,6,'UPDATE_LOCAL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-17 14:29:14','2019-06-17 14:29:14'),
	(36,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 14:35:53','2019-06-17 14:35:53'),
	(37,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 14:47:53','2019-06-17 14:47:53'),
	(38,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 14:50:19','2019-06-17 14:50:19'),
	(39,6,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": {}\n    }\n]','2019-06-17 14:50:59','2019-06-17 14:50:59'),
	(40,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-17 14:51:10','2019-06-17 14:51:10'),
	(41,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 07:32:57','2019-06-18 07:32:57'),
	(42,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 07:34:54','2019-06-18 07:34:54'),
	(43,NULL,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"name\",\n        \"remote\": \"name\",\n        \"value\": \"RaakRdam Dev\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 07:36:19','2019-06-18 07:36:19'),
	(44,6,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": {}\n    }\n]','2019-06-18 07:37:48','2019-06-18 07:37:48'),
	(45,3,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+api17@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 08:23:32','2019-06-18 08:23:32'),
	(46,3,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+api17@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 08:26:36','2019-06-18 08:26:36'),
	(47,3,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek17\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+api17x@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 08:31:37','2019-06-18 08:31:37'),
	(48,13,'CREATE_REMOTE_WITH_LOCAL','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 11:06:36','2019-06-18 11:06:36'),
	(49,6,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": {\n            \"__initializer__\": null,\n            \"__cloner__\": null,\n            \"__isInitialized__\": true\n        }\n    }\n]','2019-06-18 11:10:42','2019-06-18 11:10:42'),
	(50,6,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": {\n            \"__initializer__\": null,\n            \"__cloner__\": null,\n            \"__isInitialized__\": true\n        }\n    }\n]','2019-06-18 11:11:29','2019-06-18 11:11:29'),
	(51,6,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"Man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": \"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 11:11:31','2019-06-18 11:11:31'),
	(52,6,'PULL','[\n    {\n        \"direction\": \"pull\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": {\n            \"value\": 1,\n            \"label\": \"Man\"\n        }\n    },\n    {\n        \"direction\": \"pull\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": {\n            \"__initializer__\": null,\n            \"__cloner__\": null,\n            \"__isInitialized__\": true\n        }\n    }\n]','2019-06-18 11:12:00','2019-06-18 11:12:00'),
	(53,6,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"User\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"lean-green@test.dev\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"Man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": \"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 11:12:03','2019-06-18 11:12:03'),
	(54,14,'CREATE_REMOTE_WITH_LOCAL','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg2@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 11:14:18','2019-06-18 11:14:18'),
	(55,14,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg2@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 11:14:47','2019-06-18 11:14:47'),
	(56,15,'CREATE_REMOTE_WITH_LOCAL','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg3@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 11:52:46','2019-06-18 11:52:46'),
	(57,15,'PUSH','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg3@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    },\n    \"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"\n]','2019-06-18 11:58:28','2019-06-18 11:58:28'),
	(58,16,'CREATE_REMOTE_WITH_LOCAL','[\n    {\n        \"direction\": \"push\",\n        \"local\": \"firstName\",\n        \"remote\": \"firstname\",\n        \"value\": \"Martijn\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"lastName\",\n        \"remote\": \"lastname\",\n        \"value\": \"van Beek Test\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"telephone\",\n        \"remote\": \"telephone1\",\n        \"value\": null\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"email\",\n        \"remote\": \"emailaddress1\",\n        \"value\": \"martijn+lg4@raakrdam.nl\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"newsletter\",\n        \"remote\": \"connekt_nieuwsbrief\",\n        \"value\": false\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"gender\",\n        \"remote\": \"gendercode\",\n        \"value\": \"man\"\n    },\n    {\n        \"direction\": \"push\",\n        \"local\": \"account_id\",\n        \"remote\": \"parentcustomerid\",\n        \"value\": null\n    }\n]','2019-06-18 12:08:26','2019-06-18 12:08:26'),
	(59,17,'CREATE_REMOTE_WITH_LOCAL','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg5@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":null}]','2019-06-18 12:16:00','2019-06-18 12:16:00'),
	(60,18,'CREATE_REMOTE_WITH_LOCAL','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":null}]','2019-06-18 12:53:09','2019-06-18 12:53:09'),
	(61,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{}}]','2019-06-18 11:00:19','2019-06-18 11:00:19'),
	(62,8,'PULL','[{\"direction\":\"pull\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"pull\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true}]','2019-06-18 11:04:32','2019-06-18 11:04:32'),
	(63,8,'PULL','[{\"direction\":\"pull\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"pull\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"pull\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"pull\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"pull\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":{}},{\"direction\":\"pull\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true}]','2019-06-18 11:08:48','2019-06-18 11:08:48'),
	(64,8,'PULL','[{\"direction\":\"pull\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"pull\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"pull\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"pull\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"pull\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":{}},{\"direction\":\"pull\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":{\"value\":718760005,\"label\":\"Mobiliteitsdienst\"}},{\"direction\":\"pull\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true}]','2019-06-18 11:11:21','2019-06-18 11:11:21'),
	(65,8,'PULL','[{\"direction\":\"pull\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"pull\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"pull\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"pull\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"pull\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":{}},{\"direction\":\"pull\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":{\"value\":718760005,\"label\":\"Mobiliteitsdienst\"}},{\"direction\":\"pull\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true}]','2019-06-18 14:31:49','2019-06-18 14:31:49'),
	(66,8,'PUSH','[{\"direction\":\"push\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"push\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"push\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"push\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"push\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":\"Nederland\"},{\"direction\":\"push\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":\"Mobiliteitsdienst\"},{\"direction\":\"push\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-18 14:31:50','2019-06-18 14:31:50'),
	(67,8,'PUSH','[{\"direction\":\"push\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"push\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"push\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"push\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"push\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":\"Nederland\"},{\"direction\":\"push\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":\"Mobiliteitsdienst\"},{\"direction\":\"push\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:28:27','2019-06-19 10:28:27'),
	(68,8,'PULL','[{\"direction\":\"pull\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"pull\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"pull\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"pull\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"pull\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":{}},{\"direction\":\"pull\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":{\"value\":718760005,\"label\":\"Mobiliteitsdienst\"}},{\"direction\":\"pull\",\"local\":\"chamberOfCommerceNumber\",\"remote\":\"wsnl_kvkno\",\"value\":\"0987654321\"},{\"direction\":\"pull\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true}]','2019-06-19 10:33:50','2019-06-19 10:33:50'),
	(69,8,'PUSH','[{\"direction\":\"push\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"push\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"push\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"push\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"push\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":\"Nederland\"},{\"direction\":\"push\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":\"Mobiliteitsdienst\"},{\"direction\":\"push\",\"local\":\"chamberOfCommerceNumber\",\"remote\":\"wsnl_kvkno\",\"value\":\"0987654321\"},{\"direction\":\"push\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:33:51','2019-06-19 10:33:51'),
	(70,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:38:29','2019-06-19 10:38:29'),
	(71,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:38:29','2019-06-19 10:38:29'),
	(72,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:38:45','2019-06-19 10:38:45'),
	(73,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:38:46','2019-06-19 10:38:46'),
	(74,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:45:01','2019-06-19 10:45:01'),
	(75,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:45:03','2019-06-19 10:45:03'),
	(76,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:46:07','2019-06-19 10:46:07'),
	(77,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:46:08','2019-06-19 10:46:08'),
	(78,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:49:22','2019-06-19 10:49:22'),
	(79,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:49:23','2019-06-19 10:49:23'),
	(80,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:52:14','2019-06-19 10:52:14'),
	(81,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:52:14','2019-06-19 10:52:14'),
	(82,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:55:56','2019-06-19 10:55:56'),
	(83,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:55:57','2019-06-19 10:55:57'),
	(84,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 10:59:36','2019-06-19 10:59:36'),
	(85,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 10:59:37','2019-06-19 10:59:37'),
	(86,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 11:07:50','2019-06-19 11:07:50'),
	(87,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 11:07:50','2019-06-19 11:07:50'),
	(88,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 11:10:27','2019-06-19 11:10:27'),
	(89,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 11:10:27','2019-06-19 11:10:27'),
	(90,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 11:12:16','2019-06-19 11:12:16'),
	(91,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 11:12:16','2019-06-19 11:12:16'),
	(92,8,'PUSH','[{\"direction\":\"push\",\"local\":\"name\",\"remote\":\"name\",\"value\":\"RaakRdam Dev\"},{\"direction\":\"push\",\"local\":\"street\",\"remote\":\"address1_line1\",\"value\":\"Schiekade 189\"},{\"direction\":\"push\",\"local\":\"postalCode\",\"remote\":\"address1_postalcode\",\"value\":\"3013 BR\"},{\"direction\":\"push\",\"local\":\"city\",\"remote\":\"address1_city\",\"value\":\"ROTTERDAM\"},{\"direction\":\"push\",\"local\":\"country\",\"remote\":\"connekt_adres1land\",\"value\":\"Nederland\"},{\"direction\":\"push\",\"local\":\"organisationType\",\"remote\":\"industrycode\",\"value\":\"Mobiliteitsdienst\"},{\"direction\":\"push\",\"local\":\"chamberOfCommerceNumber\",\"remote\":\"wsnl_kvkno\",\"value\":\"0987654321\"},{\"direction\":\"push\",\"local\":\"leanAndGreenEnabled\",\"remote\":\"connekt_geangreenlidmaatschap\",\"value\":true},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 11:45:44','2019-06-19 11:45:44'),
	(93,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 13:42:15','2019-06-19 13:42:15'),
	(94,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 13:42:17','2019-06-19 13:42:17'),
	(95,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 13:42:30','2019-06-19 13:42:30'),
	(96,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 13:42:30','2019-06-19 13:42:30'),
	(97,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 14:12:53','2019-06-19 14:12:53'),
	(98,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 14:12:54','2019-06-19 14:12:54'),
	(99,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 14:13:44','2019-06-19 14:13:44'),
	(100,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 14:13:45','2019-06-19 14:13:45'),
	(101,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 14:15:08','2019-06-19 14:15:08'),
	(102,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 14:15:08','2019-06-19 14:15:08'),
	(103,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 14:18:05','2019-06-19 14:18:05'),
	(104,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 14:18:10','2019-06-19 14:18:10'),
	(105,18,'PUSH','[{\"direction\":\"push\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"push\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"push\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"push\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"push\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"push\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":\"Man\"},{\"direction\":\"push\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":\"0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7\"},\"<UpdateResponse xmlns=\\\"http:\\/\\/schemas.microsoft.com\\/xrm\\/2011\\/Contracts\\/Services\\\" xmlns:a=\\\"http:\\/\\/www.w3.org\\/2005\\/08\\/addressing\\\" xmlns:s=\\\"http:\\/\\/www.w3.org\\/2003\\/05\\/soap-envelope\\\" xmlns:u=\\\"http:\\/\\/docs.oasis-open.org\\/wss\\/2004\\/01\\/oasis-200401-wss-wssecurity-utility-1.0.xsd\\\"><\\/UpdateResponse>\"]','2019-06-19 14:19:29','2019-06-19 14:19:29'),
	(106,18,'PULL','[{\"direction\":\"pull\",\"local\":\"firstName\",\"remote\":\"firstname\",\"value\":\"Martijn\"},{\"direction\":\"pull\",\"local\":\"lastName\",\"remote\":\"lastname\",\"value\":\"van Beek Test\"},{\"direction\":\"pull\",\"local\":\"telephone\",\"remote\":\"telephone1\",\"value\":null},{\"direction\":\"pull\",\"local\":\"email\",\"remote\":\"emailaddress1\",\"value\":\"martijn+lg6@raakrdam.nl\"},{\"direction\":\"pull\",\"local\":\"newsletter\",\"remote\":\"connekt_nieuwsbrief\",\"value\":false},{\"direction\":\"pull\",\"local\":\"gender\",\"remote\":\"gendercode\",\"value\":{\"value\":1,\"label\":\"Man\"}},{\"direction\":\"pull\",\"local\":\"account_id\",\"remote\":\"parentcustomerid\",\"value\":{\"__initializer__\":null,\"__cloner__\":null,\"__isInitialized__\":true}}]','2019-06-19 14:19:31','2019-06-19 14:19:31');

/*!40000 ALTER TABLE `app_dynamics_sync_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_email_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_email_page_parts`;

CREATE TABLE `app_email_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `error_message_invalid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_file_upload_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_file_upload_page_parts`;

CREATE TABLE `app_file_upload_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_form_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_form_pages`;

CREATE TABLE `app_form_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `thanks` longtext COLLATE utf8_unicode_ci,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_header_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_header_page_parts`;

CREATE TABLE `app_header_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `niv` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_header_page_parts` WRITE;
/*!40000 ALTER TABLE `app_header_page_parts` DISABLE KEYS */;

INSERT INTO `app_header_page_parts` (`id`, `niv`, `title`)
VALUES
	(3,1,'Welkom bij onze website'),
	(4,1,'Welcome at our website'),
	(5,1,'Welkom bij onze website');

/*!40000 ALTER TABLE `app_header_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_home_c_t_a_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_home_c_t_a_page_parts`;

CREATE TABLE `app_home_c_t_a_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `register_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `register_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_home_c_t_a_page_parts` WRITE;
/*!40000 ALTER TABLE `app_home_c_t_a_page_parts` DISABLE KEYS */;

INSERT INTO `app_home_c_t_a_page_parts` (`id`, `register_title`, `register_button`, `contact_title`, `contact_button`)
VALUES
	(1,'Werk samen in een  betrouwbaar netwerk','Registreer vandaag','Aanvullende informatie  over deelname?','Maak een afspraak'),
	(2,'Work together in a reliable network','Register today','Additional information about participation?','Make an appointment'),
	(3,'Work together in a reliable network','Register today','Additional information about participation?','Make an appointment'),
	(4,'Work together in a reliable network','Register today','Additional information about participation?','Make an appointment');

/*!40000 ALTER TABLE `app_home_c_t_a_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_home_header_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_home_header_page_parts`;

CREATE TABLE `app_home_header_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `img_src_id` bigint(20) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anchor_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anchor_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9B66A2FDD2D099CC` (`img_src_id`),
  CONSTRAINT `FK_9B66A2FDD2D099CC` FOREIGN KEY (`img_src_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_home_header_page_parts` WRITE;
/*!40000 ALTER TABLE `app_home_header_page_parts` DISABLE KEYS */;

INSERT INTO `app_home_header_page_parts` (`id`, `img_src_id`, `title`, `subtitle`, `anchor_text`, `anchor_target_id`)
VALUES
	(1,1,'<p>The road to <strong>zero</strong> emission starts here</p>','Het programma','Bekijk de voordelen','benefits'),
	(2,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(3,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(4,1,'<p>The road to <strong>zero</strong> emission starts here</p>','Het programma','Bekijk de voordelen','benefits'),
	(5,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(6,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(7,1,'<p>The road to <strong>zero</strong> emission starts here</p>','Het programma','Bekijk de voordelen','benefits'),
	(8,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(9,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(10,1,'<p>The road to <strong>zero</strong> emission starts here</p>','Het programma','Bekijk de voordelen','benefits'),
	(11,1,'<p>The road to <strong>zero</strong> emission starts here</p>','Het programma','Bekijk de voordelen','benefits'),
	(12,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(13,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits'),
	(14,1,'<p>The road to <strong>zero</strong> emission starts here</p>','The program','Check the benefits','benefits');

/*!40000 ALTER TABLE `app_home_header_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_home_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_home_pages`;

CREATE TABLE `app_home_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_home_pages` WRITE;
/*!40000 ALTER TABLE `app_home_pages` DISABLE KEYS */;

INSERT INTO `app_home_pages` (`id`, `title`, `page_title`)
VALUES
	(1,'Home','Home'),
	(2,'Home','Home'),
	(3,'Home','Home'),
	(4,'Home','Home'),
	(5,'Home','Home'),
	(6,'Home','Home'),
	(7,'Home','Home'),
	(8,'Home','Home'),
	(9,'Home','Home'),
	(10,'Home','Home'),
	(11,'Home','Home'),
	(12,'Home','Home'),
	(13,'Home','Home'),
	(14,'Home','Home'),
	(15,'Home','Home'),
	(16,'Home','Home'),
	(17,'Home','Home');

/*!40000 ALTER TABLE `app_home_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_home_statistics_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_home_statistics_page_parts`;

CREATE TABLE `app_home_statistics_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `statistic1title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic1value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic1text` longtext COLLATE utf8_unicode_ci,
  `statistic2title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic2value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic2text` longtext COLLATE utf8_unicode_ci,
  `statistic3title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic3value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic3text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_home_statistics_page_parts` WRITE;
/*!40000 ALTER TABLE `app_home_statistics_page_parts` DISABLE KEYS */;

INSERT INTO `app_home_statistics_page_parts` (`id`, `statistic1title`, `statistic1value`, `statistic1text`, `statistic2title`, `statistic2value`, `statistic2text`, `statistic3title`, `statistic3value`, `statistic3text`)
VALUES
	(1,'Megaton CO₂ Reductie','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Gereduceerde wegkilometers','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(2,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(3,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(4,'Megaton CO₂ Reductie','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Gereduceerde wegkilometers','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(5,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(6,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(7,'Megaton CO₂ Reductie','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Gereduceerde wegkilometers','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(8,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(9,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(10,'Megaton CO₂ Reductie','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Gereduceerde wegkilometers','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(11,'Megaton CO₂ Reductie','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Gereduceerde wegkilometers','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(12,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(13,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus'),
	(14,'Megaton CO₂ Reduction','600+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','Mln. Less roadmiles','40+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus','WeConnekt App Users','300+','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus');

/*!40000 ALTER TABLE `app_home_statistics_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_image_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_image_page_parts`;

CREATE TABLE `app_image_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `open_in_new_window` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F721AEBFEA9FDD75` (`media_id`),
  CONSTRAINT `FK_F721AEBFEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_industry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_industry`;

CREATE TABLE `app_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_intro_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_intro_text_page_parts`;

CREATE TABLE `app_intro_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_line_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_line_page_parts`;

CREATE TABLE `app_line_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_line_page_parts` WRITE;
/*!40000 ALTER TABLE `app_line_page_parts` DISABLE KEYS */;

INSERT INTO `app_line_page_parts` (`id`)
VALUES
	(3),
	(4),
	(5);

/*!40000 ALTER TABLE `app_line_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_link_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_link_page_parts`;

CREATE TABLE `app_link_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `open_in_new_window` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_multi_line_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_multi_line_text_page_parts`;

CREATE TABLE `app_multi_line_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_authors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_authors`;

CREATE TABLE `app_news_authors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_comment`;

CREATE TABLE `app_news_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_id` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_11E540BB5A459A0` (`news_id`),
  KEY `IDX_11E540BA76ED395` (`user_id`),
  CONSTRAINT `FK_11E540BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_11E540BB5A459A0` FOREIGN KEY (`news_id`) REFERENCES `app_news_pages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_overview_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_overview_pages`;

CREATE TABLE `app_news_overview_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `max_results` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_news_overview_pages` WRITE;
/*!40000 ALTER TABLE `app_news_overview_pages` DISABLE KEYS */;

INSERT INTO `app_news_overview_pages` (`id`, `max_results`, `title`, `page_title`)
VALUES
	(1,10,'Nieuws & Events','Nieuws & Events'),
	(2,10,'News & Events','News & Events');

/*!40000 ALTER TABLE `app_news_overview_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_news_page_part_news_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_page_part_news_page`;

CREATE TABLE `app_news_page_part_news_page` (
  `news_page_part_id` bigint(20) NOT NULL,
  `news_page_id` bigint(20) NOT NULL,
  PRIMARY KEY (`news_page_part_id`,`news_page_id`),
  KEY `IDX_42D891DB50C9C9CE` (`news_page_part_id`),
  KEY `IDX_42D891DB5BA27F02` (`news_page_id`),
  CONSTRAINT `FK_42D891DB50C9C9CE` FOREIGN KEY (`news_page_part_id`) REFERENCES `app_news_page_parts` (`id`),
  CONSTRAINT `FK_42D891DB5BA27F02` FOREIGN KEY (`news_page_id`) REFERENCES `app_news_pages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_page_parts`;

CREATE TABLE `app_news_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_page_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_page_tags`;

CREATE TABLE `app_news_page_tags` (
  `news_page_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`news_page_id`,`tag_id`),
  KEY `IDX_2BA745C35BA27F02` (`news_page_id`),
  KEY `IDX_2BA745C3BAD26311` (`tag_id`),
  CONSTRAINT `FK_2BA745C35BA27F02` FOREIGN KEY (`news_page_id`) REFERENCES `app_news_pages` (`id`),
  CONSTRAINT `FK_2BA745C3BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `app_tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_news_page_theme
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_page_theme`;

CREATE TABLE `app_news_page_theme` (
  `news_page_id` bigint(20) NOT NULL,
  `theme_id` bigint(20) NOT NULL,
  PRIMARY KEY (`news_page_id`,`theme_id`),
  KEY `IDX_4751EA2E5BA27F02` (`news_page_id`),
  KEY `IDX_4751EA2E59027487` (`theme_id`),
  CONSTRAINT `FK_4751EA2E59027487` FOREIGN KEY (`theme_id`) REFERENCES `app_theme` (`id`),
  CONSTRAINT `FK_4751EA2E5BA27F02` FOREIGN KEY (`news_page_id`) REFERENCES `app_news_pages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_news_page_theme` WRITE;
/*!40000 ALTER TABLE `app_news_page_theme` DISABLE KEYS */;

INSERT INTO `app_news_page_theme` (`news_page_id`, `theme_id`)
VALUES
	(1,10);

/*!40000 ALTER TABLE `app_news_page_theme` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_news_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_news_pages`;

CREATE TABLE `app_news_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_author_id` bigint(20) DEFAULT NULL,
  `date` datetime NOT NULL,
  `summary` longtext COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CD0C957EA50ACDA` (`news_author_id`),
  CONSTRAINT `FK_CD0C957EA50ACDA` FOREIGN KEY (`news_author_id`) REFERENCES `app_news_authors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_news_pages` WRITE;
/*!40000 ALTER TABLE `app_news_pages` DISABLE KEYS */;

INSERT INTO `app_news_pages` (`id`, `news_author_id`, `date`, `summary`, `title`, `page_title`)
VALUES
	(1,NULL,'2019-07-02 14:48:00','Lorem ipsum dolor sit amet','First news element',NULL);

/*!40000 ALTER TABLE `app_news_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_participant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_participant`;

CREATE TABLE `app_participant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `max_results` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_program_header_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_program_header_page_parts`;

CREATE TABLE `app_program_header_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_id` bigint(20) DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_8CCDB8053DA5256D` (`image_id`),
  CONSTRAINT `FK_8CCDB8053DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_program_header_page_parts` WRITE;
/*!40000 ALTER TABLE `app_program_header_page_parts` DISABLE KEYS */;

INSERT INTO `app_program_header_page_parts` (`id`, `image_id`, `subtitle`, `title`, `content`)
VALUES
	(1,11,'Het programma','0% EMISSION, 100% COLLABORATION','Verduurzamen en tegelijkertijd kosten besparen? Ja, het kan! Lean & Green Logistics is een CO₂-reductieprogramma voor bedrijven die hun logistiek willen verduurzamen en tegelijkertijd hun winst willen verhogen. De eerste stap is: 20% CO2-reductie, waarvoor bedrijven de 1e Lean & Green Star verdienen. De Award wordt je overhandigd als je een Plan van Aanpak hebt ingediend om deze 20% reductie te behalen.'),
	(2,11,'Het programma','0% EMISSION, 100% COLLABORATION','Verduurzamen en tegelijkertijd kosten besparen? Ja, het kan! Lean & Green Logistics is een CO₂-reductieprogramma voor bedrijven die hun logistiek willen verduurzamen en tegelijkertijd hun winst willen verhogen. De eerste stap is: 20% CO2-reductie, waarvoor bedrijven de 1e Lean & Green Star verdienen. De Award wordt je overhandigd als je een Plan van Aanpak hebt ingediend om deze 20% reductie te behalen.');

/*!40000 ALTER TABLE `app_program_header_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_program_lean_green_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_program_lean_green_page_parts`;

CREATE TABLE `app_program_lean_green_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lean_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lean_content` longtext COLLATE utf8_unicode_ci,
  `green_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `green_content` longtext COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_program_lean_green_page_parts` WRITE;
/*!40000 ALTER TABLE `app_program_lean_green_page_parts` DISABLE KEYS */;

INSERT INTO `app_program_lean_green_page_parts` (`id`, `lean_title`, `lean_content`, `green_title`, `green_content`, `title`)
VALUES
	(1,'LEAN','Wendbaar, innovatief en comfortabel in een veranderende markt. Dat zijn de kenmerken van Lean & Green leden. Zij kijken naar hoe zaken binnen en buiten hun organisatie slimmer en duurzamer kunnen voor een betere bottom-line.','GREEN','Betrokken en bewust. Bij Lean & Green zitten de ondernemers en organisaties op zoek naar de balans tussen de verantwoordelijkheden in het bedrijf en de verantwoordelijkheid voor de wereld die zij achter laten.','Lean & Green is er voor en door  Europese ondernemers en organisaties zoals jij');

/*!40000 ALTER TABLE `app_program_lean_green_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_program_starway_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_program_starway_page_parts`;

CREATE TABLE `app_program_starway_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `star1image_id` bigint(20) DEFAULT NULL,
  `star2image_id` bigint(20) DEFAULT NULL,
  `star3image_id` bigint(20) DEFAULT NULL,
  `star4image_id` bigint(20) DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `register_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star1title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star1content` longtext COLLATE utf8_unicode_ci,
  `star2title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star2content` longtext COLLATE utf8_unicode_ci,
  `star3title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star3content` longtext COLLATE utf8_unicode_ci,
  `star4title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star4content` longtext COLLATE utf8_unicode_ci,
  `star1image_alt_text` longtext COLLATE utf8_unicode_ci,
  `star2image_alt_text` longtext COLLATE utf8_unicode_ci,
  `star3image_alt_text` longtext COLLATE utf8_unicode_ci,
  `star4image_alt_text` longtext COLLATE utf8_unicode_ci,
  `starimage_id` bigint(20) DEFAULT NULL,
  `startitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starimage_alt_text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_75D082FD2D2DDE8B` (`star1image_id`),
  KEY `IDX_75D082FD14A0E24E` (`star2image_id`),
  KEY `IDX_75D082FD3DBF60D` (`star3image_id`),
  KEY `IDX_75D082FD67BA9BC4` (`star4image_id`),
  KEY `IDX_75D082FDDE0DCA03` (`starimage_id`),
  CONSTRAINT `FK_75D082FD14A0E24E` FOREIGN KEY (`star2image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_75D082FD2D2DDE8B` FOREIGN KEY (`star1image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_75D082FD3DBF60D` FOREIGN KEY (`star3image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_75D082FD67BA9BC4` FOREIGN KEY (`star4image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_75D082FDDE0DCA03` FOREIGN KEY (`starimage_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_program_starway_page_parts` WRITE;
/*!40000 ALTER TABLE `app_program_starway_page_parts` DISABLE KEYS */;

INSERT INTO `app_program_starway_page_parts` (`id`, `star1image_id`, `star2image_id`, `star3image_id`, `star4image_id`, `subtitle`, `title`, `content`, `register_button`, `star1title`, `star1content`, `star2title`, `star2content`, `star3title`, `star3content`, `star4title`, `star4content`, `star1image_alt_text`, `star2image_alt_text`, `star3image_alt_text`, `star4image_alt_text`, `starimage_id`, `startitle`, `starimage_alt_text`)
VALUES
	(1,NULL,NULL,NULL,NULL,'The','LEAN & GREEN STARWAY','Sinds de start van het programma in 2008 is Lean & Green uitgegroeid tot een netwerk van ruim 500 Lean & Green Award-,  Star- , 2e en 3e Star Winnaars in 9 landen in Europa. Al deze koplopers in duurzame mobiliteit zijn op verschillende manieren actief op de weg naar zero emissie.','Registreer vandaag','1 Star','De Lean & Green Award wordt toegekend aan organisaties, die zich door middel van een goedgekeurd Plan van Aanpak committeren aan de duurzaamheidsambitie van een minimale CO₂-reductie van 20% binnen een periode van maximaal vijf jaar.','2 Stars','De Lean & Green Award wordt toegekend aan organisaties, die zich door middel van een goedgekeurd Plan van Aanpak committeren aan de duurzaamheidsambitie van een minimale CO₂-reductie van 20% binnen een periode van maximaal vijf jaar.','3 Stars','De Lean & Green Award wordt toegekend aan organisaties, die zich door middel van een goedgekeurd Plan van Aanpak committeren aan de duurzaamheidsambitie van een minimale CO₂-reductie van 20% binnen een periode van maximaal vijf jaar.','4 Stars','De Lean & Green Award wordt toegekend aan organisaties, die zich door middel van een goedgekeurd Plan van Aanpak committeren aan de duurzaamheidsambitie van een minimale CO₂-reductie van 20% binnen een periode van maximaal vijf jaar.','1 Star','2 Stars','3 Stars','4 Stars',NULL,NULL,NULL);

/*!40000 ALTER TABLE `app_program_starway_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_qa_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_qa_page_parts`;

CREATE TABLE `app_qa_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `max_results` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_raw_html_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_raw_html_page_parts`;

CREATE TABLE `app_raw_html_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_search_search_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_search_search_page`;

CREATE TABLE `app_search_search_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_single_line_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_single_line_text_page_parts`;

CREATE TABLE `app_single_line_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_submit_button_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_submit_button_page_parts`;

CREATE TABLE `app_submit_button_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_tag_company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_tag_company`;

CREATE TABLE `app_tag_company` (
  `company_id` int(11) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`company_id`,`tag_id`),
  KEY `IDX_246B0259979B1AD6` (`company_id`),
  KEY `IDX_246B0259BAD26311` (`tag_id`),
  CONSTRAINT `FK_246B0259979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `app_user_company` (`id`),
  CONSTRAINT `FK_246B0259BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `app_tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_tags`;

CREATE TABLE `app_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_text_page_parts`;

CREATE TABLE `app_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_text_page_parts` WRITE;
/*!40000 ALTER TABLE `app_text_page_parts` DISABLE KEYS */;

INSERT INTO `app_text_page_parts` (`id`, `content`)
VALUES
	(3,'Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed\naliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent\nadipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero.\nCras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id,\nimperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque\nfacilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et,\nhendrerit quis, nisi.'),
	(4,'Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent\ncongue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan\ncursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed\naliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus\nconsectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum\nfringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis.\n'),
	(5,'<p>Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi.</p>'),
	(6,'<p>Lorem ipsum dolor sit smaetojfe jrwksfkjds</p>');

/*!40000 ALTER TABLE `app_text_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_theme
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_theme`;

CREATE TABLE `app_theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_id` bigint(20) DEFAULT NULL,
  `image_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_idx` (`name`),
  KEY `IDX_41A66AC554B9D732` (`icon_id`),
  KEY `IDX_41A66AC53DA5256D` (`image_id`),
  CONSTRAINT `FK_41A66AC53DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_41A66AC554B9D732` FOREIGN KEY (`icon_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_theme` WRITE;
/*!40000 ALTER TABLE `app_theme` DISABLE KEYS */;

INSERT INTO `app_theme` (`id`, `name`, `deleted_at`, `content`, `colour`, `slug`, `icon_id`, `image_id`)
VALUES
	(10,'Bouwlogistiek',NULL,NULL,'light-green',NULL,4,9),
	(11,'Data',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','blue','data',5,10),
	(12,'CO₂ Reductie',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','dark-blue','co2-reductie',2,10),
	(13,'Retail',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','orange','retail',6,10),
	(14,'Stadslogistiek',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','yellow','stadslogistiek',3,10),
	(15,'Wegtransport',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','green','wegtransport',7,10),
	(16,'Synchromodaal Transport',NULL,'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','dark-green','synchromodaal-transport',8,10);

/*!40000 ALTER TABLE `app_theme` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_theme_company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_theme_company`;

CREATE TABLE `app_theme_company` (
  `company_id` int(11) NOT NULL,
  `theme_id` bigint(20) NOT NULL,
  PRIMARY KEY (`company_id`,`theme_id`),
  KEY `IDX_8CA131C0979B1AD6` (`company_id`),
  KEY `IDX_8CA131C059027487` (`theme_id`),
  CONSTRAINT `FK_8CA131C059027487` FOREIGN KEY (`theme_id`) REFERENCES `app_theme` (`id`),
  CONSTRAINT `FK_8CA131C0979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `app_user_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_theme_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_theme_page_parts`;

CREATE TABLE `app_theme_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listTitle` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_theme_page_parts` WRITE;
/*!40000 ALTER TABLE `app_theme_page_parts` DISABLE KEYS */;

INSERT INTO `app_theme_page_parts` (`id`, `title`, `content`, `listTitle`)
VALUES
	(1,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(2,NULL,NULL,NULL),
	(3,NULL,NULL,NULL),
	(4,'We helpen organisaties op  weg Naar een duurzame  en efficiënte Bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>',NULL),
	(5,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(6,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(7,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(8,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(9,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(10,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(11,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.'),
	(12,'We helpen organisaties op weg naar een duurzame en efficiënte bedrijfsvoering.','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>','Dit doen we op de volgende thema’s.');

/*!40000 ALTER TABLE `app_theme_page_parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_theme_page_parts_app_theme
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_theme_page_parts_app_theme`;

CREATE TABLE `app_theme_page_parts_app_theme` (
  `theme_part_id` bigint(20) NOT NULL,
  `theme_id` bigint(20) NOT NULL,
  PRIMARY KEY (`theme_part_id`,`theme_id`),
  KEY `IDX_2349B66515804ECF` (`theme_part_id`),
  KEY `IDX_2349B66559027487` (`theme_id`),
  CONSTRAINT `FK_2349B66515804ECF` FOREIGN KEY (`theme_part_id`) REFERENCES `app_theme_page_parts` (`id`),
  CONSTRAINT `FK_2349B66559027487` FOREIGN KEY (`theme_id`) REFERENCES `app_theme` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_theme_page_parts_app_theme` WRITE;
/*!40000 ALTER TABLE `app_theme_page_parts_app_theme` DISABLE KEYS */;

INSERT INTO `app_theme_page_parts_app_theme` (`theme_part_id`, `theme_id`)
VALUES
	(1,10),
	(1,11),
	(1,12),
	(1,13),
	(1,14),
	(1,15),
	(1,16),
	(4,10),
	(4,11),
	(4,12),
	(4,13),
	(4,14),
	(4,15),
	(4,16),
	(5,10),
	(5,11),
	(5,12),
	(5,13),
	(5,14),
	(5,15),
	(5,16),
	(6,10),
	(6,11),
	(6,12),
	(6,13),
	(6,14),
	(6,15),
	(6,16),
	(7,10),
	(7,11),
	(7,12),
	(7,13),
	(7,14),
	(7,15),
	(7,16),
	(8,10),
	(8,11),
	(8,12),
	(8,13),
	(8,14),
	(8,15),
	(8,16),
	(9,10),
	(9,11),
	(9,12),
	(9,13),
	(9,14),
	(9,15),
	(9,16),
	(10,10),
	(10,11),
	(10,12),
	(10,13),
	(10,14),
	(10,15),
	(10,16),
	(11,10),
	(11,11),
	(11,12),
	(11,13),
	(11,14),
	(11,15),
	(11,16),
	(12,10),
	(12,11),
	(12,12),
	(12,13),
	(12,14),
	(12,15),
	(12,16);

/*!40000 ALTER TABLE `app_theme_page_parts_app_theme` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_to_top_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_to_top_page_parts`;

CREATE TABLE `app_to_top_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_toc_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_toc_page_parts`;

CREATE TABLE `app_toc_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_user`;

CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_changed` tinyint(1) DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dynamics_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastDynamicsSync` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `syncToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_88BDF3E992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_88BDF3E9A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_88BDF3E9C05FB297` (`confirmation_token`),
  UNIQUE KEY `UNIQ_88BDF3E9F2B46D6D` (`dynamics_id`),
  KEY `IDX_88BDF3E9979B1AD6` (`company_id`),
  CONSTRAINT `FK_88BDF3E9979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `app_user_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;

INSERT INTO `app_user` (`id`, `company_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `firstName`, `lastName`, `telephone`, `newsletter`, `locale`, `country`, `gender`, `account_id`, `admin_locale`, `password_changed`, `google_id`, `dynamics_id`, `lastDynamicsSync`, `deleted_at`, `created_at`, `updated_at`, `syncToken`)
VALUES
	(1,NULL,'superadmin','superadmin','superadmin','superadmin',1,'.sLMqD5v4uLogfnDqTZkcN/K9ToC9r/RIuZosmzxHgc','$argon2i$v=19$m=1024,t=2,p=2$cmxnSmhOLngzVVdvZGNMMQ$62eT6kyA9+LUTPu9SwnF2wUBuR/UAe8tsrSJmEB8E5M','2019-07-01 11:53:07',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','2019-07-01 11:53:07',NULL),
	(6,8,'lean-green@test.dev','lean-green@test.dev','lean-green@test.dev','lean-green@test.dev',1,'wB.4byajrvwh9NNYjdCv2RH2wDNj2.5aOVHXGOyWWxU','$argon2i$v=19$m=1024,t=2,p=2$THVMZmxFMVNDZzhlRXVoNg$3dWNFVq3kcxMqwl+eIrl5v6zGjxfXkfcBlILHcfx3po','2019-06-14 14:37:42',NULL,NULL,'a:1:{i:0;s:18:\"ROLE_DYNAMICS_USER\";}','Test','User',NULL,0,'nl_NL',NULL,'Man','0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7',NULL,NULL,NULL,'bc7eada2-8d8e-e911-a9a8-000d3ab5cc93','2019-06-18 11:12:00',NULL,'0000-00-00 00:00:00','2019-06-18 11:12:00',''),
	(14,NULL,'martijn+lg2@raakrdam.nl','martijn+lg2@raakrdam.nl','martijn+lg2@raakrdam.nl','martijn+lg2@raakrdam.nl',1,'ntx0iQrXliROI5VOFB3QkRJ9LeH3yFFf4Vhdl1DBs8k','$argon2i$v=19$m=1024,t=2,p=2$NzQyZTlCZGtzMTc0a0pSaA$s6ZqE0JH0Qe6G4VFyvBfaM9zOLOP5jwubYCt5u7XkUw',NULL,NULL,NULL,'a:1:{i:0;s:18:\"ROLE_DYNAMICS_USER\";}','Martijn','van Beek Test',NULL,NULL,'nl_NL',NULL,'Male',NULL,NULL,NULL,NULL,'08ec5f72-a991-e911-a9a7-000d3ab5ce5c',NULL,NULL,'2019-06-18 11:14:00','2019-06-18 11:14:33',NULL),
	(15,NULL,'martijn+lg3@raakrdam.nl','martijn+lg3@raakrdam.nl','martijn+lg3@raakrdam.nl','martijn+lg3@raakrdam.nl',1,'tATEmcJPcy4p8XLxrubQvxNMXxwD.8Mtj2AfPA0TvqE','$argon2i$v=19$m=1024,t=2,p=2$NVdkMEw1UUtLYU1HVGp3Rw$TgLDjbIWg4HNSY7LVygJ0CGdHLtIV33ADH+DKBCo/x4',NULL,NULL,NULL,'a:1:{i:0;s:18:\"ROLE_DYNAMICS_USER\";}','Martijn','van Beek Test',NULL,NULL,'nl_NL',NULL,'Male',NULL,NULL,NULL,NULL,'aeedead1-ae91-e911-a99a-000d3ab5ccb7',NULL,NULL,'2019-06-18 11:50:31','2019-06-18 11:58:23',NULL),
	(16,NULL,'martijn+lg4@raakrdam.nl','martijn+lg4@raakrdam.nl','martijn+lg4@raakrdam.nl','martijn+lg4@raakrdam.nl',0,'26HY3eAz5OzyVAtmAw6WyWt9OY3kC2D5v.B1.36aNv0','$argon2i$v=19$m=1024,t=2,p=2$a1pkczZKenAvUlU5R1FMRA$6rmht4E5iQjzDj9sjyMX+RfyIKW5NZ8xKLx/u1TmI7A',NULL,'5VktUKm6w7j2zdrO7pTkwpLJVvP5ONOMpUuUKcnI1S8',NULL,'a:0:{}','Martijn','van Beek Test',NULL,NULL,'nl_NL',NULL,'Male',NULL,NULL,NULL,NULL,'d5361b00-b191-e911-a9a5-000d3ab4cccd',NULL,NULL,'2019-06-18 12:08:02','2019-06-18 12:08:26',NULL),
	(17,NULL,'martijn+lg5@raakrdam.nl','martijn+lg5@raakrdam.nl','martijn+lg5@raakrdam.nl','martijn+lg5@raakrdam.nl',0,'U9KFpd6B.yDzEniDRocxd0pof1V7Lbm7D6t0G3s7e64','$argon2i$v=19$m=1024,t=2,p=2$WEFyU2phYWtydTVpR0JYVA$1fro3hoROaCOYNyflEfbonagb4IL+hfi0SxvAlatwXo',NULL,'ySrIUzPMvaW5C0YWyny2W6Tis0YMrO8Eb51upSz-gIw',NULL,'a:0:{}','Martijn','van Beek Test',NULL,NULL,'nl_NL',NULL,'Male',NULL,NULL,NULL,NULL,'c4b0da0e-b291-e911-a9a7-000d3ab5ce5c',NULL,NULL,'2019-06-18 12:15:31','2019-06-18 12:16:00',NULL),
	(18,8,'martijn+lg6@raakrdam.nl','martijn+lg6@raakrdam.nl','martijn+lg6@raakrdam.nl','martijn+lg6@raakrdam.nl',1,'bDEsejInOOAIVD8nYS0aKpbX6sDFG6rlwWU7sow4EGc','$argon2i$v=19$m=1024,t=2,p=2$RTgxWlh0UjR3aUxsUXVkVA$/fi+umNqCWo0txVF40LDxfB61S5oGsM71eXq4nD8Epk','2019-06-19 14:19:16',NULL,NULL,'a:1:{i:0;s:18:\"ROLE_DYNAMICS_USER\";}','Martijn','van Beek Test',NULL,0,'nl_NL',NULL,'Man','0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7',NULL,NULL,NULL,'e935473f-b791-e911-a9b0-000d3ab5c6ea','2019-06-19 14:19:31',NULL,'2019-06-18 12:52:50','2019-06-19 14:19:31',NULL);

/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_user_company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_user_company`;

CREATE TABLE `app_user_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dynamics_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastDynamicsSync` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `syncToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `leanAndGreenEnabled` tinyint(1) NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organisation_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamber_of_commerce_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D53C0DC4F2B46D6D` (`dynamics_id`),
  KEY `IDX_D53C0DC42B19A734` (`industry_id`),
  CONSTRAINT `FK_D53C0DC42B19A734` FOREIGN KEY (`industry_id`) REFERENCES `app_industry` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `app_user_company` WRITE;
/*!40000 ALTER TABLE `app_user_company` DISABLE KEYS */;

INSERT INTO `app_user_company` (`id`, `name`, `dynamics_id`, `lastDynamicsSync`, `enabled`, `deleted_at`, `created_at`, `updated_at`, `syncToken`, `leanAndGreenEnabled`, `street`, `postal_code`, `city`, `country`, `organisation_type`, `chamber_of_commerce_number`, `industry_id`)
VALUES
	(8,'RaakRdam Dev','0e8f77f0-9f8e-e911-a99a-000d3ab5ccb7','2019-06-19 10:33:50',1,NULL,'2019-06-18 07:37:48','2019-06-19 11:45:35','bevteg73bvfJMGgMXq4ruJj0FgRawyS7sn9M1PrKG2g',1,'Schiekade 189','3013 BR','ROTTERDAM','Nederland','Mobiliteitsdienst','0987654321',NULL);

/*!40000 ALTER TABLE `app_user_company` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table app_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_user_groups`;

CREATE TABLE `app_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_CCDA04F1A76ED395` (`user_id`),
  KEY `IDX_CCDA04F1FE54D947` (`group_id`),
  CONSTRAINT `FK_CCDA04F1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_CCDA04F1FE54D947` FOREIGN KEY (`group_id`) REFERENCES `kuma_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_video_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_video_page_parts`;

CREATE TABLE `app_video_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video_media_id` bigint(20) DEFAULT NULL,
  `thumbnail_media_id` bigint(20) DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1B58A13F421DEFC9` (`video_media_id`),
  KEY `IDX_1B58A13FC364B086` (`thumbnail_media_id`),
  CONSTRAINT `FK_1B58A13F421DEFC9` FOREIGN KEY (`video_media_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_1B58A13FC364B086` FOREIGN KEY (`thumbnail_media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table ext_translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ext_translations`;

CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

LOCK TABLES `ext_translations` WRITE;
/*!40000 ALTER TABLE `ext_translations` DISABLE KEYS */;

INSERT INTO `ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`)
VALUES
	(1,'en','Kunstmaan\\MediaBundle\\Entity\\Folder','name','1','Media'),
	(2,'fr','Kunstmaan\\MediaBundle\\Entity\\Folder','name','1','Média'),
	(3,'en','Kunstmaan\\MediaBundle\\Entity\\Folder','name','2','Images'),
	(4,'nl','Kunstmaan\\MediaBundle\\Entity\\Folder','name','2','Afbeeldingen'),
	(5,'en','Kunstmaan\\MediaBundle\\Entity\\Folder','name','3','Files'),
	(6,'nl','Kunstmaan\\MediaBundle\\Entity\\Folder','name','3','Bestanden'),
	(7,'fr','Kunstmaan\\MediaBundle\\Entity\\Folder','name','3','Fichiers'),
	(8,'en','Kunstmaan\\MediaBundle\\Entity\\Folder','name','4','Slides'),
	(9,'nl','Kunstmaan\\MediaBundle\\Entity\\Folder','name','4','Presentaties'),
	(10,'fr','Kunstmaan\\MediaBundle\\Entity\\Folder','name','4','Presentations'),
	(11,'en','Kunstmaan\\MediaBundle\\Entity\\Folder','name','5','Videos'),
	(12,'nl','Kunstmaan\\MediaBundle\\Entity\\Folder','name','5','Video\'s'),
	(13,'fr','Kunstmaan\\MediaBundle\\Entity\\Folder','name','5','Vidéos'),
	(15,'nl_NL','App\\Entity\\Theme','content','10','<p>Lean &amp; Green is marktleider in CO2-reductieprogrammas. We bieden maatregelen en door de community aangedragen best-practices die niet alleen kosten besparen en tegelijkertijd de belasting op het milieu verlagen.</p>'),
	(16,'en_NL','App\\Entity\\Theme','content','10','<p>A big long list of english bla bla bla</p>'),
	(17,'en_NL','App\\Entity\\Theme','name','10','Construction logistics'),
	(20,'nl_NL','Kunstmaan\\MediaBundle\\Entity\\Folder','name','6','Theme icons'),
	(21,'nl_NL','App\\Entity\\Theme','slug','10','bouwlogistiek'),
	(22,'nl_NL','App\\Entity\\Theme','name','10','Bouwlogistiek'),
	(23,'nl_NL','App\\Entity\\Theme','content','11','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(24,'nl_NL','App\\Entity\\Theme','slug','11','data'),
	(25,'nl_NL','App\\Entity\\Theme','name','11','Data'),
	(26,'nl_NL','App\\Entity\\Theme','content','12','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(27,'nl_NL','App\\Entity\\Theme','slug','12','co2-reductie'),
	(28,'nl_NL','App\\Entity\\Theme','name','12','CO₂ Reductie'),
	(29,'nl_NL','App\\Entity\\Theme','content','13','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(30,'nl_NL','App\\Entity\\Theme','slug','13','retail'),
	(31,'nl_NL','App\\Entity\\Theme','name','13','Retail'),
	(32,'nl_NL','App\\Entity\\Theme','content','14','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(33,'nl_NL','App\\Entity\\Theme','slug','14','stadslogistiek'),
	(34,'nl_NL','App\\Entity\\Theme','name','14','Stadslogistiek'),
	(35,'nl_NL','App\\Entity\\Theme','content','15','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(36,'nl_NL','App\\Entity\\Theme','slug','15','wegtransport'),
	(37,'nl_NL','App\\Entity\\Theme','name','15','Wegtransport'),
	(38,'nl_NL','App\\Entity\\Theme','content','16','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
	(39,'nl_NL','App\\Entity\\Theme','slug','16','synchromodaal-transport'),
	(40,'nl_NL','App\\Entity\\Theme','name','16','Synchromodaal Transport'),
	(41,'en_NL','App\\Entity\\Theme','slug','10','construction-logistics'),
	(42,'en_NL','App\\Entity\\Theme','slug','12','co2-reduction'),
	(43,'en_NL','App\\Entity\\Theme','name','12','CO₂ Reduction'),
	(44,'en_NL','App\\Entity\\Theme','slug','14','citylogistics'),
	(45,'en_NL','App\\Entity\\Theme','name','14','Citylogistics'),
	(46,'en_NL','App\\Entity\\Theme','slug','16','synchromodal-transport'),
	(47,'en_NL','App\\Entity\\Theme','name','16','Synchromodal Transport'),
	(48,'en_NL','App\\Entity\\Theme','slug','15','roadtransport'),
	(49,'en_NL','App\\Entity\\Theme','name','15','Roadtransport'),
	(50,'en_NL','App\\Entity\\Benefit','subtitle','3','Why Lean & Green?'),
	(51,'nl_NL','App\\Entity\\Benefit','subtitle','3','Waarom Lean & Green?'),
	(52,'en_NL','App\\Entity\\Benefit','title','3','Insight into your C02 footprint and reducing measures'),
	(53,'en_NL','App\\Entity\\Benefit','title','2','National recognition as a sustainable partner'),
	(54,'en_NL','App\\Entity\\Benefit','title','1','Access to the Lean & Green network'),
	(55,'en_NL','App\\Entity\\Benefit','title','4','Exemption from the European energy audit'),
	(56,'en_NL','App\\Entity\\Benefit','subtitle','2','Why Lean & Green?'),
	(57,'en_NL','App\\Entity\\Benefit','subtitle','1','Why Lean & Green?'),
	(58,'en_NL','App\\Entity\\Benefit','subtitle','4','Why Lean & Green?');

/*!40000 ALTER TABLE `ext_translations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_acl_changesets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_acl_changesets`;

CREATE TABLE `kuma_acl_changesets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ref_id` bigint(20) NOT NULL,
  `ref_entity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `changeset` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `pid` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_953E7491A76ED395` (`user_id`),
  KEY `idx_acl_changeset_ref` (`ref_id`,`ref_entity_name`),
  CONSTRAINT `FK_953E7491A76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_analytics_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_analytics_config`;

CREATE TABLE `kuma_analytics_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci,
  `token` longtext COLLATE utf8_unicode_ci,
  `account_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `disable_goals` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_analytics_goal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_analytics_goal`;

CREATE TABLE `kuma_analytics_goal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `overview_id` bigint(20) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visits` int(11) NOT NULL,
  `chart_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2EF8AC783504B372` (`overview_id`),
  CONSTRAINT `FK_2EF8AC783504B372` FOREIGN KEY (`overview_id`) REFERENCES `kuma_analytics_overview` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_analytics_overview
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_analytics_overview`;

CREATE TABLE `kuma_analytics_overview` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config_id` bigint(20) DEFAULT NULL,
  `segment_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timespan` int(11) NOT NULL,
  `start_days_ago` int(11) NOT NULL,
  `use_year` tinyint(1) NOT NULL,
  `sessions` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `returning_users` int(11) NOT NULL,
  `new_users` double NOT NULL,
  `pageviews` int(11) NOT NULL,
  `pages_per_session` double NOT NULL,
  `chart_data_max_value` int(11) NOT NULL,
  `avg_session_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chart_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E54548DE24DB0683` (`config_id`),
  KEY `IDX_E54548DEDB296AAD` (`segment_id`),
  CONSTRAINT `FK_E54548DE24DB0683` FOREIGN KEY (`config_id`) REFERENCES `kuma_analytics_config` (`id`),
  CONSTRAINT `FK_E54548DEDB296AAD` FOREIGN KEY (`segment_id`) REFERENCES `kuma_analytics_segment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_analytics_segment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_analytics_segment`;

CREATE TABLE `kuma_analytics_segment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `query` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F38B9AA6D48A2F7C` (`config`),
  CONSTRAINT `FK_F38B9AA6D48A2F7C` FOREIGN KEY (`config`) REFERENCES `kuma_analytics_config` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_audio_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_audio_page_parts`;

CREATE TABLE `kuma_audio_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A30C548EEA9FDD75` (`media_id`),
  CONSTRAINT `FK_A30C548EEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_checkbox_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_checkbox_page_parts`;

CREATE TABLE `kuma_checkbox_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_choice_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_choice_page_parts`;

CREATE TABLE `kuma_choice_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expanded` tinyint(1) DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT NULL,
  `choices` longtext COLLATE utf8_unicode_ci,
  `empty_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_dashboard_configurations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_dashboard_configurations`;

CREATE TABLE `kuma_dashboard_configurations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_dashboard_configurations` WRITE;
/*!40000 ALTER TABLE `kuma_dashboard_configurations` DISABLE KEYS */;

INSERT INTO `kuma_dashboard_configurations` (`id`, `title`, `content`)
VALUES
	(1,'Dashboard','Welkom');

/*!40000 ALTER TABLE `kuma_dashboard_configurations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_download_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_download_page_parts`;

CREATE TABLE `kuma_download_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_65E8DB81EA9FDD75` (`media_id`),
  CONSTRAINT `FK_65E8DB81EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_email_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_email_page_parts`;

CREATE TABLE `kuma_email_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_invalid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_entity_version_lock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_entity_version_lock`;

CREATE TABLE `kuma_entity_version_lock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lockable_entity_id` bigint(20) DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7A2C84EAF9986E2B` (`lockable_entity_id`),
  KEY `evl_owner_entity_idx` (`owner`,`lockable_entity_id`),
  CONSTRAINT `FK_7A2C84EAF9986E2B` FOREIGN KEY (`lockable_entity_id`) REFERENCES `kuma_lockable_entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_exception
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_exception`;

CREATE TABLE `kuma_exception` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url_referer` longtext COLLATE utf8_unicode_ci,
  `hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `events` int(11) NOT NULL,
  `is_resolved` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_43FD618CD1B862B8` (`hash`),
  KEY `idx_exception_is_resolved` (`is_resolved`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_exception` WRITE;
/*!40000 ALTER TABLE `kuma_exception` DISABLE KEYS */;

INSERT INTO `kuma_exception` (`id`, `code`, `url`, `url_referer`, `hash`, `events`, `is_resolved`, `created_at`, `updated_at`)
VALUES
	(1,'400','https://api-lean-green.docker.for.mac.localhost/api/nl_NL/register',NULL,'65407603b25d36b9fb8e07d169863cb9',1,0,'2019-06-14 12:16:43','2019-06-14 12:16:43'),
	(2,'400','https://api-lean-green.docker.for.mac.localhost/api/login_check',NULL,'309a24939d4bdb14af94d630d45c6f2b',1,0,'2019-06-14 12:18:15','2019-06-14 12:18:15'),
	(3,'404','https://api-lean-green.docker.for.mac.localhost/','https://api-lean-green.docker.for.mac.localhost/nl_NL/admin/login','35ce17477ce8f95c67cc93ace2dc9b34',2,0,'2019-06-14 13:20:04','2019-06-17 11:57:41'),
	(4,'404','https://api-lean-green.docker.for.mac.localhost/api/sync/company/',NULL,'5ffb91b36554c82fed2fdc0f7b53ad41',1,0,'2019-06-14 14:16:38','2019-06-14 14:16:38'),
	(5,'404','https://api-lean-green.docker.for.mac.localhost/api/nl_NL/register/confirm/pk_6I5Gsn44s1vZk6I71nX6roRM9GYddBcH4qZ9g2jw',NULL,'b5e7ae0c269b468bf23c73403fdfcce9',1,0,'2019-06-18 11:05:24','2019-06-18 11:05:24'),
	(6,'404','https://api-lean-green.docker.for.mac.localhost/api/nl_NL/register/confirm/MjuoNvnbPJt8SXnLQaqZmXwBOtr573NbPGV3rQoFW4I',NULL,'c6d364598df8aed32596d70600dc98cf',1,0,'2019-06-18 11:59:12','2019-06-18 11:59:12'),
	(7,'404','https://api-lean-green.docker.for.mac.localhost/api/company/sync/NzIqJy7EBF6WElMSAHBG87Wv6JT8-gf-OIRykBSW6vs','https://api-lean-green.docker.for.mac.localhost/nl_NL/admin/settings/companies/8/edit','888bc9e251f847af0d4afbd66cb9c258',1,0,'2019-06-18 14:30:38','2019-06-18 14:30:38'),
	(8,'404','https://api-lean-green.docker.for.mac.localhost/nl_NL/admin/category/','https://api-lean-green.docker.for.mac.localhost/nl_NL/admin/settings/users/','a6d1092eb6d43ea2ca987f8f7640e987',1,0,'2019-06-18 15:05:23','2019-06-18 15:05:23'),
	(9,'404','https://api-lean-green.docker.for.mac.localhost/nl_NL/themes',NULL,'ec38fb4cd0a4cd60b5146f53915e99a2',1,0,'2019-06-18 15:13:14','2019-06-18 15:13:14'),
	(10,'404','https://api-lean-green.docker.for.mac.localhost/nl_NL/theme',NULL,'8994f311b82a30816cafbc60d7eaaa64',1,0,'2019-06-18 15:13:20','2019-06-18 15:13:20'),
	(11,'404','https://api-lean-green.docker.for.mac.localhost/api/nl_NL/theme',NULL,'68576a111a465e009ba0a7803a803795',1,0,'2019-06-18 15:13:35','2019-06-18 15:13:35'),
	(12,'404','https://ontwikkel-api.lean-green.nl/favicon.ico','https://ontwikkel-api.lean-green.nl/api/en_NL/','7a7c33591bc618df46e5999d5397d791',3,0,'2019-06-27 10:45:18','2019-06-27 11:07:45'),
	(13,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/ckeditor/plugins/wordcount/css/wordcount.css','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','ec15499334b013d52e583c227adce112',1,0,'2019-06-27 10:50:18','2019-06-27 10:50:18'),
	(14,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/ckeditor/plugins/notification/css/notification.css','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','45c8368bcce457695a2a270edb528036',1,0,'2019-06-27 10:50:18','2019-06-27 10:50:18'),
	(15,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/img/kunstmaan/kunstmaan_dark.svg','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','98f64fe796a8b87adf7f89f6b6eafc6d',1,0,'2019-06-27 10:50:19','2019-06-27 10:50:19'),
	(16,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/fonts/fa-solid-900.woff2','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','df2a029af8e3e2638c9148e596397fb2',1,0,'2019-06-27 10:50:19','2019-06-27 10:50:19'),
	(17,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/img/jstree/kuma.svg','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','655ff408b8b9ba7e9fe98ae6b470aa81',1,0,'2019-06-27 10:50:22','2019-06-27 10:50:22'),
	(18,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/fonts/fa-regular-400.woff2','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','084ded3f04a7b2c8ec68e9562e65caba',1,0,'2019-06-27 10:50:22','2019-06-27 10:50:22'),
	(19,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/img/jstree/throbber.gif','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','8e54db32f366a923e9536284798f042e',1,0,'2019-06-27 10:50:22','2019-06-27 10:50:22'),
	(20,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/ckeditor/config.js?t=F7J9','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','c68baf1a5d477adf3316997861af18da',1,0,'2019-06-27 10:50:23','2019-06-27 10:50:23'),
	(21,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/fonts/fa-regular-400.woff','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','d2f75fe89777621eb4ecb9b9e64f14f3',1,0,'2019-06-27 10:50:25','2019-06-27 10:50:25'),
	(22,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/fonts/fa-solid-900.woff','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/css/style.css','90c622ed3b9416814d304e844a0fa2b6',1,0,'2019-06-27 10:50:25','2019-06-27 10:50:25'),
	(23,'404','https://ontwikkel-api.lean-green.nl/bundles/kunstmaanadmin/default-theme/ckeditor/lang/en-gb.js?t=F7J9','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','91120375be9b10353a135ab1026ad71b',1,0,'2019-06-27 10:50:26','2019-06-27 10:50:26'),
	(24,'404','https://ontwikkel-api.lean-green.nl/favicon.ico','https://ontwikkel-api.lean-green.nl/api/nl_NL/','3d82e97accb381a4547ba8f88f8846bf',3,0,'2019-06-27 11:36:49','2019-07-01 10:44:50'),
	(25,'404','https://ontwikkel-api.lean-green.nl/uploads/media/5d0ce3842f4e4/ozfeeq4xrfc.jpg','https://ontwikkel-www.lean-green.nl/','75cffa0e02b5d75b99ab31b451ba8aec',1,0,'2019-06-27 12:13:33','2019-06-27 12:13:33'),
	(26,'404','https://ontwikkel-api.lean-green.nl/uploads/media/5d0ce3842f4e4/ozfeeq4xrfc.jpg','https://ontwikkel-www.lean-green.nl/en_NL','11b81a5ad4e6637e1afdbc4a7b1fa5e6',1,0,'2019-06-27 12:13:39','2019-06-27 12:13:39'),
	(27,'404','https://ontwikkel-api.lean-green.nl/api/en_NL/deelnemen',NULL,'3aea9c2ef7f14c9a33d8e170ca0ccabc',1,0,'2019-06-27 16:27:19','2019-06-27 16:27:19'),
	(28,'404','https://ontwikkel-api.lean-green.nl/api/nl_NL/stadslogistiek',NULL,'95e73662ef135d3ad9eec1d97e21ea18',1,0,'2019-06-28 15:28:51','2019-06-28 15:28:51'),
	(29,'404','https://ontwikkel-api.lean-green.nl/api/nl_NL/themes/bouwlogistiek',NULL,'242e11c686f7e47817f580644038a39c',1,0,'2019-06-28 15:30:53','2019-06-28 15:30:53'),
	(30,'404','https://ontwikkel-api.lean-green.nl/favicon.ico','https://ontwikkel-api.lean-green.nl/nl_NL/admin/settings/translations/','523028773a9055044684488017930d50',1,0,'2019-07-01 11:53:09','2019-07-01 11:53:09'),
	(31,'404','https://ontwikkel-api.lean-green.nl/api/nl_NL/benefit',NULL,'2ab86c723c7cbc7aa84fd3af1d16ef9c',2,0,'2019-07-01 12:19:18','2019-07-01 13:38:44'),
	(32,'404','http://127.0.0.1/aaaa',NULL,'d05f1383188a5f048f5334e780a4d699',1,0,'2019-07-02 12:01:35','2019-07-02 12:01:35'),
	(33,'404','https://ontwikkel-api.lean-green.nl/favicon.ico','https://ontwikkel-api.lean-green.nl/en_NL/admin/nodes/1','1720cea4d2e92d381126d9cdb9a600de',1,0,'2019-07-02 13:17:01','2019-07-02 13:17:01'),
	(34,'404','https://ontwikkel-api.lean-green.nl/favicon.ico','https://ontwikkel-api.lean-green.nl/nl_NL/admin/nodes/1','7437a12b70bc7e23d1719f835f7e948c',1,0,'2019-07-02 14:11:11','2019-07-02 14:11:11'),
	(35,'404','http://nginx-app/api/nl_NL/preview/APNGNRSg','https://ontwikkel-www.lean-green.nl/?preview-token=APNGNRSg','de4ce15c2c88c219a8313a62ae545ffc',8,0,'2019-07-02 14:17:53','2019-07-02 14:19:12'),
	(36,'404','https://ontwikkel-api.lean-green.nl/api/nl_NL/news',NULL,'439fd1cfdc03936b1fc56f36f080eedf',1,0,'2019-07-02 15:39:48','2019-07-02 15:39:48'),
	(37,'404','https://ontwikkel-api.lean-green.nl/admin',NULL,'40702f5f628ee74fdcff3cdf0fc9867d',1,0,'2019-07-03 10:24:42','2019-07-03 10:24:42'),
	(38,'404','https://ontwikkel-api.lean-green.nl/nl_NLadmin',NULL,'e2a565e6f99b332ec82e3317aee7bada',1,0,'2019-07-03 10:24:49','2019-07-03 10:24:49'),
	(39,'404','https://ontwikkel-api.lean-green.nl/nl_NL/login',NULL,'808f981f9bfd54322af68845b087f7c9',1,0,'2019-07-03 10:31:14','2019-07-03 10:31:14');

/*!40000 ALTER TABLE `kuma_exception` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_file_upload_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_file_upload_page_parts`;

CREATE TABLE `kuma_file_upload_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_folders`;

CREATE TABLE `kuma_folders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `rel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `lvl` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2D3C07E4727ACA70` (`parent_id`),
  KEY `idx_folder_internal_name` (`internal_name`),
  KEY `idx_folder_name` (`name`),
  KEY `idx_folder_deleted` (`deleted`),
  CONSTRAINT `FK_2D3C07E4727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `kuma_folders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_folders` WRITE;
/*!40000 ALTER TABLE `kuma_folders` DISABLE KEYS */;

INSERT INTO `kuma_folders` (`id`, `parent_id`, `name`, `created_at`, `updated_at`, `rel`, `internal_name`, `lft`, `lvl`, `rgt`, `deleted`)
VALUES
	(1,NULL,'Media','2019-06-14 08:17:39','2019-06-14 08:17:39','media',NULL,1,0,12,0),
	(2,1,'Images','2019-06-14 08:17:39','2019-06-14 08:17:39','image',NULL,2,1,3,0),
	(3,1,'Files','2019-06-14 08:17:39','2019-06-14 08:17:39','files',NULL,4,1,5,0),
	(4,1,'Slides','2019-06-14 08:17:39','2019-06-14 08:17:39','slideshow',NULL,6,1,7,0),
	(5,1,'Videos','2019-06-14 08:17:39','2019-06-14 08:17:39','video',NULL,10,1,11,0),
	(6,1,'Theme icons','2019-06-28 14:24:53','2019-06-28 14:24:53','media',NULL,8,1,9,0);

/*!40000 ALTER TABLE `kuma_folders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_form_submission_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_form_submission_fields`;

CREATE TABLE `kuma_form_submission_fields` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_submission_id` bigint(20) DEFAULT NULL,
  `field_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bfsf_value` tinyint(1) DEFAULT NULL,
  `efsf_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ffsf_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sfsf_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tfsf_value` longtext COLLATE utf8_unicode_ci,
  `cfsf_value` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `expanded` tinyint(1) DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT NULL,
  `choices` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A0B2BD32D17F50A6` (`uuid`),
  KEY `IDX_A0B2BD32422B0E0C` (`form_submission_id`),
  CONSTRAINT `FK_A0B2BD32422B0E0C` FOREIGN KEY (`form_submission_id`) REFERENCES `kuma_form_submissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_form_submissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_form_submissions`;

CREATE TABLE `kuma_form_submissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6493A44D460D9FD7` (`node_id`),
  CONSTRAINT `FK_6493A44D460D9FD7` FOREIGN KEY (`node_id`) REFERENCES `kuma_nodes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_groups`;

CREATE TABLE `kuma_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_groups` WRITE;
/*!40000 ALTER TABLE `kuma_groups` DISABLE KEYS */;

INSERT INTO `kuma_groups` (`id`, `name`)
VALUES
	(1,'Dynamics');

/*!40000 ALTER TABLE `kuma_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_groups_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_groups_roles`;

CREATE TABLE `kuma_groups_roles` (
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`role_id`),
  KEY `IDX_B7EFE85EFE54D947` (`group_id`),
  KEY `IDX_B7EFE85ED60322AC` (`role_id`),
  CONSTRAINT `FK_B7EFE85ED60322AC` FOREIGN KEY (`role_id`) REFERENCES `kuma_roles` (`id`),
  CONSTRAINT `FK_B7EFE85EFE54D947` FOREIGN KEY (`group_id`) REFERENCES `kuma_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_groups_roles` WRITE;
/*!40000 ALTER TABLE `kuma_groups_roles` DISABLE KEYS */;

INSERT INTO `kuma_groups_roles` (`group_id`, `role_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `kuma_groups_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_header_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_header_page_parts`;

CREATE TABLE `kuma_header_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `niv` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_image_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_image_page_parts`;

CREATE TABLE `kuma_image_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `open_in_new_window` tinyint(1) DEFAULT NULL,
  `alt_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CF8612B5EA9FDD75` (`media_id`),
  CONSTRAINT `FK_CF8612B5EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_line_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_line_page_parts`;

CREATE TABLE `kuma_line_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_link_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_link_page_parts`;

CREATE TABLE `kuma_link_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `openinnewwindow` tinyint(1) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_lockable_entity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_lockable_entity`;

CREATE TABLE `kuma_lockable_entity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `entity_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_kuma_lockable_entity_id_class` (`entity_id`,`entity_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_media`;

CREATE TABLE `kuma_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `folder_id` bigint(20) DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metadata` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `filesize` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `removed_from_file_system` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_47400F63D17F50A6` (`uuid`),
  KEY `IDX_47400F63162CB942` (`folder_id`),
  KEY `idx_media_name` (`name`),
  KEY `idx_media_deleted` (`deleted`),
  CONSTRAINT `FK_47400F63162CB942` FOREIGN KEY (`folder_id`) REFERENCES `kuma_folders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_media` WRITE;
/*!40000 ALTER TABLE `kuma_media` DISABLE KEYS */;

INSERT INTO `kuma_media` (`id`, `folder_id`, `uuid`, `name`, `description`, `copyright`, `location`, `content_type`, `metadata`, `created_at`, `updated_at`, `filesize`, `url`, `original_filename`, `deleted`, `removed_from_file_system`)
VALUES
	(1,1,'5d1483d4ddd89','Homepage background',NULL,NULL,'local','image/png','a:2:{s:14:\"original_width\";i:1600;s:15:\"original_height\";i:900;}','2019-06-27 10:52:36','2019-06-27 10:52:36',3937295,'/uploads/media/5d1483d4ddd89/home.png','home.png',0,0),
	(2,6,'5d1606ffe69d2','Theme - C02 reduction',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:24:31','2019-06-28 14:25:18',1515,'/uploads/media/5d1606ffe69d2/theme-c02-reduction.svg','theme-c02-reduction.svg',0,0),
	(3,6,'5d160758184c3','theme-city-logistics.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:00','2019-06-28 14:26:00',1500,'/uploads/media/5d160758184c3/theme-city-logistics.svg','theme-city-logistics.svg',0,0),
	(4,6,'5d160758d2ff3','theme-construction-logistics.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:00','2019-06-28 14:26:00',1245,'/uploads/media/5d160758d2ff3/theme-construction-logistics.svg','theme-construction-logistics.svg',0,0),
	(5,6,'5d160759aaafe','theme-data.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:01','2019-06-28 14:26:01',2043,'/uploads/media/5d160759aaafe/theme-data.svg','theme-data.svg',0,0),
	(6,6,'5d16075a92c76','theme-retail.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:02','2019-06-28 14:26:02',1473,'/uploads/media/5d16075a92c76/theme-retail.svg','theme-retail.svg',0,0),
	(7,6,'5d16075b86054','theme-road-transport.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:03','2019-06-28 14:26:03',1640,'/uploads/media/5d16075b86054/theme-road-transport.svg','theme-road-transport.svg',0,0),
	(8,6,'5d16075c673cc','theme-synchromodal-transport.svg',NULL,NULL,'local','image/svg+xml','a:2:{s:14:\"original_width\";N;s:15:\"original_height\";N;}','2019-06-28 14:26:04','2019-06-28 14:26:04',945,'/uploads/media/5d16075c673cc/theme-synchromodal-transport.svg','theme-synchromodal-transport.svg',0,0),
	(9,1,'5d16092980b48','BG - Co2 Reduction',NULL,NULL,'local','application/zip','a:0:{}','2019-06-28 14:33:45','2019-06-28 15:02:22',18488784,'/uploads/media/5d16092980b48/5d160fde826a3.zip','aerial-2604483.zip',1,0),
	(10,1,'5d161005adf77','aerial-2604483.png',NULL,NULL,'local','image/png','a:2:{s:14:\"original_width\";i:1676;s:15:\"original_height\";i:1118;}','2019-06-28 15:03:01','2019-06-28 15:03:01',3999612,'/uploads/media/5d161005adf77/aerial-2604483.png','aerial-2604483.png',0,0),
	(11,1,'5d1b6b3c71924','Program background',NULL,NULL,'local','image/png','a:2:{s:14:\"original_width\";i:1690;s:15:\"original_height\";i:1128;}','2019-07-02 16:33:32','2019-07-02 16:33:32',2823872,'/uploads/media/5d1b6b3c71924/artboard-1-2.png','artboard-1-2.png',0,0);

/*!40000 ALTER TABLE `kuma_media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_menu`;

CREATE TABLE `kuma_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_menu` WRITE;
/*!40000 ALTER TABLE `kuma_menu` DISABLE KEYS */;

INSERT INTO `kuma_menu` (`id`, `name`, `locale`)
VALUES
	(1,'header','nl_NL'),
	(2,'header','en_NL'),
	(3,'subheader','nl_NL'),
	(4,'subheader','en_NL'),
	(5,'footer','nl_NL'),
	(6,'footer','en_NL'),
	(7,'subfooter','nl_NL'),
	(8,'subfooter','en_NL');

/*!40000 ALTER TABLE `kuma_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_menu_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_menu_item`;

CREATE TABLE `kuma_menu_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  `node_translation_id` bigint(20) DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_window` tinyint(1) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EB603AB1727ACA70` (`parent_id`),
  KEY `IDX_EB603AB1CCD7E912` (`menu_id`),
  KEY `IDX_EB603AB1E0B87CE0` (`node_translation_id`),
  CONSTRAINT `FK_EB603AB1727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `kuma_menu_item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EB603AB1CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `kuma_menu` (`id`),
  CONSTRAINT `FK_EB603AB1E0B87CE0` FOREIGN KEY (`node_translation_id`) REFERENCES `kuma_node_translations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_multi_line_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_multi_line_text_page_parts`;

CREATE TABLE `kuma_multi_line_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_node_queued_node_translation_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_node_queued_node_translation_actions`;

CREATE TABLE `kuma_node_queued_node_translation_actions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_translation_id` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D270D8D1E0B87CE0` (`node_translation_id`),
  KEY `IDX_D270D8D1A76ED395` (`user_id`),
  CONSTRAINT `FK_D270D8D1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_D270D8D1E0B87CE0` FOREIGN KEY (`node_translation_id`) REFERENCES `kuma_node_translations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_node_translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_node_translations`;

CREATE TABLE `kuma_node_translations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) DEFAULT NULL,
  `public_node_version_id` bigint(20) DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` smallint(6) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_kuma_node_translations_node_lang` (`node_id`,`lang`),
  KEY `IDX_5E8968CD460D9FD7` (`node_id`),
  KEY `IDX_5E8968CDB9A563EE` (`public_node_version_id`),
  KEY `idx__node_translation_lang_url` (`lang`,`url`),
  CONSTRAINT `FK_5E8968CD460D9FD7` FOREIGN KEY (`node_id`) REFERENCES `kuma_nodes` (`id`),
  CONSTRAINT `FK_5E8968CDB9A563EE` FOREIGN KEY (`public_node_version_id`) REFERENCES `kuma_node_versions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_node_translations` WRITE;
/*!40000 ALTER TABLE `kuma_node_translations` DISABLE KEYS */;

INSERT INTO `kuma_node_translations` (`id`, `node_id`, `public_node_version_id`, `lang`, `online`, `title`, `slug`, `url`, `weight`, `created`, `updated`)
VALUES
	(1,1,1,'nl_NL',1,'Home',NULL,NULL,0,'2019-06-14 08:17:39','2019-07-01 16:50:44'),
	(2,1,25,'en_NL',1,'Home',NULL,NULL,0,'2019-06-14 08:17:40','2019-07-02 14:24:37'),
	(3,2,3,'nl_NL',1,'Mee doen','program','program',0,'2019-06-14 08:17:40','2019-07-03 16:59:25'),
	(4,2,4,'en_NL',1,'Participate','program','program',0,'2019-06-14 08:17:40','2019-07-02 14:31:41'),
	(5,3,5,'nl_NL',1,'Nieuws & Events','nieuws-events','nieuws-events',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(6,3,6,'en_NL',1,'News & Events','news-events','news-events',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(7,4,7,'nl_NL',1,'Maatregelen','maatregelen','maatregelen',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(8,4,8,'en_NL',1,'Measures','measures','measures',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(9,5,9,'nl_NL',1,'Deelnemers','deelnemers','deelnemers',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(10,5,10,'en_NL',1,'Attendees','attendees','attendees',0,'2019-06-14 08:17:40','2019-06-14 08:17:40'),
	(11,6,28,'nl_NL',1,'First news element','first-news-element','nieuws-events/first-news-element',1,'2019-07-02 14:48:48','2019-07-02 15:40:07');

/*!40000 ALTER TABLE `kuma_node_translations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_node_version_lock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_node_version_lock`;

CREATE TABLE `kuma_node_version_lock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_translation_id` bigint(20) DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `public_version` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AEA13540E0B87CE0` (`node_translation_id`),
  KEY `nt_owner_public_idx` (`owner`,`node_translation_id`,`public_version`),
  CONSTRAINT `FK_AEA13540E0B87CE0` FOREIGN KEY (`node_translation_id`) REFERENCES `kuma_node_translations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_node_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_node_versions`;

CREATE TABLE `kuma_node_versions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_translation_id` bigint(20) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `ref_id` bigint(20) NOT NULL,
  `ref_entity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FF496637E0B87CE0` (`node_translation_id`),
  KEY `IDX_FF49663756A273CC` (`origin_id`),
  KEY `idx_node_version_lookup` (`ref_id`,`ref_entity_name`),
  CONSTRAINT `FK_FF49663756A273CC` FOREIGN KEY (`origin_id`) REFERENCES `kuma_node_versions` (`id`),
  CONSTRAINT `FK_FF496637E0B87CE0` FOREIGN KEY (`node_translation_id`) REFERENCES `kuma_node_translations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_node_versions` WRITE;
/*!40000 ALTER TABLE `kuma_node_versions` DISABLE KEYS */;

INSERT INTO `kuma_node_versions` (`id`, `node_translation_id`, `origin_id`, `type`, `owner`, `created`, `updated`, `ref_id`, `ref_entity_name`)
VALUES
	(1,1,22,'public','superadmin','2019-07-01 15:23:49','2019-07-01 16:50:44',1,'App\\Entity\\Pages\\HomePage'),
	(2,2,25,'draft','superadmin','2019-07-02 14:24:37','2019-07-02 14:24:36',2,'App\\Entity\\Pages\\HomePage'),
	(3,3,30,'public','superadmin','2019-07-03 15:51:18','2019-07-03 16:59:25',1,'App\\Entity\\Pages\\ContentPage'),
	(4,4,27,'public','superadmin','2019-07-02 14:30:59','2019-07-02 14:31:41',2,'App\\Entity\\Pages\\ContentPage'),
	(5,5,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',1,'App\\Entity\\Pages\\NewsOverviewPage'),
	(6,6,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',2,'App\\Entity\\Pages\\NewsOverviewPage'),
	(7,7,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',3,'App\\Entity\\Pages\\ContentPage'),
	(8,8,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',4,'App\\Entity\\Pages\\ContentPage'),
	(9,9,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',5,'App\\Entity\\Pages\\ContentPage'),
	(10,10,NULL,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',6,'App\\Entity\\Pages\\ContentPage'),
	(11,1,1,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',3,'App\\Entity\\Pages\\HomePage'),
	(12,2,2,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',4,'App\\Entity\\Pages\\HomePage'),
	(13,1,1,'public','superadmin','2019-06-19 14:18:49','2019-06-19 14:18:51',5,'App\\Entity\\Pages\\HomePage'),
	(14,2,2,'public','superadmin','2019-06-27 10:50:59','2019-06-27 10:56:26',6,'App\\Entity\\Pages\\HomePage'),
	(15,1,1,'public','superadmin','2019-06-27 10:51:53','2019-06-27 10:53:33',7,'App\\Entity\\Pages\\HomePage'),
	(16,2,2,'public','superadmin','2019-06-27 16:15:13','2019-06-27 16:15:13',8,'App\\Entity\\Pages\\HomePage'),
	(17,2,2,'public','superadmin','2019-06-28 14:41:25','2019-06-28 14:41:26',9,'App\\Entity\\Pages\\HomePage'),
	(18,1,1,'public','superadmin','2019-06-28 14:23:00','2019-06-28 15:24:48',10,'App\\Entity\\Pages\\HomePage'),
	(19,2,2,'public','superadmin','2019-06-28 16:22:43','2019-06-28 16:23:13',11,'App\\Entity\\Pages\\HomePage'),
	(20,2,2,'public','superadmin','2019-07-01 10:46:25','2019-07-01 10:46:25',12,'App\\Entity\\Pages\\HomePage'),
	(21,1,1,'public','superadmin','2019-07-01 10:46:16','2019-07-01 10:46:17',13,'App\\Entity\\Pages\\HomePage'),
	(22,1,1,'public','superadmin','2019-07-01 13:39:15','2019-07-01 13:39:16',14,'App\\Entity\\Pages\\HomePage'),
	(23,2,2,'public','superadmin','2019-07-01 13:38:37','2019-07-01 13:38:38',15,'App\\Entity\\Pages\\HomePage'),
	(24,2,23,'public','superadmin','2019-07-01 15:53:48','2019-07-02 14:23:17',16,'App\\Entity\\Pages\\HomePage'),
	(25,2,2,'public','superadmin','2019-07-02 14:23:17','2019-07-02 14:24:36',17,'App\\Entity\\Pages\\HomePage'),
	(26,3,3,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',7,'App\\Entity\\Pages\\ContentPage'),
	(27,4,4,'public','superadmin','2019-06-14 08:17:40','2019-06-14 08:17:40',8,'App\\Entity\\Pages\\ContentPage'),
	(28,11,NULL,'public','superadmin','2019-07-02 14:48:48','2019-07-02 15:40:07',1,'App\\Entity\\Pages\\NewsPage'),
	(29,3,3,'public','superadmin','2019-07-02 14:30:47','2019-07-02 14:31:52',9,'App\\Entity\\Pages\\ContentPage'),
	(30,3,3,'public','superadmin','2019-07-02 16:14:24','2019-07-02 16:34:06',10,'App\\Entity\\Pages\\ContentPage');

/*!40000 ALTER TABLE `kuma_node_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_nodes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_nodes`;

CREATE TABLE `kuma_nodes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `lvl` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `hidden_from_nav` tinyint(1) NOT NULL,
  `ref_entity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `internal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3051AB93727ACA70` (`parent_id`),
  KEY `idx_node_internal_name` (`internal_name`),
  KEY `idx_node_ref_entity_name` (`ref_entity_name`),
  KEY `idx_node_tree` (`deleted`,`hidden_from_nav`,`lft`,`rgt`),
  CONSTRAINT `FK_3051AB93727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `kuma_nodes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_nodes` WRITE;
/*!40000 ALTER TABLE `kuma_nodes` DISABLE KEYS */;

INSERT INTO `kuma_nodes` (`id`, `parent_id`, `lft`, `lvl`, `rgt`, `deleted`, `hidden_from_nav`, `ref_entity_name`, `internal_name`)
VALUES
	(1,NULL,1,0,12,0,0,'App\\Entity\\Pages\\HomePage','homepage'),
	(2,1,2,1,3,0,0,'App\\Entity\\Pages\\ContentPage','deelnemen'),
	(3,1,4,1,7,0,0,'App\\Entity\\Pages\\NewsOverviewPage','nieuws'),
	(4,1,8,1,9,0,0,'App\\Entity\\Pages\\ContentPage','maatregelen'),
	(5,1,10,1,11,0,0,'App\\Entity\\Pages\\ContentPage','deelnemers'),
	(6,3,5,2,6,0,0,'App\\Entity\\Pages\\NewsPage',NULL);

/*!40000 ALTER TABLE `kuma_nodes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_nodes_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_nodes_search`;

CREATE TABLE `kuma_nodes_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) DEFAULT NULL,
  `boost` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1560AF72460D9FD7` (`node_id`),
  CONSTRAINT `FK_1560AF72460D9FD7` FOREIGN KEY (`node_id`) REFERENCES `kuma_nodes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_page_part_refs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_page_part_refs`;

CREATE TABLE `kuma_page_part_refs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pageId` bigint(20) NOT NULL,
  `pageEntityname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequencenumber` int(11) NOT NULL,
  `page_part_id` bigint(20) NOT NULL,
  `page_part_entityname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_page_part_search` (`pageId`,`pageEntityname`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_page_part_refs` WRITE;
/*!40000 ALTER TABLE `kuma_page_part_refs` DISABLE KEYS */;

INSERT INTO `kuma_page_part_refs` (`id`, `pageId`, `pageEntityname`, `context`, `sequencenumber`, `page_part_id`, `page_part_entityname`, `created`, `updated`)
VALUES
	(9,3,'App\\Entity\\Pages\\HomePage','main',1,3,'App\\Entity\\PageParts\\HeaderPagePart','2019-06-19 14:18:49','2019-06-19 14:18:49'),
	(10,3,'App\\Entity\\Pages\\HomePage','main',2,3,'App\\Entity\\PageParts\\LinePagePart','2019-06-19 14:18:49','2019-06-19 14:18:49'),
	(11,3,'App\\Entity\\Pages\\HomePage','main',3,3,'App\\Entity\\PageParts\\ButtonPagePart','2019-06-19 14:18:49','2019-06-19 14:18:49'),
	(12,3,'App\\Entity\\Pages\\HomePage','main',4,3,'App\\Entity\\PageParts\\TextPagePart','2019-06-19 14:18:49','2019-06-19 14:18:49'),
	(13,1,'App\\Entity\\Pages\\HomePage','main',1,1,'App\\Entity\\PageParts\\ThemePagePart','2019-06-19 14:18:51','2019-07-01 15:53:29'),
	(14,4,'App\\Entity\\Pages\\HomePage','main',1,4,'App\\Entity\\PageParts\\HeaderPagePart','2019-06-27 10:50:59','2019-06-27 10:50:59'),
	(15,4,'App\\Entity\\Pages\\HomePage','main',2,4,'App\\Entity\\PageParts\\LinePagePart','2019-06-27 10:50:59','2019-06-27 10:50:59'),
	(16,4,'App\\Entity\\Pages\\HomePage','main',3,4,'App\\Entity\\PageParts\\ButtonPagePart','2019-06-27 10:50:59','2019-06-27 10:50:59'),
	(17,4,'App\\Entity\\Pages\\HomePage','main',4,4,'App\\Entity\\PageParts\\TextPagePart','2019-06-27 10:50:59','2019-06-27 10:50:59'),
	(18,5,'App\\Entity\\Pages\\HomePage','main',1,5,'App\\Entity\\PageParts\\HeaderPagePart','2019-06-27 10:51:53','2019-06-27 10:51:53'),
	(19,5,'App\\Entity\\Pages\\HomePage','main',2,5,'App\\Entity\\PageParts\\LinePagePart','2019-06-27 10:51:53','2019-06-27 10:51:53'),
	(20,5,'App\\Entity\\Pages\\HomePage','main',3,5,'App\\Entity\\PageParts\\ButtonPagePart','2019-06-27 10:51:53','2019-06-27 10:51:53'),
	(21,5,'App\\Entity\\Pages\\HomePage','main',4,5,'App\\Entity\\PageParts\\TextPagePart','2019-06-27 10:51:53','2019-06-27 10:51:53'),
	(22,1,'App\\Entity\\Pages\\HomePage','main',2,1,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-27 10:52:51','2019-07-01 13:39:16'),
	(23,1,'App\\Entity\\Pages\\HomePage','main',3,1,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-27 10:53:33','2019-07-01 15:53:29'),
	(24,2,'App\\Entity\\Pages\\HomePage','main',1,2,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-27 10:55:09','2019-07-01 15:53:48'),
	(25,2,'App\\Entity\\Pages\\HomePage','main',3,2,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-27 10:55:09','2019-07-01 15:53:48'),
	(26,6,'App\\Entity\\Pages\\HomePage','main',1,3,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-27 16:15:13','2019-06-27 16:15:13'),
	(27,6,'App\\Entity\\Pages\\HomePage','main',2,3,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-27 16:15:13','2019-06-27 16:15:13'),
	(28,2,'App\\Entity\\Pages\\HomePage','main',2,1,'App\\Entity\\PageParts\\ThemePagePart','2019-06-27 16:15:13','2019-07-01 15:53:48'),
	(29,7,'App\\Entity\\Pages\\HomePage','main',1,4,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-28 14:23:00','2019-06-28 14:23:00'),
	(30,7,'App\\Entity\\Pages\\HomePage','main',2,4,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-28 14:23:00','2019-06-28 14:23:00'),
	(31,7,'App\\Entity\\Pages\\HomePage','main',3,2,'App\\Entity\\PageParts\\ThemePagePart','2019-06-28 14:23:00','2019-06-28 14:23:00'),
	(32,8,'App\\Entity\\Pages\\HomePage','main',1,3,'App\\Entity\\PageParts\\ThemePagePart','2019-06-28 14:41:25','2019-06-28 14:41:25'),
	(33,8,'App\\Entity\\Pages\\HomePage','main',2,5,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-28 14:41:25','2019-06-28 14:41:25'),
	(34,8,'App\\Entity\\Pages\\HomePage','main',3,5,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-28 14:41:25','2019-06-28 14:41:25'),
	(35,9,'App\\Entity\\Pages\\HomePage','main',1,4,'App\\Entity\\PageParts\\ThemePagePart','2019-06-28 16:22:43','2019-06-28 16:22:43'),
	(36,9,'App\\Entity\\Pages\\HomePage','main',2,6,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-06-28 16:22:43','2019-06-28 16:22:43'),
	(37,9,'App\\Entity\\Pages\\HomePage','main',3,6,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-06-28 16:22:43','2019-06-28 16:22:43'),
	(38,10,'App\\Entity\\Pages\\HomePage','main',1,7,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 10:46:16','2019-07-01 10:46:16'),
	(39,10,'App\\Entity\\Pages\\HomePage','main',2,7,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 10:46:16','2019-07-01 10:46:16'),
	(40,10,'App\\Entity\\Pages\\HomePage','main',3,5,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 10:46:16','2019-07-01 10:46:16'),
	(41,11,'App\\Entity\\Pages\\HomePage','main',1,6,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 10:46:25','2019-07-01 10:46:25'),
	(42,11,'App\\Entity\\Pages\\HomePage','main',2,8,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 10:46:25','2019-07-01 10:46:25'),
	(43,11,'App\\Entity\\Pages\\HomePage','main',3,8,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 10:46:25','2019-07-01 10:46:25'),
	(44,12,'App\\Entity\\Pages\\HomePage','main',1,9,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 13:38:37','2019-07-01 13:38:37'),
	(45,12,'App\\Entity\\Pages\\HomePage','main',2,7,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 13:38:37','2019-07-01 13:38:37'),
	(46,12,'App\\Entity\\Pages\\HomePage','main',3,9,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 13:38:37','2019-07-01 13:38:37'),
	(47,2,'App\\Entity\\Pages\\HomePage','main',4,1,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-01 13:38:38','2019-07-01 15:53:48'),
	(48,13,'App\\Entity\\Pages\\HomePage','main',1,10,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 13:39:15','2019-07-01 13:39:15'),
	(49,13,'App\\Entity\\Pages\\HomePage','main',2,8,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 13:39:15','2019-07-01 13:39:15'),
	(50,13,'App\\Entity\\Pages\\HomePage','main',3,10,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 13:39:15','2019-07-01 13:39:15'),
	(51,1,'App\\Entity\\Pages\\HomePage','main',4,2,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-01 13:39:16','2019-07-01 15:53:29'),
	(52,14,'App\\Entity\\Pages\\HomePage','main',1,3,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-01 15:23:49','2019-07-01 15:23:49'),
	(53,14,'App\\Entity\\Pages\\HomePage','main',2,11,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 15:23:49','2019-07-01 15:23:49'),
	(54,14,'App\\Entity\\Pages\\HomePage','main',3,9,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 15:23:49','2019-07-01 15:23:49'),
	(55,14,'App\\Entity\\Pages\\HomePage','main',4,11,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 15:23:49','2019-07-01 15:23:49'),
	(56,15,'App\\Entity\\Pages\\HomePage','main',1,4,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-01 15:53:48','2019-07-01 15:53:48'),
	(57,15,'App\\Entity\\Pages\\HomePage','main',2,12,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-01 15:53:48','2019-07-01 15:53:48'),
	(58,15,'App\\Entity\\Pages\\HomePage','main',3,10,'App\\Entity\\PageParts\\ThemePagePart','2019-07-01 15:53:48','2019-07-01 15:53:48'),
	(59,15,'App\\Entity\\Pages\\HomePage','main',4,12,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-01 15:53:48','2019-07-01 15:53:48'),
	(60,1,'App\\Entity\\Pages\\HomePage','main',5,1,'App\\Entity\\PageParts\\HomeCTAPagePart','2019-07-01 16:50:44','2019-07-01 16:50:44'),
	(61,2,'App\\Entity\\Pages\\HomePage','main',5,2,'App\\Entity\\PageParts\\HomeCTAPagePart','2019-07-01 16:51:53','2019-07-01 16:51:53'),
	(62,16,'App\\Entity\\Pages\\HomePage','main',1,13,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-02 14:23:17','2019-07-02 14:23:17'),
	(63,16,'App\\Entity\\Pages\\HomePage','main',2,11,'App\\Entity\\PageParts\\ThemePagePart','2019-07-02 14:23:17','2019-07-02 14:23:17'),
	(64,16,'App\\Entity\\Pages\\HomePage','main',3,13,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-02 14:23:17','2019-07-02 14:23:17'),
	(65,16,'App\\Entity\\Pages\\HomePage','main',4,5,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-02 14:23:17','2019-07-02 14:23:17'),
	(66,16,'App\\Entity\\Pages\\HomePage','main',5,3,'App\\Entity\\PageParts\\HomeCTAPagePart','2019-07-02 14:23:17','2019-07-02 14:23:17'),
	(67,17,'App\\Entity\\Pages\\HomePage','main',1,14,'App\\Entity\\PageParts\\HomeHeaderPagePart','2019-07-02 14:24:36','2019-07-02 14:24:36'),
	(68,17,'App\\Entity\\Pages\\HomePage','main',2,12,'App\\Entity\\PageParts\\ThemePagePart','2019-07-02 14:24:37','2019-07-02 14:24:37'),
	(69,17,'App\\Entity\\Pages\\HomePage','main',3,14,'App\\Entity\\PageParts\\HomeStatisticsPagePart','2019-07-02 14:24:37','2019-07-02 14:24:37'),
	(70,17,'App\\Entity\\Pages\\HomePage','main',4,6,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-02 14:24:37','2019-07-02 14:24:37'),
	(71,17,'App\\Entity\\Pages\\HomePage','main',5,4,'App\\Entity\\PageParts\\HomeCTAPagePart','2019-07-02 14:24:37','2019-07-02 14:24:37'),
	(72,1,'App\\Entity\\Pages\\ContentPage','main',2,7,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-02 14:30:48','2019-07-02 16:34:06'),
	(73,2,'App\\Entity\\Pages\\ContentPage','main',1,8,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-02 14:30:59','2019-07-02 14:30:59'),
	(74,1,'App\\Entity\\Pages\\NewsPage','newsmain',1,6,'App\\Entity\\PageParts\\TextPagePart','2019-07-02 14:50:57','2019-07-02 14:50:57'),
	(75,9,'App\\Entity\\Pages\\ContentPage','main',1,9,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-02 16:14:24','2019-07-02 16:14:24'),
	(76,1,'App\\Entity\\Pages\\ContentPage','main',1,1,'App\\Entity\\PageParts\\ProgramHeaderPagePart','2019-07-02 16:34:06','2019-07-02 16:34:06'),
	(77,10,'App\\Entity\\Pages\\ContentPage','main',1,2,'App\\Entity\\PageParts\\ProgramHeaderPagePart','2019-07-03 15:51:18','2019-07-03 15:51:18'),
	(78,10,'App\\Entity\\Pages\\ContentPage','main',2,10,'App\\Entity\\PageParts\\BenefitPagePart','2019-07-03 15:51:18','2019-07-03 15:51:18'),
	(79,1,'App\\Entity\\Pages\\ContentPage','main',3,1,'App\\Entity\\PageParts\\ProgramLeanGreenPagePart','2019-07-03 15:51:19','2019-07-03 15:51:19'),
	(80,1,'App\\Entity\\Pages\\ContentPage','main',4,1,'App\\Entity\\PageParts\\ProgramStarwayPagePart','2019-07-03 16:59:25','2019-07-03 16:59:25');

/*!40000 ALTER TABLE `kuma_page_part_refs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_page_template_configuration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_page_template_configuration`;

CREATE TABLE `kuma_page_template_configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page_id` bigint(20) NOT NULL,
  `page_entity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_page_template_config_search` (`page_id`,`page_entity_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_page_template_configuration` WRITE;
/*!40000 ALTER TABLE `kuma_page_template_configuration` DISABLE KEYS */;

INSERT INTO `kuma_page_template_configuration` (`id`, `page_id`, `page_entity_name`, `page_template`)
VALUES
	(1,3,'App\\Entity\\Pages\\HomePage','Home page'),
	(2,1,'App\\Entity\\Pages\\HomePage','Home page'),
	(3,4,'App\\Entity\\Pages\\HomePage','Home page'),
	(4,2,'App\\Entity\\Pages\\HomePage','Home page'),
	(5,5,'App\\Entity\\Pages\\HomePage','Home page'),
	(6,6,'App\\Entity\\Pages\\HomePage','Home page'),
	(7,7,'App\\Entity\\Pages\\HomePage','Home page'),
	(8,8,'App\\Entity\\Pages\\HomePage','Home page'),
	(9,9,'App\\Entity\\Pages\\HomePage','Home page'),
	(10,10,'App\\Entity\\Pages\\HomePage','Home page'),
	(11,11,'App\\Entity\\Pages\\HomePage','Home page'),
	(12,12,'App\\Entity\\Pages\\HomePage','Home page'),
	(13,13,'App\\Entity\\Pages\\HomePage','Home page'),
	(14,14,'App\\Entity\\Pages\\HomePage','Home page'),
	(15,15,'App\\Entity\\Pages\\HomePage','Home page'),
	(16,16,'App\\Entity\\Pages\\HomePage','Home page'),
	(17,17,'App\\Entity\\Pages\\HomePage','Home page'),
	(18,7,'App\\Entity\\Pages\\ContentPage','Content page'),
	(19,1,'App\\Entity\\Pages\\ContentPage','Content page'),
	(20,8,'App\\Entity\\Pages\\ContentPage','Content page'),
	(21,2,'App\\Entity\\Pages\\ContentPage','Content page'),
	(22,1,'App\\Entity\\Pages\\NewsPage','News page'),
	(23,9,'App\\Entity\\Pages\\ContentPage','Content page'),
	(24,10,'App\\Entity\\Pages\\ContentPage','Content page');

/*!40000 ALTER TABLE `kuma_page_template_configuration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_raw_html_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_raw_html_page_parts`;

CREATE TABLE `kuma_raw_html_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_redirects`;

CREATE TABLE `kuma_redirects` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permanent` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kuma_redirects_idx_domain_origin` (`domain`,`origin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_robots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_robots`;

CREATE TABLE `kuma_robots` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `robots_txt` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_roles`;

CREATE TABLE `kuma_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9B5280A857698A6A` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_roles` WRITE;
/*!40000 ALTER TABLE `kuma_roles` DISABLE KEYS */;

INSERT INTO `kuma_roles` (`id`, `role`)
VALUES
	(1,'ROLE_DYNAMICS_USER');

/*!40000 ALTER TABLE `kuma_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_seo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_seo`;

CREATE TABLE `kuma_seo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `og_image_id` bigint(20) DEFAULT NULL,
  `twitter_image_id` bigint(20) DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `meta_author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_robots` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_description` longtext COLLATE utf8_unicode_ci,
  `extra_metadata` longtext COLLATE utf8_unicode_ci,
  `ref_id` bigint(20) NOT NULL,
  `ref_entity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_article_author` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_article_publisher` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `og_article_section` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_description` longtext COLLATE utf8_unicode_ci,
  `twitter_site` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4695F4A76EFCB8B8` (`og_image_id`),
  KEY `IDX_4695F4A72368A6F1` (`twitter_image_id`),
  KEY `idx_seo_lookup` (`ref_id`,`ref_entity_name`),
  CONSTRAINT `FK_4695F4A72368A6F1` FOREIGN KEY (`twitter_image_id`) REFERENCES `kuma_media` (`id`),
  CONSTRAINT `FK_4695F4A76EFCB8B8` FOREIGN KEY (`og_image_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_seo` WRITE;
/*!40000 ALTER TABLE `kuma_seo` DISABLE KEYS */;

INSERT INTO `kuma_seo` (`id`, `og_image_id`, `twitter_image_id`, `meta_title`, `meta_description`, `meta_author`, `meta_robots`, `og_type`, `og_title`, `og_description`, `extra_metadata`, `ref_id`, `ref_entity_name`, `og_url`, `og_article_author`, `og_article_publisher`, `og_article_section`, `twitter_title`, `twitter_description`, `twitter_site`, `twitter_creator`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,1,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,2,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,5,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,6,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,7,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,8,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,9,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,10,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,11,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,12,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,13,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,14,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,15,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,16,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,17,'App\\Entity\\Pages\\HomePage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,NULL,NULL,'Title','Meta meta',NULL,'noindex',NULL,NULL,NULL,'eeeffff',1,'App\\Entity\\Pages\\ContentPage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,2,'App\\Entity\\Pages\\ContentPage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,1,'App\\Entity\\Pages\\NewsPage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,9,'App\\Entity\\Pages\\ContentPage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,10,'App\\Entity\\Pages\\ContentPage',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `kuma_seo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_single_line_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_single_line_text_page_parts`;

CREATE TABLE `kuma_single_line_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) DEFAULT NULL,
  `error_message_required` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_message_regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_sitemap_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_sitemap_pages`;

CREATE TABLE `kuma_sitemap_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_slide_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_slide_page_parts`;

CREATE TABLE `kuma_slide_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EA7A3F17EA9FDD75` (`media_id`),
  CONSTRAINT `FK_EA7A3F17EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_submit_button_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_submit_button_page_parts`;

CREATE TABLE `kuma_submit_button_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_text_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_text_page_parts`;

CREATE TABLE `kuma_text_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_to_top_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_to_top_page_parts`;

CREATE TABLE `kuma_to_top_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_toc_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_toc_page_parts`;

CREATE TABLE `kuma_toc_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_translation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_translation`;

CREATE TABLE `kuma_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translation_id` int(11) DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'enabled',
  `file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci,
  `domain` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `flag` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword_per_locale` (`keyword`,`locale`,`domain`),
  UNIQUE KEY `translation_id_per_locale` (`translation_id`,`locale`),
  KEY `idx_translation_locale_domain` (`locale`,`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `kuma_translation` WRITE;
/*!40000 ALTER TABLE `kuma_translation` DISABLE KEYS */;

INSERT INTO `kuma_translation` (`id`, `translation_id`, `keyword`, `locale`, `status`, `file`, `text`, `domain`, `created_at`, `updated_at`, `flag`)
VALUES
	(1,1,'heading.hello_world','en','enabled',NULL,'Hello World!','messages','2019-06-14 08:17:39','2019-06-14 08:17:39','new'),
	(2,1,'heading.hello_world','fr','enabled',NULL,'Bonjour tout le monde','messages','2019-06-14 08:17:39','2019-06-14 08:17:39','new'),
	(3,1,'heading.hello_world','nl','enabled',NULL,'Hallo wereld!','messages','2019-06-14 08:17:39','2019-06-14 08:17:39','new'),
	(4,1,'warning.outdated.title','nl_NL','enabled',NULL,'Uw browser is verouderd.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(5,1,'warning.outdated.title','en_NL','enabled',NULL,'You are using an outdated browser.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(6,2,'warning.outdated.subtitle','nl_NL','enabled',NULL,'Some page content will be lost or rendered incorrectly.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(7,2,'warning.outdated.subtitle','en_NL','enabled',NULL,'Sommige inhoud kan verloren gaan of zal niet correct weergegeven worden.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(8,3,'warning.outdated.description','nl_NL','enabled',NULL,'Gelieve een meer recente versie van uw browser te installeren.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(9,3,'warning.outdated.description','en_NL','enabled',NULL,'Please install a more recent version of your browser.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(10,4,'warning.outdated.upgrade_browser','nl_NL','enabled',NULL,'Upgrade uw browser','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(11,4,'warning.outdated.upgrade_browser','en_NL','enabled',NULL,'Upgrade your browser','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(12,5,'cookieconsent.description','nl_NL','enabled',NULL,'Deze website gebruikt cookies om uw surfervaring makkelijker te maken. <a href=\"#\">Meer informatie</a>.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(13,5,'cookieconsent.description','en_NL','enabled',NULL,'This website uses cookies to enhance your browsing experience. <a href=\"#\">More information</a>.','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(14,6,'cookieconsent.confirm','nl_NL','enabled',NULL,'Doorgaan','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(15,6,'cookieconsent.confirm','en_NL','enabled',NULL,'Proceed','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(16,7,'categories.co_reduction','nl_NL','enabled',NULL,'CO² Reductie','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(17,7,'categories.co_reduction','en_NL','enabled',NULL,'CO² Reduction','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(18,8,'categories.retail','nl_NL','enabled',NULL,'Retail','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(19,8,'categories.retail','en_NL','enabled',NULL,'Retail','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(20,9,'tag.retail','nl_NL','enabled',NULL,'Retail','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(21,9,'tag.retail','en_NL','enabled',NULL,'Retail','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(22,10,'tag.international','nl_NL','enabled',NULL,'International','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(23,10,'tag.international','en_NL','enabled',NULL,'International','messages','2019-06-14 08:17:40','2019-06-14 08:17:40','new'),
	(24,11,'app.users.adminlist.header.enabled','nl_NL','enabled',NULL,'Actief','messages','2019-06-14 12:55:45','2019-06-14 12:55:45','new'),
	(25,11,'app.users.adminlist.header.enabled','en_NL','enabled',NULL,'Active','messages','2019-06-14 12:55:45','2019-06-14 12:55:45','new');

/*!40000 ALTER TABLE `kuma_translation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kuma_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_users`;

CREATE TABLE `kuma_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `admin_locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_changed` tinyint(1) DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_39EF0B8692FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_39EF0B86A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_39EF0B86C05FB297` (`confirmation_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table kuma_video_page_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kuma_video_page_parts`;

CREATE TABLE `kuma_video_page_parts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23FF1D35EA9FDD75` (`media_id`),
  CONSTRAINT `FK_23FF1D35EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `kuma_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `refresh_tokens`;

CREATE TABLE `refresh_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refresh_token` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valid` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9BACE7E1C74F2195` (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `refresh_tokens` WRITE;
/*!40000 ALTER TABLE `refresh_tokens` DISABLE KEYS */;

INSERT INTO `refresh_tokens` (`id`, `refresh_token`, `username`, `valid`)
VALUES
	(1,'070b281c79aeffd0511448197f032bb1e72af6cc2597fdc5595651d1b1dea1543fdfb2790a6c6355f3b270ae77aeb84add027c914324de48ad009ff88ff10267','martijn+api17@raakrdam.nl','2019-07-14 11:52:28'),
	(2,'294e85df2b15aabba4d5b96361b05913f5b78f8f24d2a9f314d4ad1f889d0abb874b233d6db1e844d83c5e42f893af6ef2a8ea779cc2066abf82c213509dca64','martijn+api17@raakrdam.nl','2019-07-14 11:56:30'),
	(3,'7589fcc1e678d67787e8081bf08c25ef53f54d4c90a4a1e65bd0ac897885b26f76b824f139d1f6ffdfece91e108fd2d8645d0f193375bf97460e6ce45469957c','martijn+api17@raakrdam.nl','2019-07-14 11:58:54'),
	(4,'4fa36538c57540be598b510c15f2789c2f6e3d3cd4be5cde10d4fc41abfc1fd9d41e703768762848d60614b6ba4f10d7494be1246548b8830a223175d2861d57','martijn+api17@raakrdam.nl','2019-07-14 11:59:18'),
	(5,'5bf73c59cee7073196905d4ecc9adeec86211540d2c70fc3316e0de384a47b1812e02eb5eae3192b7e2acf6e91393f1974fd94c7391abfa9ffd51f1828c917cb','martijn+api17@raakrdam.nl','2019-07-14 12:00:01'),
	(6,'a60a3ebfeba92d451af086e0bc12671489ac3a6cd9fa05c19c44deb498301232a8dfba56b12c1c3cfaf6348a74f80df5da02b479b7b69188d7e789022acd2a3a','lean-green@test.dev','2019-07-14 12:18:33'),
	(7,'c6c2709683cc81a0facf18caeb4033c0da0948fe0e20416ce78743e70ccc7ce52be4677ee205f4c1d728bddf87b0d9d8b0b97a029580eea6b0211963a61af78f','lean-green@test.dev','2019-07-14 13:09:41'),
	(8,'e1182550d88bb5e118174d782cd0d0260b5e9398585da1b9a0c162228894aa0e412616dcf70e855171610bfa1174fafbdbdf254566798bb1aaf784da3ebe3f3c','lean-green@test.dev','2019-07-14 14:38:25'),
	(9,'2e1a7d7db14461182ef05f72165a7f3615e1b1b49ea5d3a9c8b202a37abf0e30d11d052a9bb63eb65d13fb1161213cd32bdaec80b50e08235d2b0604dfb198d1','martijn+lg6@raakrdam.nl','2019-07-19 10:38:29'),
	(10,'117e567e9aa24458639bd06393df7ebb18a8c5de2a5130bbaff66bc3b64a58b41569db85b15cd955e2807f7af1c86ac4899696e9e63dac0d57a4541361594511','martijn+lg6@raakrdam.nl','2019-07-19 13:42:17');

/*!40000 ALTER TABLE `refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
