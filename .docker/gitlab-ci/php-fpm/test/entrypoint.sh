#!/usr/bin/env bash

mkdir /cache \
    && chown -R www-data:www-data /cache \
    && chmod -R 777 /cache \
    && setfacl -dR -m u:www-data:rwX -m u:www-data:rwX /cache \
    && setfacl  -R -m u:www-data:rwX -m u:www-data:rwX /cache

bin/console doctrine:migrations:migrate -n

php-fpm
