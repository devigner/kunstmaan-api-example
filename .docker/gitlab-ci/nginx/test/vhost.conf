server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name test-www.lean-green.nl;
    return 301 https://$host$request_uri;
}

server {
    listen [::]:443 ssl http2 default_server;
    listen 443 ssl http2 default_server;

    ssl_certificate /etc/ssl/wildcard.cert;
    ssl_certificate_key /etc/ssl/wildcard.key;
    
    charset utf-8;
    gzip off;

    client_max_body_size 200M;
    access_log /var/log/nginx/access.log;

    # VUE ROUTES
    location / {
        proxy_pass http://node-app:3000;
    }

    location /_loading/ {
        proxy_pass http://node-app:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /nginx_status {
       stub_status on;
       access_log off;
       allow all;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate /etc/ssl/wildcard.cert;
    ssl_certificate_key /etc/ssl/wildcard.key;

    server_name test-api.lean-green.nl test-backend.lean-green.nl;
    charset utf-8;

    client_max_body_size 200M;
    access_log /var/log/nginx/access.log;

    root /app/public/;

    # SYMFONY ROUTES
    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass phpfpm-app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include /etc/nginx/fastcgi_params;
        fastcgi_param HTTPS on;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location /nginx_status {
        stub_status on;
        access_log off;
        allow all;
    }

    location /php_status {
        fastcgi_pass phpfpm-app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include /etc/nginx/fastcgi_params;
    }
}
