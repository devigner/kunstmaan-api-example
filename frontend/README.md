# Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at 0.0.0.0:3000
yarn run dev

# build for test/acceptance
yarn run build --devtools
yarn start

# build for production and launch server
yarn run build
yarn start

```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
