module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    extends: [
        '@nuxtjs',
        'plugin:nuxt/recommended',
        'prettier',
        'prettier/vue'
    ],
    plugins: [
        'prettier'
    ],
    // add your custom rules here
    rules: {
        "no-console": "off",
        "yoda": [1, "always"],
        "nuxt/no-globals-in-created": "off"
    }
}
