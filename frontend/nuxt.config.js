require('dotenv').config()

// Language configuration
import nl_NL from './lang/nl_NL.js'
import en_NL from './lang/en_NL.js'

export const languages = {
    nl_NL: nl_NL,
    en_NL: en_NL,
}

const messages = Object.assign(languages)

module.exports = {
    mode: 'universal',

    /*
     ** Headers of the page
     */
    head: {
        title: 'Project NL',
        meta: [
            {charset: 'utf-8'},
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: ''
            }
        ],
        link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {color: '#fff'},

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        {src: '~/plugins/vuex-cache.js', ssr: false},
        {src: '~/plugins/api.js'},
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv',
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',

        '@nuxtjs/proxy',

        // Doc: https://auth.nuxtjs.org/
        '@nuxtjs/auth',
        // Doc: https://bootstrap-vue.js.org/docs/
        'bootstrap-vue/nuxt',
        // Doc: https://github.com/nuxt-community/style-resources-module
        '@nuxtjs/style-resources',

        // Doc: https://nuxt-community.github.io/nuxt-i18n
        ['nuxt-i18n', {
            locales: [{code: 'nl_NL', iso: 'nl-NL'}, {code: 'en_NL', iso: 'en-NL'}],
            defaultLocale: 'nl_NL',
            vueI18n: {
                locale: 'nl_NL',
                fallbackLocale: 'nl_NL',
                messages
            }
        }]
    ],

    /*
     * Nuxt Auth options
     */
    auth: {
        // Default redirects
        redirect: {
            login: '/login',
            logout: '/',
            callback: false,
            home: '/',
        },

        // Vuex store namespace for keeping state.
        vuex: {
            namespace: 'auth',
        },

        scopeKey: 'roles', // User object property used for scope checking (hasScope). Can be either an array or a object.

        // Default strategy for JWT
        strategies: {
            local: {
                endpoints: {
                    login: {url: process.env.BACKEND_HOST + '/api/login_check', method: 'post', propertyName: 'token'},
                    logout: false,
                    user: {url: process.env.BACKEND_HOST + '/api', method: 'get', propertyName: false},
                },
                tokenRequired: true,
            },
        },
    },

    /*
     * Disable automatic inclusion of Bootstrap and BootstrapVue
     */
    bootstrapVue: {
        bootstrapCSS: false,
        bootstrapVueCSS: false,
    },

    /*
     ** Global CSS
     */
    css: [
        'bootstrap/scss/bootstrap.scss',
        '@assets/scss/main.scss',
    ],

    /*
    * Global SCSS
    *
    * These files will be accessible from within component '<style>'
    */
    styleResources: {
        sass: [
            '~assets/scss/theme/index.scss',
        ],
    },

    /*
     ** Axios module configuration
     */
    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        debug: 'dev' === process.env.APP_ENV,
        baseURL: 'http://nginx-app',
        browserBaseURL: process.env.BACKEND_HOST,
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        postcss: {
            preset: {
                // Change the postcss-preset-env settings
                autoprefixer: {}
            }
        }
    }
}
