const apiFactory = (axios, app, query) => ({
    /**
     * Get a page
     *
     * @param slug
     */
    getPage(slug = '') {
        return this.get(slug)
    },

    /**
     * Get function, adds i18n locale into the url
     *
     * @param slug
     */
    get(slug) {
        if (query.hasOwnProperty('preview-token')) {
            return axios.get(`/preview/${query['preview-token']}`)
        }

        return axios.get(`/api/${app.i18n.locale}/${slug}`)
    }
})

/*
 ** Executed by ~/.nuxt/index.js with context given
 ** This method can be asynchronous
 */
export default ({$axios, app, query}, inject) => {
    // Inject `api` key
    // -> app.$api
    // -> this.$api in vue components
    // -> this.$api in store actions/mutations

    const api = apiFactory($axios, app, query)
    inject('api', api)
}
