export const state = () => ({
    header: {
        title: '',
        subtitle: '',
        anchorText: '',
    },
    statistics: [{
        value: '',
        title: '',
        text: '',
    }]
})

export const mutations = {
    setHeader(state, val) {
        state.header = val
    },

    setStatistics(state, val) {
        state.statistics = val
    },
}

export const actions = {
    init(context, homepage) {
        const parts = homepage.data.parts

        if (!parts) {
            console.error('No homepage parts found', homepage)
            return
        }

        /*

        parseHeader()
        parseStatistics()

        function parseHeader() {
            // Get header object
            const data = parts.find(obj => {
                return obj.type === 'HomeHeaderPagePart'
            })

            if (undefined === data) {
                console.error('No header data found', homepage)
                return
            }

            context.commit('setHeader', {
                imgSrc: data.imgSrc,
                title: data.title,
                subtitle: data.subtitle,
                anchorText: data.anchorText,
                anchorTargetId: data.anchorTargetId
            })
        }

        function parseStatistics() {
            // Get header object
            const data = parts.find(obj => {
                return obj.type === 'HomeStatisticsPagePart'
            })

            if (undefined === data) {
                console.error('No statistics data found', homepage)
                return
            }

            context.commit('setStatistics', data.items)
        }*/
    },
}

export const getters = {
    getHeader(state) {
        return state.header
    },

    getStatistics(state) {
        return state.statistics
    },
}
