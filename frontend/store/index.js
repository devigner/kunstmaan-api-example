export const state = () => ({
    reqHeader: '',
    headerHeight: 0,
})

export const mutations = {
    setReqHeader(state, val) {
        state.reqHeader = val
    },

    setHeaderHeight(state, val) {
        state.headerHeight = val
    },
}

export const actions = {

}

export const getters = {
    getReqHeader(state) {
        return state.reqHeader
    },

    getHeaderHeight(state) {
        return state.headerHeight
    },
}
