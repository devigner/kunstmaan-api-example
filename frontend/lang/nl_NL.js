export default {
    pages: {
        home: 'Home',
        program: 'Mee doen',
        news: 'Nieuws',
        events: 'Events',
        measurements: 'Maatregelen',
        participants: 'Deelnemers',
        contact: 'Contact',
    },
    global: {
        register: 'Registreren',
        login: 'Inloggen',
    }
}
