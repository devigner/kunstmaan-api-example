export default {
    pages: {
        home: 'Home',
        program: 'Participate',
        news: 'News',
        events: 'Events',
        measurements: 'Measurements',
        participants: 'Participants',
        contact: 'Contact',
    },
    global: {
        register: 'Register',
        login: 'Log in',
    }
}
